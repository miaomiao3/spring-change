package com.change.entity.account;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.change.entity.BaseEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDate;

/**
 * @author 坏孩子是你啊
 * @date 2022-07-15
 */
@Data
@TableName("account_info")
public class AccountInfo extends BaseEntity {
    private static final long serialVersionUID = 1L;
    @ApiModelProperty("账号 username")
    private String account;
    @ApiModelProperty("密码加密")
    private String password;
    @TableField(value = "browser_uuid")
    @ApiModelProperty("浏览器标识")
    private String browserUuid;
    @ApiModelProperty("角色")
    private String role;
    @ApiModelProperty("部门")
    private Integer dept;
    @ApiModelProperty("真实姓名")
    private String realName;
    @ApiModelProperty("昵称")
    private String nickName;
    @ApiModelProperty("描述")
    private String desc;
    @ApiModelProperty("简介")
    private String intro;
    @ApiModelProperty("资金")
    private BigDecimal balance;
    @ApiModelProperty("微信收款码")
    private String wxMoneyCode;
    @ApiModelProperty("支付宝收款码")
    private String aliMoneyCode;
    @ApiModelProperty("手机号")
    private String mobile;
    @ApiModelProperty("邮箱地址")
    private String email;
    @ApiModelProperty("头像地址")
    private String avatar;

    @ApiModelProperty("生日")
    private LocalDate birthday;
    @ApiModelProperty("1 男性 2 女性 3 保密")
    private Integer sex;
}