package com.change.entity.moments;

import com.baomidou.mybatisplus.annotation.TableName;
import com.change.entity.BaseEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.sql.Timestamp;
import java.time.LocalDateTime;

/**
* @author 坏孩子是你啊
* @date  2022-10-06
*/
@Data
@TableName("moments_gather")
public class MomentsGather extends BaseEntity {
private static final long serialVersionUID = 1L;
	@ApiModelProperty("图片数组")
	private String imageList;
	@ApiModelProperty("坐标1")
	private Double latitude;
	@ApiModelProperty("坐标2")
	private Double longitude;
	@ApiModelProperty("标题")
	private String title;
	@ApiModelProperty("地址名称")
	private String name;
	@ApiModelProperty("地址")
	private String address;
	@ApiModelProperty("0 公开 1仅关注")
	private Integer whoType;
	@ApiModelProperty("内容")
	private String content;
	@ApiModelProperty("人数")
	private Integer counts;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	@ApiModelProperty("集合时间")
	private LocalDateTime gatherTime;
	@ApiModelProperty("1正常 2满员 3已结束")
	private Integer status;
						

}