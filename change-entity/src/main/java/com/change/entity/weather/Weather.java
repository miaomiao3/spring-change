package com.change.entity.weather;

import com.baomidou.mybatisplus.annotation.TableName;
import com.change.entity.BaseEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
* @author 坏孩子是你啊
* @date  2022-08-18
*/
@Data
@TableName("weather")
public class Weather extends BaseEntity {
private static final long serialVersionUID = 1L;
	@ApiModelProperty("温度")
	private String temperature;
	@ApiModelProperty("状态")
	private String status;
	

}