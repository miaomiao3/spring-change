package com.change.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public abstract class BaseEntity implements Identifiable<Integer> {


    @ApiModelProperty(value = "ID")
    @TableId(type = IdType.AUTO)
    private Integer id;

    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private LocalDateTime createTime;

    @TableField(fill = FieldFill.INSERT_UPDATE,update = "now()")
    @ApiModelProperty(value = "更新时间")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime updateTime;

    @JsonIgnore
    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建人ID")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Integer creatorId;

    @JsonIgnore
    @TableField(fill = FieldFill.INSERT_UPDATE)
    @ApiModelProperty(value = "更新人ID")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Integer updateId;

    @JsonIgnore
    @TableField(exist = false, fill = FieldFill.INSERT_UPDATE)
    @ApiModelProperty(value = "公司ID")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Integer companyId;

    @JsonIgnore
    @ApiModelProperty(hidden = true)
    @TableLogic
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Integer deleted;

    @TableField( exist = false)
    @ApiModelProperty(value = "更新人")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String updateBy;

    @TableField( exist = false)
    @ApiModelProperty(value = "创建人")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String createBy;

}
