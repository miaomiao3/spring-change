package com.change.entity.system;

import com.baomidou.mybatisplus.annotation.TableName;
import com.change.entity.BaseEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author jhlz
 * @date 2022-08-27
 */
@Data
@TableName("system_role")
public class SystemRole extends BaseEntity {
    private static final long serialVersionUID = 1L;
    @ApiModelProperty("角色名称")
    private String roleName;
    @ApiModelProperty("角色权限字符串")
    private String roleValue;
    @ApiModelProperty("排序")
    private Integer orderNo;
    @ApiModelProperty("角色状态（1启用，0禁用）")
    private String status;
    @ApiModelProperty("备注")
    private String remark;
}