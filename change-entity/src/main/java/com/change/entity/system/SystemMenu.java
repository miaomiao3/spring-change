package com.change.entity.system;

import com.baomidou.mybatisplus.annotation.TableName;
import com.change.entity.BaseEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author jhlz
 * @date 2022-08-27
 */
@Data
@TableName("system_menu")
public class SystemMenu extends BaseEntity {
    private static final long serialVersionUID = 1L;
    @ApiModelProperty("菜单名称")
    private String menuName;
    @ApiModelProperty("菜单类型（0目录 1菜单 2按钮）")
    private String type;
    @ApiModelProperty("父菜单ID")
    private Integer parentMenu;
    @ApiModelProperty("显示顺序")
    private Integer orderNo;
    @ApiModelProperty("路由地址")
    private String routePath;
    @ApiModelProperty("组件路径")
    private String component;
    @ApiModelProperty("是否显示（0显示 1隐藏）")
    private String visible;
    @ApiModelProperty("菜单状态（0正常 1停用）")
    private String status;
    @ApiModelProperty("权限标识")
    private String permission;
    @ApiModelProperty("菜单图标")
    private String icon;
    @ApiModelProperty("备注")
    private String remark;
    @ApiModelProperty("是否缓存(0否 1是)")
    private String keepLive;
    @ApiModelProperty("是否外链(0否 1是)")
    private String isExt;
}