package com.change.entity.system;

import com.baomidou.mybatisplus.annotation.TableName;
import com.change.entity.BaseEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
* @author 路人甲
* @date  2022-08-21
*/
@Data
@TableName("system_department")
public class SystemDepartment extends BaseEntity {
private static final long serialVersionUID = 1L;
	@ApiModelProperty("名称")
	private String name;
	@ApiModelProperty("父id")
	private Integer pid;
	@ApiModelProperty("排序")
	private Integer sort;
	@ApiModelProperty("0启用1停用")
	private Integer status;
	@ApiModelProperty("备注")
	private String remark;
	

}