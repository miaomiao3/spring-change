package com.change.entity.article;

import com.baomidou.mybatisplus.annotation.TableName;
import com.change.entity.BaseEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
* @author 坏孩子是你啊
* @date  2022-07-24
*/
@Data
@TableName("article_info")
public class ArticleInfo extends BaseEntity {
private static final long serialVersionUID = 1L;
	@ApiModelProperty("标题")
	private String title;
	@ApiModelProperty("内容")
	private String content;
	@ApiModelProperty("标签")
	private String tags;
	

}