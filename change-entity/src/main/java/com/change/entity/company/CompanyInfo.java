package com.change.entity.company;

import com.baomidou.mybatisplus.annotation.TableName;
import com.change.entity.BaseEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
* @author 坏孩子是你啊
* @date  2022-08-15
*/
@Data
@TableName("company_info")
public class CompanyInfo extends BaseEntity {
private static final long serialVersionUID = 1L;
	@ApiModelProperty("企业名称")
	private String companyName;
	@ApiModelProperty("企业类型")
	private String companyType;
	@ApiModelProperty("企业地址")
	private String address;
	@ApiModelProperty("企业邮箱")
	private String email;
	@ApiModelProperty("企业电话")
	private String phone;
	@ApiModelProperty("资金")
	private BigDecimal balance;
	@ApiModelProperty("微信收款码")
	private String wxMoneyCode;
	@ApiModelProperty("支付宝收款码")
	private String aliMoneyCode;
	@ApiModelProperty("法人代表")
	private String representive;
	@ApiModelProperty("法人微信号")
	private String representWechat;
	@ApiModelProperty("注册资本")
	private BigDecimal registerCapital;
	@ApiModelProperty("社会统一信用码")
	private String companyCode;
	@ApiModelProperty("企业简介")
	private String introduction;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	@ApiModelProperty("成立日期")
	private LocalDateTime listingDate;
	@ApiModelProperty("经营状态(1：正常；2：禁用；0：注销)")
	private Integer listingStatus;
	@ApiModelProperty("所属行业")
	private String industry;
	@ApiModelProperty("是否启用")
	private Boolean enabled;
	@ApiModelProperty("企业账户/标识")
	private String companyAccounts;
					

}