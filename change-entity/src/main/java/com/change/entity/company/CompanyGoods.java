package com.change.entity.company;

import com.baomidou.mybatisplus.annotation.TableName;
import com.change.entity.BaseEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @author jhlz
 * @date 2022-08-30
 */
@Data
@TableName("company_goods")
public class CompanyGoods extends BaseEntity {
    private static final long serialVersionUID = 1L;
    @ApiModelProperty("名称")
    private String goodsName;
    @ApiModelProperty("类型")
    private String goodsType;
    @ApiModelProperty("价格")
    private BigDecimal price;
    @ApiModelProperty("标签")
    private String tags;
    @ApiModelProperty("用户id")
    private Integer accountId;
    @ApiModelProperty("备注")
    private String remark;


}