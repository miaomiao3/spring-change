package com.change.entity.recommend;

import com.baomidou.mybatisplus.annotation.TableName;
import com.change.entity.BaseEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
* @author 坏孩子是你啊
* @date  2022-09-05
*/
@Data
@TableName("recommend_info_file")
public class RecommendInfoFile extends BaseEntity {
	private static final long serialVersionUID = 1L;

	@ApiModelProperty("推荐信息")
	private Integer recInfoId;
	@ApiModelProperty("地址")
	private String url;
	@ApiModelProperty("1 图片 2 视频 3其他")
	private String fileType;

}