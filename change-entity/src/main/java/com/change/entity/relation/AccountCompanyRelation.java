package com.change.entity.relation;

import com.baomidou.mybatisplus.annotation.TableName;
import com.change.entity.BaseEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author jhlz
 * @time 2022/8/22 15:17
 * @desc: AccountCompanyRelation
 */
@Data
@TableName("account_company_relation")
public class AccountCompanyRelation extends BaseEntity {
    private static final long serialVersionUID = 1L;
    @ApiModelProperty("公司id")
    private Integer companyId;
    @ApiModelProperty("用户id")
    private Integer accountId;
}
