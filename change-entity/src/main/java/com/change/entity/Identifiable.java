package com.change.entity;

import java.io.Serializable;

public interface Identifiable<ID> extends Serializable {

    void setId(ID id);

    ID getId();
}
