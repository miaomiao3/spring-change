package com.change.dto.req;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
* @author 路人甲
* @date  2022-08-21
*/
@Data
public class QuerySystemDepartmentReq extends BaseReq {


    @ApiModelProperty("名称")
    private String deptName;
    @ApiModelProperty("父id")
    private Integer pid;
    @ApiModelProperty("排序")
    private Integer sort;
    @ApiModelProperty("0启用1停用")
    private Boolean status;
    @ApiModelProperty("备注")
    private String remark;


}