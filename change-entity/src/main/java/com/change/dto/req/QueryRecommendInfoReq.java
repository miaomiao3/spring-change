package com.change.dto.req;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
* @author 坏孩子是你啊
* @date  2022-09-04
*/
@Data
public class QueryRecommendInfoReq extends BaseReq {
	@ApiModelProperty("标题")
	private String title;
	@ApiModelProperty("积分")
	private Integer points;
	@ApiModelProperty("封面")
	private String poster;
	@ApiModelProperty("描述")
	private String descript;
	@ApiModelProperty("内容")
	private String content;
	@ApiModelProperty("纬度")
	private BigDecimal lat;
	@ApiModelProperty("经度")
	private BigDecimal lng;
	@ApiModelProperty("省")
	private String province;
	@ApiModelProperty("市")
	private String city;
	@ApiModelProperty("地址")
	private String address;
						

}