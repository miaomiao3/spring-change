package com.change.dto.req;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
* @author 坏孩子是你啊
* @date  2022-08-30
*/
@Data
public class QueryCompanyArticleReq extends BaseReq {
	@ApiModelProperty("标题")
	private String title;
	@ApiModelProperty("内容")
	private String content;
	@ApiModelProperty("标签")
	private String tags;
						

}