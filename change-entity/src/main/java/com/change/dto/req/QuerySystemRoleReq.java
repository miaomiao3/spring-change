package com.change.dto.req;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author jhlz
 * @date 2022-08-27
 */
@Data
public class QuerySystemRoleReq extends BaseReq {
    @ApiModelProperty("角色名称")
    private String roleName;
    @ApiModelProperty("角色权限字符串")
    private String roleValue;
    @ApiModelProperty("排序")
    private Integer orderNo;
    @ApiModelProperty("角色状态（1启用，0禁用）")
    private String status;
    @ApiModelProperty("创建者")
    private String createBy;
    @ApiModelProperty("更新者")
    private String updateBy;
    @ApiModelProperty("备注")
    private String remark;


}