package com.change.dto.req;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author jhlz
 * @time 2022/8/15 15:01
 * @desc: QueryCompanyInfoReq
 */
@Data
public class QueryCompanyInfoReq {
    @ApiModelProperty("企业名称")
    private String companyName;
}
