package com.change.dto.req;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
* @author 路人甲
* @date  2022-08-21
*/
@Data
public class SaveSystemDepartmentReq  {

    @ApiModelProperty("主键")
    private Integer id;

    @ApiModelProperty("名称")
    private String deptName;
    @ApiModelProperty("父id")
    private Integer parentDept;
    @ApiModelProperty("排序")
    private Integer orderNo;

    @ApiModelProperty("0启用1停用")
    private Integer status;
    @ApiModelProperty("备注")
    private String remark;



}