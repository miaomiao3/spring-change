package com.change.dto.req;

import com.change.entity.BaseEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
* @author 坏孩子是你啊
* @date  2022-08-18
*/
@Data
public class SaveRobotReq extends BaseEntity {
    @ApiModelProperty("机器人名称")
    private String name;
    @ApiModelProperty("机器人微信号")
    private String wxAccount;
    @ApiModelProperty("密码")
    private String password;
    @ApiModelProperty("机器人微信昵称")
    private String wxName;
    @ApiModelProperty("公司id")
    private Integer companyId;
    @ApiModelProperty("部署状态")
    private String deployed;
    @ApiModelProperty("用途描述")
    private String description;
}