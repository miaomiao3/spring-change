package com.change.dto.req;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
* @author 坏孩子是你啊
* @date  2022-07-27
*/
@Data
public class SaveAccountWechatReq  {

    @ApiModelProperty("微信号")
    private String wxNo;
    @ApiModelProperty("昵称")
    private String nickname;
    @ApiModelProperty("备注")
    private String remarks;
    @ApiModelProperty("地区")
    private String area;
    @ApiModelProperty("性别")
    private String gender;
    @ApiModelProperty("朋友权限")
    private String auth;
    @ApiModelProperty("来源")
    private String source;
    @ApiModelProperty("头像地址")
    private String avatar;
    @ApiModelProperty("标签")
    private String tags;

}