package com.change.dto.req;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
* @author 坏孩子是你啊
* @date  2022-07-24
*/
@Data
public class QueryArticleInfoReq extends BaseReq {
    @ApiModelProperty("标题")
    private String title;

}