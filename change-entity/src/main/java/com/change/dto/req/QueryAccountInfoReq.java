package com.change.dto.req;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
* @author 坏孩子是你啊
* @date  2022-07-15
*/
@Data
public class QueryAccountInfoReq extends BaseReq {
    @ApiModelProperty("部门")
    private Integer deptId;
    @ApiModelProperty("账号")
    private String account;
    @ApiModelProperty("手机号")
    private String mobile;
    @ApiModelProperty("姓名")
    private String realname;
    @ApiModelProperty("昵称")
    private String nickname;
}