package com.change.dto.req;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
* @author 坏孩子是你啊
* @date  2022-07-27
*/
@Data
public class QueryAccountWechatReq extends BaseReq {

    @ApiModelProperty("微信号")
    private String wxNo;
    @ApiModelProperty("昵称")
    private String nickname;
}