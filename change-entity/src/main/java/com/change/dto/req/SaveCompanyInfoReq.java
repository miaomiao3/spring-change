package com.change.dto.req;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * @author jhlz
 * @time 2022/8/15 15:02
 * @desc: SaveCompanyInfoReq
 */
@Data
public class SaveCompanyInfoReq {
    @ApiModelProperty("id")
    private Integer id;
    @ApiModelProperty("企业名称")
    private String companyName;
    @ApiModelProperty("企业类型")
    private String companyType;
    @ApiModelProperty("企业地址")
    private String address;
    @ApiModelProperty("企业邮箱")
    private String email;
    @ApiModelProperty("企业电话")
    private String phone;
    @ApiModelProperty("法人代表")
    private String representive;
    @ApiModelProperty("法人微信号")
    private String representWechat;
    @ApiModelProperty("注册资本")
    private BigDecimal registerCapital;
    @ApiModelProperty("社会统一信用码")
    private String companyCode;
    @ApiModelProperty("企业简介")
    private String introduction;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @ApiModelProperty("成立日期")
    private LocalDateTime listingDate;
    @ApiModelProperty("经营状态(1：正常；2：禁用；0：注销)")
    private Integer listingStatus;
    @ApiModelProperty("所属行业")
    private String industry;
}
