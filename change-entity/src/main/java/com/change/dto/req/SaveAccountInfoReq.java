package com.change.dto.req;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
* @author 坏孩子是你啊
* @date  2022-07-15
*/
@Data
public class SaveAccountInfoReq  {
    @ApiModelProperty("账户id")
    private Integer userId;
    @ApiModelProperty("真实姓名")
    private String realName;
    @ApiModelProperty("昵称")
    private String nickName;
    @ApiModelProperty("介绍")
    private String intro;
    @ApiModelProperty("头像")
    private String avatar;
    @ApiModelProperty("1男性 2女性 3保密")
    private Integer sex;
    @ApiModelProperty("生日")
    private String birthday;
    @ApiModelProperty("微信收款")
    private String wxMoneyCode;
    @ApiModelProperty("支付宝收款")
    private String aliMoneyCode;
    @ApiModelProperty("联系方式")
    private String mobile;
}