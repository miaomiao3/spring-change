package com.change.dto.req;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @author jhlz
 * @date 2022-08-30
 */
@Data
public class QueryCompanyGoodsReq extends BaseReq {
    @ApiModelProperty("名称")
    private String goodsName;
    @ApiModelProperty("类型")
    private String goodsType;
    @ApiModelProperty("价格")
    private BigDecimal price;
    @ApiModelProperty("标签")
    private String tags;
    @ApiModelProperty("用户id")
    private Integer accountId;
    @ApiModelProperty("备注")
    private String remark;


}