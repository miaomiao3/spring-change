package com.change.dto.req;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class RegisterReq {

    @ApiModelProperty("用户名")
    private String username;
    @ApiModelProperty("密码")
    private String password;
    @ApiModelProperty(value = "浏览器标识",hidden = true)
    private String browserUuid;
    @ApiModelProperty("1 账号密码 2 匿名登陆")
    private Integer anonymous=2;
}
