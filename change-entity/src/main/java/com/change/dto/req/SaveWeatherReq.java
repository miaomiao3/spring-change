package com.change.dto.req;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
* @author 坏孩子是你啊
* @date  2022-08-18
*/
@Data
public class SaveWeatherReq  {
    @ApiModelProperty("温度")
    private String temperature;
    @ApiModelProperty("状态")
    private String status;
}