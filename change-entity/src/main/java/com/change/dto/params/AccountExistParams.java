package com.change.dto.params;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class AccountExistParams {

    @ApiModelProperty("账号")
    private String account;

}
