package com.change.dto.params;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

@Data
public class UploadFileParams {

    private String name;
    private MultipartFile file;
    private String filename;
    private String key;
}
