package com.change.dto.params;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 基础参数
 * @param <T>
 */
@Data
public class BaseParams<T> {
    @ApiModelProperty("主键")
    private Integer id;
    @ApiModelProperty("公司主键")
    private Integer companyId;
    @ApiModelProperty("用户id")
    private Integer userId;
    @ApiModelProperty(value = "开始-结束")
    private String[] timeRange;
    @ApiModelProperty(value = "更新开始时间", hidden = true)
    private String startTime;
    @ApiModelProperty(value = "更新结束时间", hidden = true)
    private String endTime;
    @ApiModelProperty(value = "是否展示所有字段 0.自定义 1.全部")
    private String isShow;
    @ApiModelProperty(value = "是否查询条件 0.自定义 1.全部")
    private String isQuery;
    @ApiModelProperty(value = "是否导出全部 0.自定义 1.全部")
    private String isExport;
    @ApiModelProperty(hidden = true)
    private String timeSql;
    @ApiModelProperty(hidden = true)
    private String tableName;

    public void setTimeRange(String[] timeRange) {
        this.timeRange = timeRange;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime + " 23:59:59";
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime + " 00:00:00";
    }

    public void timeSet() {
        String[] timeRange = this.getTimeRange();
        if (timeRange != null && timeRange.length > 0) {
            this.setStartTime(timeRange[0]);
            this.setEndTime(timeRange[1]);
        }

    }

    public void timeSet(String tableName) {
        this.timeSet();
        this.setTableName(tableName);
    }
}
