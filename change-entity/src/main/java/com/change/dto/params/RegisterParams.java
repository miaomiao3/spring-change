package com.change.dto.params;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 公司注册参数
 */
@Data
public class RegisterParams {
    @ApiModelProperty("公司名称")
    private String companyName;
    @ApiModelProperty("账号")
    private String account;
    @ApiModelProperty("密码")
    private String password;
    @ApiModelProperty("浏览器标识")
    private String browserUuid;
}
