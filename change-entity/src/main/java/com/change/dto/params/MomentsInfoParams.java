package com.change.dto.params;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
* @author 坏孩子是你啊
* @date  2022-10-04
*/
@Data
public class MomentsInfoParams extends BaseParams {
private static final long serialVersionUID = 1L;
	@ApiModelProperty("坐标1")
	private Double latitude;
	@ApiModelProperty("坐标2")
	private Double longitude;
	@ApiModelProperty("地址名称")
	private String name;
	@ApiModelProperty("地址")
	private String address;
	@ApiModelProperty("0 公开 1仅关注")
	private Integer whoType;
	@ApiModelProperty("内容")
	private String content;
	@ApiModelProperty("图片数组")
	private String[] imageList;


}