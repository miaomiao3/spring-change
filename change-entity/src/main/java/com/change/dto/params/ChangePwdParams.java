package com.change.dto.params;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class ChangePwdParams {

    @ApiModelProperty("原密码")
    private String passwordOld;

    @ApiModelProperty("新密码")
    private String passwordNew;
}
