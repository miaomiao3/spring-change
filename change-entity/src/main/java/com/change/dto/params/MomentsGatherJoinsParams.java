package com.change.dto.params;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
* @author 坏孩子是你啊
* @date  2022-10-06
*/
@Data
public class MomentsGatherJoinsParams extends BaseParams {
private static final long serialVersionUID = 1L;
	@ApiModelProperty("账号")
	private Integer accountId;
	@ApiModelProperty("线下")
	private Integer gatherId;
	@ApiModelProperty("手机号")
	private String mobile;
	@ApiModelProperty("1正常 2退出 3放鸽子")
	private Integer status;
						

}