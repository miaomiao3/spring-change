package com.change.dto.resp;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class UserLoginResp {

    @ApiModelProperty("用户主键")
    private Integer id;
    @ApiModelProperty("用户名")
    private String username;
    @ApiModelProperty("浏览器标识")
    private String browserUuid;
    @ApiModelProperty("密码")
    @JsonIgnore
    private String password;
    @ApiModelProperty("头像")
    @JsonIgnore
    private String avatar;
    @ApiModelProperty("签名")
    @JsonIgnore
    private String signature;
    @ApiModelProperty("手机号")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String mobile;
    @ApiModelProperty("1游客 2普通会员 3黄金会员 4砖石会员")
    private Integer vip;
    @ApiModelProperty("访问token")
    private String token;
    @ApiModelProperty("header内容格式")
    private String header;

}
