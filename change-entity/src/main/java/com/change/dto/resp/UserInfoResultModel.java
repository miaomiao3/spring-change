package com.change.dto.resp;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
public class UserInfoResultModel {

    private String desc = "manager";
    private String homePath= "/dashboard/analysis";
    private String password = "123456";
    private String realName = "Vben Admin";
    private String token = "fakeToken1";
    private Integer userId = 1;
    private String username = "vben";
    @ApiModelProperty("生日")
    private String birthday;
    @ApiModelProperty("1 男性 2 女性 3 保密")
    private Integer sex;

    @ApiModelProperty("昵称")
    private String nickName;
    @ApiModelProperty("简介")
    private String intro;
    @ApiModelProperty("头像")
    private String avatar;
    @ApiModelProperty("余额")
    private BigDecimal balance;
    @ApiModelProperty("微信收款")
    private String wxMoneyCode;
    @ApiModelProperty("支付宝收款")
    private String aliMoneyCode;
    @ApiModelProperty("联系方式")
    private String mobile;
    @ApiModelProperty("角色字符串")
    private String role;


    @ApiModelProperty("用户角色")
    private List<LoginResultModel.RoleInfo> roles;

    @Data
    public static class RoleInfo {
        @ApiModelProperty("角色名称")
        private String roleName;
        @ApiModelProperty("角色内容")
        private String value;
    }
}
