package com.change.dto.resp;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
* @author 路人甲
* @date  2022-08-30
*/
@Data
public class QueryCompanyWebsiteResp extends BaseResp{


	@ApiModelProperty("网站名称")
	private String site;
	@ApiModelProperty("域名")
	private String domain;
	@ApiModelProperty("logo")
	private String logo;
	@ApiModelProperty("公司介绍（富文本）")
	private String introduction;
	@ApiModelProperty("位置（百度地图）")
	private String location;
	@ApiModelProperty("联系方式")
	private String mobile;
	@ApiModelProperty("邮箱")
	private String email;
	@ApiModelProperty("机器人微信图片")
	private String robotWxpic;
	@ApiModelProperty("公众号图片")
	private String publicPic;
	@ApiModelProperty("客服微信号")
	private String customerWxpic;
	@ApiModelProperty("备案号")
	private String icpn;
	@ApiModelProperty("隐私政策（富文本）")
	private String privacyPolicy;
	@ApiModelProperty("使用条款（富文本）")
	private String useTerms;
						

}