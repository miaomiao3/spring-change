package com.change.dto.resp;

import com.change.entity.BaseEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
* @author 坏孩子是你啊
* @date  2022-08-18
*/
@Data
public class QueryRobotResp extends BaseEntity {
    @ApiModelProperty("机器人名称")
    private String name;
    @ApiModelProperty("机器人微信号")
    private String wxAccount;
    @ApiModelProperty("密码")
    private String password;

}