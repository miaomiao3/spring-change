package com.change.dto.resp;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
* @author 坏孩子是你啊
* @date  2022-07-15
*/
@Data
public class QueryAccountInfoResp extends BaseResp {
    @ApiModelProperty("账号")
    private String account;
    @ApiModelProperty("真实姓名")
    private String realName;
    @ApiModelProperty("角色")
    private String role;
    @ApiModelProperty("部门")
    private Integer dept;
    @ApiModelProperty("密码加密")
    private String password;
    @ApiModelProperty("浏览器标识")
    private String browserUuid;
    @ApiModelProperty("手机号")
    private String mobile;
    @ApiModelProperty("邮箱地址")
    private String email;
    @ApiModelProperty("头像地址")
    private String avatar;
    @ApiModelProperty("昵称")
    private String nickName;
}