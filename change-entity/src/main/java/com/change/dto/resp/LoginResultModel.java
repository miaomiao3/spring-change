package com.change.dto.resp;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
public class LoginResultModel {

    @ApiModelProperty("用户标识")
    private Integer userId;
    private String realName;
    private String username;
    private String desc;
    @ApiModelProperty("登录token")
    private String token;

    @ApiModelProperty("用户角色")
    private List<RoleInfo> roles;

    @Data
    public static class RoleInfo {
        @ApiModelProperty("角色名称")
        private String roleName;
        @ApiModelProperty("角色内容")
        private String value;
    }
}
