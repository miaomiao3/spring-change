package com.change.dto.resp;

import io.swagger.annotations.ApiModelProperty;
import io.swagger.models.auth.In;
import lombok.Data;
import org.apache.kafka.common.protocol.types.Field;

/**
 * 基础返回数据
 */
@Data
public class BaseResp {

    // 数据隔离
    @ApiModelProperty("公司id")
    private Integer companyId;

    @ApiModelProperty("主键")
    private Integer id;

    @ApiModelProperty("创建时间")
    private String createTime;

    // 所属用户
    @ApiModelProperty("头像")
    private String avatar;

    @ApiModelProperty("昵称")
    private String nickName;

    @ApiModelProperty("介绍")
    private String intro;
}
