package com.change.dto.resp;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.security.Timestamp;

/**
* @author 路人甲
* @date  2022-08-21
*/
@Data
public class QuerySystemDepartmentResp {

    @ApiModelProperty("主键")
    private Integer id;
    @ApiModelProperty("名称")
    private String name;
    @ApiModelProperty("父id")
    private Integer pid;
    @ApiModelProperty("排序")
    private Integer sort;
    @ApiModelProperty("0启用1停用")
    private Integer status;
    @ApiModelProperty("备注")
    private String remark;
    @ApiModelProperty("创建时间")
    private String createTime;
    @ApiModelProperty("更新时间")
    private String updateTime;
    @ApiModelProperty("创建人ID")
    private Integer creatOrid;
    @ApiModelProperty("更新人ID")
    private Integer updateId;
    @ApiModelProperty("公司id")
    private Integer companyId;


}