package com.change.dto.result;

import lombok.Data;

@Data
public class WxConfigResult {

    private String url;
    private String jsapiTicket;
    private String nonceStr;
    private String timestamp;
    private String signature;
    private String appId;
}
