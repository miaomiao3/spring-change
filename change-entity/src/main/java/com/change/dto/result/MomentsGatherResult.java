package com.change.dto.result;

import cn.hutool.json.JSONArray;
import com.change.dto.params.BaseParams;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

/**
* @author 坏孩子是你啊
* @date  2022-10-06
*/
@Data
public class MomentsGatherResult extends BaseResult {
private static final long serialVersionUID = 1L;
	@ApiModelProperty("图片数组json")
	private String imageList;
	@ApiModelProperty("图片数组")
	private JSONArray imgList;
	@ApiModelProperty("坐标1")
	private Double latitude;
	@ApiModelProperty("坐标2")
	private Double longitude;
	@ApiModelProperty("标题")
	private String title;
	@ApiModelProperty("地址名称")
	private String name;
	@ApiModelProperty("地址")
	private String address;
	@ApiModelProperty("0 公开 1仅关注")
	private Integer whoType;
	@ApiModelProperty("内容")
	private String content;
	@ApiModelProperty("人数")
	private Integer counts;
	@ApiModelProperty("集合时间")
	private String gatherTime;
	@ApiModelProperty("1正常 2满员 3已结束")
	private Integer status;
						

}