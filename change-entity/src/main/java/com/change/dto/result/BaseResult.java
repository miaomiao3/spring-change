package com.change.dto.result;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class BaseResult {
    @ApiModelProperty("主键")
    private Integer id;
    @ApiModelProperty("公司主键")
    private Integer companyId;
    @ApiModelProperty("创建时间")
    private String createTime;
    @ApiModelProperty("头像")
    private String avatar;
    @ApiModelProperty("昵称")
    private String nickName;
    @ApiModelProperty("创建人")
    private Integer userId;
}
