package com.change.dto.result;

import cn.hutool.json.JSONArray;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
* @author 坏孩子是你啊
* @date  2022-10-04
*/
@Data
public class MomentsInfoResult extends BaseResult {
private static final long serialVersionUID = 1L;
	@ApiModelProperty("坐标1")
	private Double latitude;
	@ApiModelProperty("坐标2")
	private Double longitude;
	@ApiModelProperty("地址名称")
	private String name;
	@ApiModelProperty("地址")
	private String address;
	@ApiModelProperty("0 公开 1仅关注")
	private Integer whoType;
	@ApiModelProperty("内容")
	private String content;
	@ApiModelProperty("图片数组json")
	private String imageList;
	@ApiModelProperty("图片数组")
	private JSONArray imgList;
	@ApiModelProperty("喜欢")
	private Boolean isLike = false;
	@ApiModelProperty("赞赏")
	private Boolean isGiveReward = true;
	@ApiModelProperty("点赞数量")
	private Integer likeNumber = 0;
	@ApiModelProperty("打赏数量")
	private Integer giveRewardNumber = 0;
	@ApiModelProperty("评论数量")
	private Integer chatNumber = 0;
	@ApiModelProperty("是否关注")
	private Boolean isFocusOn = false;
}