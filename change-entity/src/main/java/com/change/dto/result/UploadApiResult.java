package com.change.dto.result;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 上传图片返回
 */
@Data
public class UploadApiResult {

    @ApiModelProperty("消息")
    private String message="成功";
    @ApiModelProperty("状态码")
    private Integer code = 0;
    @ApiModelProperty("地址")
    private String url;
}
