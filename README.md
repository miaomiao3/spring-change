### 项目名称：改变
> 从呱呱落地，到文质彬彬，或者吊儿郎当。人总在改变。或好，或坏。
>
> 互联网技术的改变，就像内存条一样，从128M到128G。这是一个质的飞跃。
>
> 人多力量大，一起改变吧！！！



### 技术栈

1. Spring Boot  2.5.9   快速构建微服务项目；
2. Mybatis-plus 3.4.3  快速和数据库进行数据通信；
3. hutool 5.7.21  各种骚操作的工具类；

### Docker 安装工具
#### kafka的安装命令:
```shell
docker run -d --name kafka -p 9002:9002 -e KAFKA_BROKER_ID=0 -e KAFKA_ZOOKEEPER_CONNECT=10.0.8.9:2181/kafka -e KAFKA_ADVERTISED_LISTENERS=PLAINTEXT://43.138.198.140:9002 -e KAFKA_LISTENERS=PLAINTEXT://0.0.0.0:9002 -v /etc/localtime:/etc/localtime wurstmeister/kafka:2.12-2.5.0
```