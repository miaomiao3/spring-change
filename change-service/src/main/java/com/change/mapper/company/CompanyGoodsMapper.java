package com.change.mapper.company;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.change.core.page.Page;
import com.change.dto.req.QueryCompanyGoodsReq;
import com.change.dto.resp.QueryCompanyGoodsResp;
import com.change.entity.company.CompanyGoods;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author jhlz
 * @date 2022-08-30
 */
@Repository
public interface CompanyGoodsMapper extends BaseMapper<CompanyGoods> {

    List<QueryCompanyGoodsResp> findGoodsByPage(@Param("page") Page page,
                                                @Param("req") QueryCompanyGoodsReq req);

    long findGoodsTotal(@Param("req") QueryCompanyGoodsReq req);
}