package com.change.mapper.company;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.change.core.page.Page;
import com.change.dto.req.QueryCompanyWebsiteReq;
import com.change.dto.resp.QueryCompanyWebsiteResp;
import com.change.entity.company.CompanyWebsite;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
* @author 路人甲
* @date  2022-08-30
*/
@Repository
public interface CompanyWebsiteMapper extends BaseMapper<CompanyWebsite> {

    List<QueryCompanyWebsiteResp> pageCompany(Page page, QueryCompanyWebsiteReq req);

    long pageCompanyTotal(QueryCompanyWebsiteReq req);

    QueryCompanyWebsiteResp selectWebsiteByCompanyId(@Param("companyId") Integer companyId);
}