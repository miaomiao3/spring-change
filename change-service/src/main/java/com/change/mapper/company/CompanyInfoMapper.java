package com.change.mapper.company;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.change.core.page.Page;
import com.change.dto.req.QueryCompanyInfoReq;
import com.change.dto.resp.QueryCompanyInfoResp;
import com.change.entity.company.CompanyInfo;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
* @author 坏孩子是你啊
* @date  2022-08-15
*/
@Repository
public interface CompanyInfoMapper extends BaseMapper<CompanyInfo> {
    QueryCompanyInfoResp detailCompanyById(@Param("id") Integer id);

    /**
     * 分页获取审核通过并且正常经营的公司信息
     * @param page  Page
     * @param req   QueryCompanyInfoReq
     * @return      QueryCompanyInfoReq集合
     */
    List<QueryCompanyInfoReq> pageCompany(@Param("page") Page page,
                                          @Param("req") QueryCompanyInfoReq req);

    /**
     * 获取公司记录总数
     * @param req   QueryCompanyInfoReq
     * @return      记录总数
     */
    long pageCompanyTotal(@Param("req") QueryCompanyInfoReq req);

    @Select("select count(id) from company_info where company_name = #{companyName} ")
    Integer queryCountsByCompanyName(@Param("companyName") String companyName);
}