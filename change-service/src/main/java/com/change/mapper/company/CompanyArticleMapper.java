package com.change.mapper.company;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.change.core.page.Page;
import com.change.dto.req.QueryCompanyArticleReq;
import com.change.dto.resp.QueryCompanyArticleResp;
import com.change.entity.company.CompanyArticle;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
* @author 坏孩子是你啊
* @date  2022-08-30
*/
@Repository
public interface CompanyArticleMapper extends BaseMapper<CompanyArticle> {

    List<QueryCompanyArticleResp> pageList(@Param("page") Page page,@Param("req") QueryCompanyArticleReq req);

    long pageListTotal(@Param("req") QueryCompanyArticleReq req);

    QueryCompanyArticleResp detailById(@Param("id") Integer id);
}