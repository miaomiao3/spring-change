package com.change.mapper.article;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.change.core.page.Page;
import com.change.dto.req.QueryArticleInfoReq;
import com.change.dto.resp.QueryArticleInfoResp;
import com.change.entity.article.ArticleInfo;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
* @author 坏孩子是你啊
* @date  2022-07-24
*/
@Repository
public interface ArticleInfoMapper extends BaseMapper<ArticleInfo> {

    @Select("SELECT id,title,content,tags,create_time from article_info where id = #{id} and deleted = 0 order by create_time desc")
    QueryArticleInfoResp detailArticleById(@Param("id") Integer id);

    List<QueryArticleInfoResp> pageArticle(@Param("page") Page page,@Param("req") QueryArticleInfoReq req);

    long pageArticleTotal(@Param("req") QueryArticleInfoReq req);
}