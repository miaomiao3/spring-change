package com.change.mapper.weather;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.change.entity.weather.Weather;
import org.springframework.stereotype.Repository;

/**
* @author 坏孩子是你啊
* @date  2022-08-18
*/
@Repository
public interface WeatherMapper extends BaseMapper<Weather> {

    /**
     * 获取最新一条记录
     * @return
     */
    Weather getFirstByCreateTime();
}