package com.change.mapper.system;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.change.core.page.Page;
import com.change.dto.req.QuerySystemDepartmentReq;
import com.change.dto.resp.QuerySystemDepartmentResp;
import com.change.entity.system.SystemDepartment;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
* @author 路人甲
* @date  2022-08-21
*/
@Repository
public interface SystemDepartmentMapper extends BaseMapper<SystemDepartment> {



    List<QuerySystemDepartmentResp> pagesystem(@Param("page") Page page,
                                               @Param("req") QuerySystemDepartmentReq req);

    List<QuerySystemDepartmentResp> pageforChildren(@Param("resp") QuerySystemDepartmentResp resp);

    List<QuerySystemDepartmentResp> pagesforPid(@Param("page") Page page,
                                               @Param("req") QuerySystemDepartmentReq req);

    long pageSystemTotal(@Param("req") QuerySystemDepartmentReq req);

    @Update("Update system_department set deleted = 1 where pid = #{id}")
    int deleteChildren(@Param("id") int id);


    List<QuerySystemDepartmentResp> getDeptList(@Param("req") QuerySystemDepartmentReq req);
}