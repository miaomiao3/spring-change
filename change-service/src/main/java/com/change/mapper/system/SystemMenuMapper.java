package com.change.mapper.system;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.change.dto.req.QuerySystemMenuReq;
import com.change.dto.resp.QuerySystemMenuResp;
import com.change.entity.system.SystemMenu;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author jhlz
 * @date 2022-08-27
 */
@Repository
public interface SystemMenuMapper extends BaseMapper<SystemMenu> {

    List<QuerySystemMenuResp> getMenuList(@Param("req") QuerySystemMenuReq req);
}