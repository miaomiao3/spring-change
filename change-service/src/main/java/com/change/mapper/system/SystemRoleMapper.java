package com.change.mapper.system;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.change.core.page.Page;
import com.change.dto.req.QuerySystemRoleReq;
import com.change.dto.resp.QuerySystemRoleResp;
import com.change.entity.system.SystemRole;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author jhlz
 * @date 2022-08-27
 */
@Repository
public interface SystemRoleMapper extends BaseMapper<SystemRole> {

    long findRoleTotal(@Param("req") QuerySystemRoleReq req);

    List<QuerySystemRoleResp> findRolePage(@Param("page") Page page,
                                           @Param("req") QuerySystemRoleReq req);
}