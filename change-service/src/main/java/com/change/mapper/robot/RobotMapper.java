package com.change.mapper.robot;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.change.core.page.Page;
import com.change.dto.req.QueryRobotReq;
import com.change.dto.resp.QueryRobotResp;
import com.change.entity.robot.Robot;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
* @author 坏孩子是你啊
* @date  2022-08-18
*/
@Repository
public interface RobotMapper extends BaseMapper<Robot> {

    List<QueryRobotResp> pageRobots(Page page, QueryRobotReq req);

    long pageRobotTotal(QueryRobotReq req);
}