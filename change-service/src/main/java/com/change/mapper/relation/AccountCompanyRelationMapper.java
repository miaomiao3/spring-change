package com.change.mapper.relation;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.change.entity.relation.AccountCompanyRelation;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * @author jhlz
 * @time 2022/8/22 15:19
 * @desc: AccountCompanyRelationMapper
 */
@Repository
public interface AccountCompanyRelationMapper extends BaseMapper<AccountCompanyRelation> {

    /**
     * 根据 accountId 和 companyId 查找关联关系
     * @param accountId
     * @param companyId
     * @return
     */
    AccountCompanyRelation queryRelationByIds(@Param("accountId") Integer accountId,
                                              @Param("companyId") Integer companyId);
}
