package com.change.mapper.moments;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.change.core.page.Page;
import com.change.dto.params.MomentsInfoParams;
import com.change.dto.result.MomentsInfoResult;
import com.change.entity.moments.MomentsInfo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
* @author 坏孩子是你啊
* @date  2022-10-04
*/
@Repository
public interface MomentsInfoMapper extends BaseMapper<MomentsInfo> {

    List<MomentsInfoResult> pageList(@Param("page") Page page,@Param("req") MomentsInfoParams req);

    long pageListTotal(@Param("req") MomentsInfoParams req);
}