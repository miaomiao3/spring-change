package com.change.mapper.moments;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.change.core.page.Page;
import com.change.dto.params.MomentsGatherParams;
import com.change.dto.result.MomentsGatherResult;
import com.change.entity.moments.MomentsGather;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
* @author 坏孩子是你啊
* @date  2022-10-06
*/
@Repository
public interface MomentsGatherMapper extends BaseMapper<MomentsGather> {

    List<MomentsGatherResult> pageList(@Param("page") Page page,@Param("req") MomentsGatherParams req);

    long pageListTotal(@Param("req") MomentsGatherParams req);
}