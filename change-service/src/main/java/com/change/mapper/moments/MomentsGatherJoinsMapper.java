package com.change.mapper.moments;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.change.entity.moments.MomentsGatherJoins;
import org.springframework.stereotype.Repository;

/**
* @author 坏孩子是你啊
* @date  2022-10-06
*/
@Repository
public interface MomentsGatherJoinsMapper extends BaseMapper<MomentsGatherJoins> {

}