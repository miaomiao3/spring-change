package com.change.mapper.friends;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.change.core.page.Page;
import com.change.dto.req.QueryFriendsReq;
import com.change.dto.resp.QueryFriendsResp;
import com.change.entity.friends.Friends;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
* @author 坏孩子是你啊
* @date  2022-08-18
*/
@Repository
public interface FriendsMapper extends BaseMapper<Friends> {

    List<QueryFriendsResp> pageFriends(Page page, QueryFriendsReq req);

    long pageFriendsTotal(QueryFriendsReq req);
}