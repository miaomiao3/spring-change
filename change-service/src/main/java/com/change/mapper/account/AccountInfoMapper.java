package com.change.mapper.account;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.change.core.page.Page;
import com.change.dto.req.QueryAccountInfoReq;
import com.change.dto.req.RegisterReq;
import com.change.dto.resp.QueryAccountInfoResp;
import com.change.dto.resp.UserInfoResultModel;
import com.change.dto.resp.UserLoginResp;
import com.change.entity.account.AccountInfo;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
* @author 坏孩子是你啊
* @date  2022-07-15
*/
@Repository
public interface AccountInfoMapper extends BaseMapper<AccountInfo> {

    UserLoginResp selectOneByAccountInfo(@Param("req") RegisterReq req);

    AccountInfo selectOneByBrowserUuid(@Param("browserUuid") String browserUuid);

    /**
     * 分页查询账号信息
     * @param page
     * @param req
     * @return
     */
    List<QueryAccountInfoResp> findAccountInfoPage(@Param("page") Page page,
                                                   @Param("req") QueryAccountInfoReq req);

    /**
     * 获取账号总数
     * @param req
     * @return
     */
    long findAccountInfoPageTotal(@Param("req") QueryAccountInfoReq req);

    @Select("select count(id) from account_info where account = #{account} ")
    Integer queryCountsByAccount(@Param("account") String account);

    UserInfoResultModel getUserInfoFromToken(@Param("userId") Integer userId);
}