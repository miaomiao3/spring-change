package com.change.mapper.account;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.change.core.page.Page;
import com.change.dto.req.QueryAccountWechatReq;
import com.change.dto.resp.QueryAccountWechatResp;
import com.change.entity.account.AccountWechat;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
* @author 坏孩子是你啊
* @date  2022-07-27
*/
@Repository
public interface AccountWechatMapper extends BaseMapper<AccountWechat> {

    @Select("SELECT * from account_wechat WHERE wx_no = #{wxNo} ")
    AccountWechat findAccountWechatByWxNo(@Param("wxNo") String wxNo);

    List<QueryAccountWechatResp> findAccountWechatPage(@Param("page") Page page,
                                                       @Param("req") QueryAccountWechatReq req);

    long findAccountWechatPageTotal(@Param("req") QueryAccountWechatReq req);

    QueryAccountWechatResp detailByWxNo(@Param("wxNo") String wxNo);
}