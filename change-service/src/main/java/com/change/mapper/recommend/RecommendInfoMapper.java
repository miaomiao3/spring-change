package com.change.mapper.recommend;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.change.core.page.Page;
import com.change.dto.req.QueryRecommendInfoReq;
import com.change.dto.resp.QueryRecommendInfoResp;
import com.change.entity.recommend.RecommendInfo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
* @author 坏孩子是你啊
* @date  2022-09-04
*/
@Repository
public interface RecommendInfoMapper extends BaseMapper<RecommendInfo> {

    List<QueryRecommendInfoResp> pageRecommendInfo(@Param("page") Page page,
                                                   @Param("req") QueryRecommendInfoReq req);

    long pageRecommendInfoTotal(@Param("req") QueryRecommendInfoReq req);
}