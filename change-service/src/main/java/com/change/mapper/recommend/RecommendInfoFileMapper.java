package com.change.mapper.recommend;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.change.entity.recommend.RecommendInfoFile;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
* @author 坏孩子是你啊
* @date  2022-09-05
*/
@Repository
public interface RecommendInfoFileMapper extends BaseMapper<RecommendInfoFile> {

    @Select("select id from recommend_info_file where rec_info_id = #{recInfoId} and deleted = 0;")
    List<Integer> queryIdListByRecInfoId(@Param("recInfoId") Integer recInfoId);

    List<Integer> queryIdListByUrls(@Param("recInfoId") Integer recInfoId,
                                    @Param("urls") String[] urls);

    void updateDeleteByRecInfoId(@Param("recInfoId") Integer recInfoId);
}