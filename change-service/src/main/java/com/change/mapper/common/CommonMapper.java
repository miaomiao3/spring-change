package com.change.mapper.common;

import com.baomidou.mybatisplus.annotation.InterceptorIgnore;
import org.apache.ibatis.annotations.MapKey;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public interface CommonMapper {

    @MapKey("id")
    @InterceptorIgnore(tenantLine = "true")
    List<Map<Integer, String>> queryList(@Param("sql") String sql);

    Integer updateSql(@Param("sql") String sql);

    @InterceptorIgnore(tenantLine = "true")
    Integer queryCount(String querySql);
}
