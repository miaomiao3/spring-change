package com.change.service.relation;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.change.entity.relation.AccountCompanyRelation;
import com.change.mapper.relation.AccountCompanyRelationMapper;
import org.springframework.stereotype.Service;

/**
 * @author jhlz
 * @time 2022/8/22 15:19
 * @desc: AccountCompanyRelationService
 */
@Service
public class AccountCompanyRelationService extends ServiceImpl<AccountCompanyRelationMapper, AccountCompanyRelation> {

}
