package com.change.service.system;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.lang.tree.Tree;
import cn.hutool.core.lang.tree.TreeNodeConfig;
import cn.hutool.core.lang.tree.TreeUtil;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.change.dto.req.QuerySystemDepartmentReq;
import com.change.dto.req.SaveSystemDepartmentReq;
import com.change.dto.resp.QuerySystemDepartmentResp;
import com.change.entity.system.SystemDepartment;
import com.change.mapper.system.SystemDepartmentMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
* @author 路人甲
* @date  2022-08-21
*/
@Slf4j
@Service
public class SystemDepartmentService extends ServiceImpl<SystemDepartmentMapper, SystemDepartment>{
    private final SystemDepartmentMapper systemDepartmentMapper;

    public SystemDepartmentService(SystemDepartmentMapper systemDepartmentMapper) {
    this.systemDepartmentMapper = systemDepartmentMapper;
    }

    /**
     * 分页查询
     * @param req
     * @return
     */
    public List<Tree<String>> getDeptList(QuerySystemDepartmentReq req) {
        //hutool的tree工具
        List<QuerySystemDepartmentResp> lists = CollUtil.newArrayList();
        //father
        lists = this.systemDepartmentMapper.getDeptList(req);
        System.out.println(lists);
        //children
        for(int i =0;i <= lists.size()-1;i++){
            List<QuerySystemDepartmentResp> lists1 = this.systemDepartmentMapper.pageforChildren(lists.get(i));
            lists.addAll(lists1);
        }
        TreeNodeConfig config = new TreeNodeConfig();
        config.setIdKey("id");
        //tree深度
        config.setDeep(2);
        List<Tree<String>> treeDept = TreeUtil.build(lists, "0", config, (node, tree) -> {
            tree.setId(node.getId().toString());
            tree.setName(node.getName());
            tree.setParentId(node.getPid().toString());
            // 额外的值
            tree.put("deptName",node.getName());
            tree.put("status", node.getStatus().toString());
            tree.put("orderNo", node.getSort());
            tree.put("remark", node.getRemark());
            tree.put("createTime", node.getCreateTime());
            tree.put("parentDept", node.getPid().toString());
        });
        return treeDept;
    }

    /**
     * 详细
     * @param id
     * @return
     */
    public QuerySystemDepartmentResp detail(Integer id) {
         SystemDepartment queryCount = this.systemDepartmentMapper.selectById(id);
         QuerySystemDepartmentResp querySystemDepartmentResp = new QuerySystemDepartmentResp();
         BeanUtil.copyProperties(queryCount,querySystemDepartmentResp);
         return querySystemDepartmentResp;
    }



    /**
     * 增加
     * @param req
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public SystemDepartment saveOrUpdate(SaveSystemDepartmentReq req) {
        SystemDepartment department = new SystemDepartment();
        department.setId(req.getId());
        System.out.println(req.getId());
        department.setPid(req.getParentDept()==null?0:req.getParentDept());// 上级
        department.setName(req.getDeptName()); // 部门名称
        department.setStatus(req.getStatus()); // 状态
        department.setSort(req.getOrderNo()); // 排序
        department.setRemark(req.getRemark());// 备注
        this.saveOrUpdate(department);
        return department;
    }


    /**
     * 更新(根据id)
     * @param req
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public SystemDepartment update(SaveSystemDepartmentReq req) {
        SystemDepartment systemDepartment = this.systemDepartmentMapper.selectById(req.getId());
        BeanUtil.copyProperties(req, systemDepartment);
        this.systemDepartmentMapper.updateById(systemDepartment);
        return systemDepartment;
    }

    /**
     * 删除
     * @param id
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public Integer delete(Integer id) {
        int a = this.systemDepartmentMapper.deleteById(id);
        int b = this.systemDepartmentMapper.deleteChildren(id);
        return a+b;
    }
}
