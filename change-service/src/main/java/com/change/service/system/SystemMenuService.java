package com.change.service.system;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.lang.tree.Tree;
import cn.hutool.core.lang.tree.TreeNodeConfig;
import cn.hutool.core.lang.tree.TreeUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.change.core.page.Page;
import com.change.dto.req.QuerySystemMenuReq;
import com.change.dto.req.SaveSystemMenuReq;
import com.change.dto.resp.QuerySystemDepartmentResp;
import com.change.dto.resp.QuerySystemMenuResp;
import com.change.entity.system.SystemMenu;
import com.change.exception.BizException;
import com.change.exception.ResultStatus;
import com.change.mapper.system.SystemMenuMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * @author jhlz
 * @date 2022-08-27
 */
@Slf4j
@Service
public class SystemMenuService extends ServiceImpl<SystemMenuMapper, SystemMenu> {
    private final SystemMenuMapper sysMenuMapper;

    public SystemMenuService(SystemMenuMapper sysMenuMapper) {
        this.sysMenuMapper = sysMenuMapper;
    }

    public Page<List<QuerySystemMenuResp>> getPage(Page page, QuerySystemMenuReq req) {

        return page;
    }

    public QuerySystemMenuResp detail(Integer id) {
        SystemMenu sysMenu = this.sysMenuMapper.selectById(id);
        SystemMenu menuDetail = Optional.ofNullable(sysMenu)
                .orElseThrow(() -> new BizException(ResultStatus.Common.NOT_FOUND));
        QuerySystemMenuResp resp = new QuerySystemMenuResp();
        BeanUtil.copyProperties(menuDetail, resp);
        return resp;
    }

    /**
     * 保存或更新菜单
     * @param req
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public SystemMenu saveOrUpdateMenu(SaveSystemMenuReq req) {
        SystemMenu menu = new SystemMenu();
        BeanUtil.copyProperties(req, menu);
        menu.setVisible(req.getShow());
        boolean bool = this.saveOrUpdate(menu);
        return menu;
    }

    @Transactional(rollbackFor = Exception.class)
    public SystemMenu update(SaveSystemMenuReq req) {
        SystemMenu menu = sysMenuMapper.selectById(req.getId());
        BeanUtil.copyProperties(req, menu);
        menu.setVisible(req.getShow());
        this.sysMenuMapper.updateById(menu);
        return menu;
    }

    @Transactional(rollbackFor = Exception.class)
    public Integer delete(Integer id) {
        return this.sysMenuMapper.deleteById(id);
    }

    /**
     * 菜单树列表渲染
     * @param req
     * @return
     */
    public List<Tree<Integer>> getMenuTreeList(QuerySystemMenuReq req) {
        //hutool的tree工具
        List<QuerySystemMenuResp> lists = this.sysMenuMapper.getMenuList(req);
        TreeNodeConfig config = new TreeNodeConfig();
        config.setIdKey("id");
        config.setParentIdKey("parentMenu");
        config.setChildrenKey("children");
        config.setWeightKey("orderNo");
        // 构建树
        List<Tree<Integer>> treeMenus = TreeUtil.build(lists, 0, config, (node, tree) -> {
            tree.setId(node.getId());
            tree.setName(node.getMenuName());
            tree.setParentId(node.getParentMenu());
            // 额外的值
            tree.put("icon",node.getIcon());
            tree.put("component", node.getComponent());
            tree.put("type", node.getType());
            tree.put("menuName", node.getMenuName());
            tree.put("permission", node.getPermission());
            tree.put("orderNo", node.getOrderNo());
            tree.put("createTime", node.getCreateTime());
            tree.put("status", node.getStatus());
            tree.put("parentMenu", node.getParentMenu());
            tree.put("routePath", node.getRoutePath());
            tree.put("isExt", node.getIsExt());
            tree.put("keepLive", node.getKeepLive());
            tree.put("show", node.getVisible());
            tree.put("status", node.getStatus());
        });
        return treeMenus;
    }
}
