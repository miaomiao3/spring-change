package com.change.service.moments;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.convert.Convert;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.change.core.page.Page;
import com.change.dto.params.MomentsInfoParams;
import com.change.dto.resp.QueryCompanyArticleResp;
import com.change.dto.result.MomentsInfoResult;
import com.change.entity.moments.MomentsInfo;
import com.change.mapper.moments.MomentsInfoMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
* @author 坏孩子是你啊
* @date  2022-10-04
*/
@Slf4j
@Service
public class MomentsInfoService extends ServiceImpl<MomentsInfoMapper, MomentsInfo>{
    private final MomentsInfoMapper momentsInfoMapper;

    public MomentsInfoService(MomentsInfoMapper momentsInfoMapper) {
    this.momentsInfoMapper = momentsInfoMapper;
    }

    public Page<List<MomentsInfoResult>> getPage(Page page, MomentsInfoParams req) {
        page.offset();
        // 数据
        List<MomentsInfoResult> items = momentsInfoMapper.pageList(page,req);
        items.stream().forEach(item -> {
            String imageList = item.getImageList();
            JSONArray objects = JSONUtil.parseArray(imageList);
            item.setImgList(objects);
        });
        page.setItems(items);
        // 总数
        long total = momentsInfoMapper.pageListTotal(req);
        page.setTotal(total);
        return page;
    }

    public MomentsInfoParams detail(Integer id) {
         return new MomentsInfoParams();
    }

    @Transactional(rollbackFor = Exception.class)
    public MomentsInfo saveOrUpdate(MomentsInfoParams req) {
        MomentsInfo momentsInfo = new MomentsInfo();
        BeanUtil.copyProperties(req,momentsInfo,"imageList");
        // 图片数组转换
        String imageJson = JSONUtil.toJsonStr(req.getImageList());
        momentsInfo.setImageList(imageJson);
        this.saveOrUpdate(momentsInfo);
        return momentsInfo;
    }

    @Transactional(rollbackFor = Exception.class)
    public MomentsInfo update(MomentsInfoParams req) {
         return null;
    }

    @Transactional(rollbackFor = Exception.class)
    public Integer delete(Integer id) {
        return null;
    }
}
