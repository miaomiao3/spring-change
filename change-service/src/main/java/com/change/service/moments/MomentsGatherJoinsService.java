package com.change.service.moments;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.change.core.page.Page;
import com.change.dto.params.MomentsGatherJoinsParams;
import com.change.entity.moments.MomentsGatherJoins;
import com.change.mapper.moments.MomentsGatherJoinsMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
* @author 坏孩子是你啊
* @date  2022-10-06
*/
@Slf4j
@Service
public class MomentsGatherJoinsService extends ServiceImpl<MomentsGatherJoinsMapper, MomentsGatherJoins>{
    private final MomentsGatherJoinsMapper momentsGatherJoinsMapper;

    public MomentsGatherJoinsService(MomentsGatherJoinsMapper momentsGatherJoinsMapper) {
    this.momentsGatherJoinsMapper = momentsGatherJoinsMapper;
    }

    public Page<List<MomentsGatherJoinsParams>> getPage(Page page, MomentsGatherJoinsParams req) {

        return page;
    }

    public MomentsGatherJoinsParams detail(Integer id) {
         return new MomentsGatherJoinsParams();
    }

    @Transactional(rollbackFor = Exception.class)
    public MomentsGatherJoins saveOrUpdate(MomentsGatherJoinsParams req) {

        return null;
    }

    @Transactional(rollbackFor = Exception.class)
    public MomentsGatherJoins update(MomentsGatherJoinsParams req) {
         return null;
    }

    @Transactional(rollbackFor = Exception.class)
    public Integer delete(Integer id) {
        return null;
    }
}
