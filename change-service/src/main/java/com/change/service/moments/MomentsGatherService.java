package com.change.service.moments;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.change.core.page.Page;
import com.change.dto.params.MomentsGatherParams;
import com.change.dto.result.MomentsGatherResult;
import com.change.dto.result.MomentsInfoResult;
import com.change.entity.moments.MomentsGather;
import com.change.entity.moments.MomentsInfo;
import com.change.mapper.moments.MomentsGatherMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
* @author 坏孩子是你啊
* @date  2022-10-06
*/
@Slf4j
@Service
public class MomentsGatherService extends ServiceImpl<MomentsGatherMapper, MomentsGather>{
    private final MomentsGatherMapper momentsGatherMapper;

    public MomentsGatherService(MomentsGatherMapper momentsGatherMapper) {
    this.momentsGatherMapper = momentsGatherMapper;
    }

    public Page<List<MomentsGatherResult>> getPage(Page page, MomentsGatherParams req) {
        page.offset();
        // 数据
        List<MomentsGatherResult> items = momentsGatherMapper.pageList(page,req);
        items.stream().forEach(item -> {
            String imageList = item.getImageList();
            JSONArray objects = JSONUtil.parseArray(imageList);
            item.setImgList(objects);
        });
        page.setItems(items);
        // 总数
        long total = momentsGatherMapper.pageListTotal(req);
        page.setTotal(total);
        return page;
    }

    public MomentsGatherParams detail(Integer id) {
         return new MomentsGatherParams();
    }

    @Transactional(rollbackFor = Exception.class)
    public MomentsGather saveOrUpdate(MomentsGatherParams req) {
        MomentsGather entity = new MomentsGather();
        BeanUtil.copyProperties(req,entity,"imageList");
        // 图片数组转换
        String imageJson = JSONUtil.toJsonStr(req.getImageList());
        entity.setImageList(imageJson);
        this.saveOrUpdate(entity);
        return entity;
    }

    @Transactional(rollbackFor = Exception.class)
    public MomentsGather update(MomentsGatherParams req) {
         return null;
    }

    @Transactional(rollbackFor = Exception.class)
    public Integer delete(Integer id) {
        return null;
    }
}
