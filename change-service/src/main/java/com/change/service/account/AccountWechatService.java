package com.change.service.account;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.change.core.page.Page;
import com.change.dto.req.QueryAccountWechatReq;
import com.change.dto.req.SaveAccountWechatReq;
import com.change.dto.resp.QueryAccountWechatResp;
import com.change.entity.account.AccountWechat;
import com.change.exception.BizException;
import com.change.exception.ResultStatus;
import com.change.mapper.account.AccountWechatMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author 坏孩子是你啊
 * @date 2022-07-27
 */
@Slf4j
@Service
public class AccountWechatService extends ServiceImpl<AccountWechatMapper, AccountWechat> {
    private final AccountWechatMapper accountWechatMapper;

    public AccountWechatService(AccountWechatMapper accountWechatMapper) {
        this.accountWechatMapper = accountWechatMapper;
    }

    public Page<List<QueryAccountWechatResp>> getPage(Page page, QueryAccountWechatReq req) {
        page.offset();
        List<QueryAccountWechatResp> records = this.accountWechatMapper.findAccountWechatPage(page,req);
        long total = this.accountWechatMapper.findAccountWechatPageTotal(req);
        // 封装结果
        page.setRecords(records);
        page.setTotal(total);
        return page;
    }

    public QueryAccountWechatResp detail(Integer id) {
        return new QueryAccountWechatResp();
    }

    @Transactional(rollbackFor = Exception.class)
    public AccountWechat save(SaveAccountWechatReq req) {
        AccountWechat accountWechatSave = new AccountWechat();
        BeanUtil.copyProperties(req,accountWechatSave);
        // 1. 通过微信号查询是否已经存在
        String wxNo = req.getWxNo();
        AccountWechat accountWechat = this.accountWechatMapper.findAccountWechatByWxNo(wxNo);
        if(accountWechat!=null){
            log.info("数据库已存在微信号:{}",wxNo);
            return accountWechat;
        }else{
            log.info("成功新增微信号：{}",wxNo);
            this.accountWechatMapper.insert(accountWechatSave);
        }
        return accountWechatSave;
    }

    @Transactional(rollbackFor = Exception.class)
    public AccountWechat update(SaveAccountWechatReq req) {
        return null;
    }

    @Transactional(rollbackFor = Exception.class)
    public Integer delete(Integer id) {
        return null;
    }

    /**
     * 批量新增微信好友
     * @param req
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public boolean saveBatchWechat(List<SaveAccountWechatReq> req) {
        if(req==null || req.size() == 0){
            throw new BizException(ResultStatus.Common.NOT_FOUNT_WECHAT_LIST);
        }else{
            req = req.stream().distinct().collect(Collectors.toList());
        }
        // 1. 新增微信好友列表
        List<AccountWechat> accountWechatList = new ArrayList<>();
        req.stream().forEach(item -> {
            AccountWechat accountWechatSave = new AccountWechat();
            String wxNo = item.getWxNo();
            AccountWechat accountWechat = this.accountWechatMapper.findAccountWechatByWxNo(wxNo);
            if(accountWechat==null){
                BeanUtil.copyProperties(item,accountWechatSave);
                accountWechatList.add(accountWechatSave);
            }else{
                log.info("数据库已存在微信号:{}",wxNo);
            }
        });
        // 2. 进行批量新增
        log.info("批量新增好友数量为：{}",accountWechatList.size());
        return this.saveBatch(accountWechatList);
    }

    /**
     * 通过微信号查找
     * @param wxNo
     * @return
     */
    public QueryAccountWechatResp detailByWxNo(String wxNo) {
        return this.accountWechatMapper.detailByWxNo(wxNo);
    }
}
