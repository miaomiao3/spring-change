package com.change.service.account;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONUtil;
import cn.hutool.jwt.JWTPayload;
import cn.hutool.jwt.JWTUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.change.core.constant.Constants;
import com.change.core.dto.Account;
import com.change.core.page.Page;
import com.change.core.web.request.AccountContext;
import com.change.dto.params.ChangePwdParams;
import com.change.dto.params.RegisterParams;
import com.change.dto.req.QueryAccountInfoReq;
import com.change.dto.req.RegisterReq;
import com.change.dto.req.SaveAccountInfoReq;
import com.change.dto.resp.LoginResultModel;
import com.change.dto.resp.QueryAccountInfoResp;
import com.change.dto.resp.UserInfoResultModel;
import com.change.dto.resp.UserLoginResp;
import com.change.entity.account.AccountInfo;
import com.change.entity.company.CompanyInfo;
import com.change.entity.relation.AccountCompanyRelation;
import com.change.exception.BizException;
import com.change.exception.ResultStatus;
import com.change.mapper.account.AccountInfoMapper;
import com.change.mapper.company.CompanyInfoMapper;
import com.change.mapper.relation.AccountCompanyRelationMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;

/**
* @author 坏孩子是你啊
* @date  2022-07-15
*/
@Slf4j
@Service
public class AccountInfoService extends ServiceImpl<AccountInfoMapper, AccountInfo>{
    private final AccountInfoMapper accountInfoMapper;
    private final CompanyInfoMapper companyInfoMapper;
    private final AccountCompanyRelationMapper relationMapper;


    public AccountInfoService(AccountInfoMapper accountInfoMapper,
                              CompanyInfoMapper companyInfoMapper,
                              AccountCompanyRelationMapper relationMapper) {
        this.accountInfoMapper = accountInfoMapper;
        this.companyInfoMapper = companyInfoMapper;
        this.relationMapper = relationMapper;
    }

    public Page<List<QueryAccountInfoResp>> getPage(Page page, QueryAccountInfoReq req) {
        page.offset();
        // token获取公司id ，set 到 req参数中 todo
        req.setCompanyId(0);
        List<QueryAccountInfoResp> items = this.accountInfoMapper.findAccountInfoPage(page, req);
        long total = this.accountInfoMapper.findAccountInfoPageTotal(req);
        page.setItems(items);
        page.setTotal(total);
        return page;
    }

    public QueryAccountInfoResp detail(Integer id) {
        AccountInfo accountInfo =  this.accountInfoMapper.selectById(id);
        QueryAccountInfoResp resp = new QueryAccountInfoResp();
        BeanUtil.copyProperties(accountInfo, resp);
        return resp;
    }

    @Transactional(rollbackFor = Exception.class)
    public AccountInfo save(SaveAccountInfoReq req) {

        return null;
    }

    @Transactional(rollbackFor = Exception.class)
    public AccountInfo update(SaveAccountInfoReq req) {
         return null;
    }

    @Transactional(rollbackFor = Exception.class)
    public Integer delete(Integer id) {
        return null;
    }

    /**
     * 注册会员
     * @param req
     * @return
     */
    public Integer register(RegisterParams req) {
        // 校验: 账号是否存在
        String account = req.getAccount();
        Integer a_counts = this.accountInfoMapper.queryCountsByAccount(account);
        if(a_counts>0){
            throw new BizException(ResultStatus.Auth.ERROR_ACCOUNT_EXIST);
        }
        // 新增
        AccountInfo accountInfo = new AccountInfo();
        accountInfo.setAccount(req.getAccount());
        accountInfo.setPassword(req.getPassword());
        accountInfo.setBrowserUuid(req.getBrowserUuid());
        accountInfo.setRole("admin");
        accountInfoMapper.insert(accountInfo);
        return accountInfo.getId();
    }

    /**
     * 账号密码登录
     * @param req
     * @return
     */
    public LoginResultModel login(RegisterReq req) {
        // 返回封装登陆对象
        LoginResultModel resultModel = new LoginResultModel();

        UserLoginResp userLoginResp = accountInfoMapper.selectOneByAccountInfo(req);
        if(null == userLoginResp){
            throw new BizException(ResultStatus.Common.ERROR_PASSWORD_ACCOUNT);
        }
//        UserLoginResp loginResp = getUserLoginResp(userLoginResp);

        Map<String, Object> claims = new HashMap<>();
        LocalDateTime now = LocalDateTime.now();
        // 生效时间
        claims.put(JWTPayload.NOT_BEFORE, now);
        // 过期时间：7 天
        claims.put(JWTPayload.EXPIRES_AT, now.plusDays(7));
        claims.put("uuid", userLoginResp.getBrowserUuid());
        // 用户id 公司id todo
        claims.put(JWTPayload.AUDIENCE,userLoginResp.getId());
        // 生成 token
        String token = JWTUtil.createToken(claims, Constants.ANONYMOUS_LOGIN_USER_KEY.getBytes());

        // 赋值
        resultModel.setUserId(userLoginResp.getId());
        resultModel.setRealName("张飞");
        resultModel.setUsername(userLoginResp.getUsername());
        resultModel.setToken(token);
        // 角色todo
        List<LoginResultModel.RoleInfo> roleInfoList = new ArrayList<>();
        LoginResultModel.RoleInfo roleInfo = new LoginResultModel.RoleInfo();
        roleInfo.setRoleName("Super Admin");
        roleInfo.setValue("super");
        roleInfoList.add(roleInfo);
        resultModel.setRoles(roleInfoList);
        return resultModel;
    }

    /**
     * 匿名登录：浏览器标识唯一判断
     * @param req
     * @return
     */
    public LoginResultModel anonymousLogin(RegisterReq req) {
        // 返回封装登陆对象
        LoginResultModel resultModel = new LoginResultModel();

        String browserUuid = req.getBrowserUuid();
        if(StrUtil.isBlank(browserUuid)){
            throw new BizException(ResultStatus.Common.NOT_FOUNT_BROWSER_UUID);
        }
        // 1. 获取数据库account_info by browser_uuid
        AccountInfo accountInfo = accountInfoMapper.selectOneByBrowserUuid(req.getBrowserUuid());
        // 2. 不存在,进行初始化数据
        if(accountInfo == null){
            accountInfo = new AccountInfo();
            accountInfo.setAccount(browserUuid);
            accountInfo.setPassword("123456");
            accountInfo.setBrowserUuid(browserUuid);
            accountInfo.setRole("good");
            accountInfoMapper.insert(accountInfo);
        }
        Integer userId = accountInfo.getId();
        UserLoginResp userLoginResp = new UserLoginResp();
        userLoginResp.setId(userId);
        userLoginResp.setUsername(browserUuid);

        Map<String, Object> claims = new HashMap<>();
        claims.put("uuid", accountInfo.getBrowserUuid());
        LocalDateTime now = LocalDateTime.now();
        // 生效时间
        claims.put(JWTPayload.NOT_BEFORE, now);
        // 过期时间：7天
        claims.put(JWTPayload.EXPIRES_AT, now.plusDays(7));
        // 一次性 token
        claims.put(JWTPayload.JWT_ID, Instant.now().getEpochSecond());
        // 用户id  公司id todo
        claims.put(JWTPayload.AUDIENCE,userId);
        String token = JWTUtil.createToken(claims, Constants.ANONYMOUS_LOGIN_USER_KEY.getBytes());

        // 赋值
        resultModel.setUserId(userLoginResp.getId());
        resultModel.setRealName("张飞");
        resultModel.setUsername(userLoginResp.getUsername());
        resultModel.setToken(token);
        // 角色 todo
        List<LoginResultModel.RoleInfo> roleInfoList = new ArrayList<>();
        LoginResultModel.RoleInfo roleInfo = new LoginResultModel.RoleInfo();
        roleInfo.setRoleName("Super Admin");
        roleInfo.setValue("super");
        roleInfoList.add(roleInfo);
        resultModel.setRoles(roleInfoList);
        return resultModel;
    }

    /**
     * redis缓存账户对象
     * @param userLoginResp
     * @return
     */
    private UserLoginResp getUserLoginResp(UserLoginResp userLoginResp) {
        Account account = new Account();
        account.setId(userLoginResp.getId());
        account.setAccount(userLoginResp.getUsername());
        String token = AccountContext.storeSession(account);
        userLoginResp.setToken(token);
        // header内容
        userLoginResp.setHeader(HttpHeaders.AUTHORIZATION + ":" + "Bearer " + token);
        return userLoginResp;
    }

    public UserInfoResultModel getUserInfo() {
        return new UserInfoResultModel();
    }

    /**
     * 注册账号，注册公司
     * @param params
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public boolean registerCompany(RegisterParams params) {
        // 校验: 用户名唯一
        String account = params.getAccount();
        Integer a_counts = this.accountInfoMapper.queryCountsByAccount(account);
        if(a_counts>0){
            throw new BizException(ResultStatus.Auth.ERROR_ACCOUNT_EXIST);
        }
        // 校验：公司名称唯一
        String companyName = params.getCompanyName();
        Integer c_counts = this.companyInfoMapper.queryCountsByCompanyName(companyName);
        if(c_counts>0){
            throw new BizException(ResultStatus.Auth.ERROR_COMPANY_EXIST);
        }
        // 1. 保存账号信息
        AccountInfo accountInfo = new AccountInfo();
        accountInfo.setAccount(account);
        accountInfo.setPassword(params.getPassword());
        accountInfo.setBrowserUuid(params.getBrowserUuid());
        this.accountInfoMapper.insert(accountInfo);
        // 2. 保存公司信息
        String companyAccounts =
                DateUtil.format(LocalDateTime.now(), "yyyyMMddHHmmss") + "001";
        CompanyInfo companyInfo = new CompanyInfo();
        companyInfo.setCompanyName(companyName);
        companyInfo.setCompanyAccounts(companyAccounts);
        this.companyInfoMapper.insert(companyInfo);
        // 3. 关联账号和公司信息
        Integer accountId = accountInfo.getId();
        Integer companyId = companyInfo.getId();
        // 该用户虽然可以关联多个公司，自己申请注册公司的时候绝对是木有滴
        AccountCompanyRelation hasRelation =
                this.relationMapper.queryRelationByIds(accountId, companyId);
        // null 时添加申请的关联关系
        if (Objects.isNull(hasRelation)) {
            // 申请注册公司的用户关联起来
            AccountCompanyRelation relation = new AccountCompanyRelation();
            relation.setCompanyId(companyId);
            relation.setAccountId(accountId);
            this.relationMapper.insert(relation);
        }
        return true;
    }

    /**
     * 修改密码
     * @param params
     * @return
     */
    public boolean changePwd(ChangePwdParams params) {
        // 1.原密码是否正确
        String passwordOld = params.getPasswordOld();
        String passwordNew = params.getPasswordNew();
//        this.accountInfoMapper.queryCountsByPassword(passwordOld);
        // 2.更新新密码
        return true;
    }

    /**
     * 校验账号是否存在
     * @param account
     * @return
     */
    public String accountExist(String account) {
        log.info("校验账号：{}",account);
        Integer aCounts = this.accountInfoMapper.queryCountsByAccount(account);
        if(aCounts > 0){
            throw new BizException(ResultStatus.Auth.ERROR_ACCOUNT_EXIST);
        }
        return "账号可用";
    }

    /**
     * 新增或更新账号信息
     * @param req
     * @return
     */
    public AccountInfo saveOrUpdateAccount(SaveAccountInfoReq req) {
        AccountInfo accountInfo = new AccountInfo();
        BeanUtil.copyProperties(req,accountInfo,"id","birthday");
        // 重新赋值id
        accountInfo.setId(req.getUserId());
        if(StrUtil.isNotBlank(req.getBirthday())) {
            accountInfo.setBirthday(LocalDate.parse(req.getBirthday()));
        }
//        log.info("保存:{}", JSONUtil.toJsonPrettyStr(accountInfo));
        this.updateById(accountInfo);
        return accountInfo;
    }

    /**
     * 通过用户id获取用户信息
     * @param userId
     * @return
     */
    public UserInfoResultModel getUserInfoFromToken(Integer userId) {
        UserInfoResultModel user =  this.accountInfoMapper.getUserInfoFromToken(userId);
        // 角色
        String[] roleArr = user.getRole().split(",");
        List<LoginResultModel.RoleInfo> roleInfoList = new ArrayList<>();
        for (int i = 0; i < roleArr.length; i++) {
            String role = roleArr[i];
            LoginResultModel.RoleInfo roleInfo = new LoginResultModel.RoleInfo();
            roleInfo.setRoleName("Super Admin");
            roleInfo.setValue(role);
            roleInfoList.add(roleInfo);
        }
        user.setRoles(roleInfoList);
        return user;
    }
}
