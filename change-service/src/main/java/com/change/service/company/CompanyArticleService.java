package com.change.service.company;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.change.core.page.Page;
import com.change.dto.req.QueryCompanyArticleReq;
import com.change.dto.req.SaveCompanyArticleReq;
import com.change.dto.resp.QueryCompanyArticleResp;
import com.change.entity.company.CompanyArticle;
import com.change.mapper.company.CompanyArticleMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
* @author 坏孩子是你啊
* @date  2022-08-30
*/
@Slf4j
@Service
public class CompanyArticleService extends ServiceImpl<CompanyArticleMapper, CompanyArticle>{
    private final CompanyArticleMapper companyArticleMapper;

    public CompanyArticleService(CompanyArticleMapper companyArticleMapper) {
    this.companyArticleMapper = companyArticleMapper;
    }

    public Page<List<QueryCompanyArticleResp>> getPage(Page page, QueryCompanyArticleReq req) {
        page.offset();
        // 数据
        List<QueryCompanyArticleResp> items = companyArticleMapper.pageList(page,req);
        page.setItems(items);
        // 总数
        long total = companyArticleMapper.pageListTotal(req);
        page.setTotal(total);
        return page;
    }

    public QueryCompanyArticleResp detail(Integer id) {
         return companyArticleMapper.detailById(id);
    }

    @Transactional(rollbackFor = Exception.class)
    public CompanyArticle saveOrUpdate(SaveCompanyArticleReq req) {
        CompanyArticle article = new CompanyArticle();
        article.setTitle(req.getTitle());
        article.setContent(req.getContent());
        article.setTags(req.getTags());
        this.saveOrUpdate(article);
        return article;
    }

    @Transactional(rollbackFor = Exception.class)
    public CompanyArticle update(SaveCompanyArticleReq req) {
         return null;
    }

    @Transactional(rollbackFor = Exception.class)
    public Integer delete(Integer id) {
        return null;
    }
}
