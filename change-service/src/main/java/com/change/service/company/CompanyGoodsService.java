package com.change.service.company;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.change.core.page.Page;
import com.change.dto.req.QueryCompanyGoodsReq;
import com.change.dto.req.SaveCompanyGoodsReq;
import com.change.dto.resp.QueryCompanyGoodsResp;
import com.change.entity.company.CompanyGoods;
import com.change.mapper.company.CompanyGoodsMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

import java.util.List;

/**
 * @author jhlz
 * @date 2022-08-30
 */
@Slf4j
@Service
public class CompanyGoodsService extends ServiceImpl<CompanyGoodsMapper, CompanyGoods> {
    private final CompanyGoodsMapper companyGoodsMapper;

    public CompanyGoodsService(CompanyGoodsMapper companyGoodsMapper) {
        this.companyGoodsMapper = companyGoodsMapper;
    }

    public Page<List<QueryCompanyGoodsResp>> getPage(Page page, QueryCompanyGoodsReq req) {
        page.offset();
        List<QueryCompanyGoodsResp> records = this.companyGoodsMapper.findGoodsByPage(page, req);
        long total = this.companyGoodsMapper.findGoodsTotal(req);
        page.setTotal(total);
        page.setRecords(records);
        return page;
    }

    public QueryCompanyGoodsResp detail(Integer id) {
        CompanyGoods goods = this.companyGoodsMapper.selectById(id);
        QueryCompanyGoodsResp resp = new QueryCompanyGoodsResp();
        BeanUtil.copyProperties(goods, resp);
        return resp;
    }

    @Transactional(rollbackFor = Exception.class)
    public CompanyGoods saveOrUpdate(SaveCompanyGoodsReq req) {
        CompanyGoods goods = this.companyGoodsMapper.selectById(req.getId());
        CompanyGoods saveGoods = new CompanyGoods();
        BeanUtil.copyProperties(req, saveGoods);
        if (ObjectUtils.isEmpty(goods)) {
            this.companyGoodsMapper.insert(saveGoods);
        } else {
            this.companyGoodsMapper.updateById(saveGoods);
        }
        return saveGoods;
    }

    @Transactional(rollbackFor = Exception.class)
    public CompanyGoods update(SaveCompanyGoodsReq req) {
        return null;
    }

    @Transactional(rollbackFor = Exception.class)
    public Integer delete(Integer id) {
        return this.companyGoodsMapper.deleteById(id);
    }
}
