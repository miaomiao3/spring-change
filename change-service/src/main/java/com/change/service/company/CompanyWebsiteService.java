package com.change.service.company;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.change.core.page.Page;
import com.change.dto.req.QueryCompanyWebsiteReq;
import com.change.dto.req.SaveCompanyWebsiteReq;
import com.change.dto.resp.QueryCompanyWebsiteResp;
import com.change.entity.company.CompanyWebsite;
import com.change.mapper.company.CompanyWebsiteMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
* @author 路人甲
* @date  2022-08-30
*/
@Slf4j
@Service
public class CompanyWebsiteService extends ServiceImpl<CompanyWebsiteMapper, CompanyWebsite>{
    private final CompanyWebsiteMapper companyWebsiteMapper;

    public CompanyWebsiteService(CompanyWebsiteMapper companyWebsiteMapper) {
    this.companyWebsiteMapper = companyWebsiteMapper;
    }

    public Page<List<QueryCompanyWebsiteResp>> getPage(Page page, QueryCompanyWebsiteReq req) {
        page.offset();
        List<QueryCompanyWebsiteResp> records = this.companyWebsiteMapper.pageCompany(page, req);
        page.setRecords(records);
        long total = this.companyWebsiteMapper.pageCompanyTotal(req);
        page.setTotal(total);
        return page;
    }

    /**
     * 网站设置详情
     * @param id
     * @return
     */
    public QueryCompanyWebsiteResp detail(Integer id) {
        CompanyWebsite companyWebsite = this.companyWebsiteMapper.selectById(id);
        System.out.println(companyWebsite);
        QueryCompanyWebsiteResp queryCompanyWebsiteResp = new QueryCompanyWebsiteResp();
        BeanUtil.copyProperties(companyWebsite,queryCompanyWebsiteResp);
        System.out.println(queryCompanyWebsiteResp);
        return queryCompanyWebsiteResp;
    }

    /**
     * 新增或更新
     * @param req
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public CompanyWebsite saveOrUpdate(SaveCompanyWebsiteReq req) {
        CompanyWebsite website = new CompanyWebsite();
        BeanUtil.copyProperties(req,website);
        this.saveOrUpdate(website);
        return website;
    }

    /**
     * 修改更新
     * @param req
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public CompanyWebsite update(SaveCompanyWebsiteReq req) {
        CompanyWebsite companyWebsite = this.companyWebsiteMapper.selectById(req.getId());
        BeanUtil.copyProperties(req,companyWebsite);
        this.companyWebsiteMapper.updateById(companyWebsite);
        return companyWebsite;
    }

    /**
     * 删除
     * @param id
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public Integer delete(Integer id) {
        int a = this.companyWebsiteMapper.deleteById(id);
        return a;
    }

    /**
     * 根据companyId获取网站信息
     * @return
     */
    public QueryCompanyWebsiteResp detailWebsiteByCompanyId() {
        // 公司id TODO token中获取
        Integer companyId = 0;
        QueryCompanyWebsiteResp detail = this.companyWebsiteMapper.selectWebsiteByCompanyId(companyId);
        if(detail == null){
            return new QueryCompanyWebsiteResp();
        }
        return detail;
    }
}
