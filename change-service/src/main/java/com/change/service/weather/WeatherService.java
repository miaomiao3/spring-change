package com.change.service.weather;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.change.core.dto.Poster;
import com.change.core.dto.image.WeatherImg;
import com.change.core.page.Page;
import com.change.core.util.PosterUtil;
import com.change.dto.req.QueryWeatherReq;
import com.change.dto.req.SaveWeatherReq;
import com.change.dto.resp.QueryWeatherResp;
import com.change.entity.weather.Weather;
import com.change.mapper.weather.WeatherMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

/**
* @author 坏孩子是你啊
* @date  2022-08-18
*/
@Slf4j
@Service
public class WeatherService extends ServiceImpl<WeatherMapper, Weather>{
    private final WeatherMapper weatherMapper;

    public WeatherService(WeatherMapper weatherMapper) {
    this.weatherMapper = weatherMapper;
    }

    public File generateImage() {
        // 查询数据库的最新一条记录
        Weather weather = weatherMapper.getFirstByCreateTime();
        // 生成图片
        // TODO 根据 weather.getStatus() 选择不同的状态图标
        String statusUrl = "http://img.icons8.com/office/80/000000/partly-cloudy-night--v1.png"; //状态图片
        WeatherImg image_ = new WeatherImg();
        image_.setTemperature(weather.getTemperature());
        image_.setStatusUrl(statusUrl);
        LocalDateTime createTime = weather.getCreateTime();
        String time = "周" + createTime.getDayOfWeek().getValue() + "-" +createTime.format(DateTimeFormatter.ofPattern("yyyy年MM月dd日"));
        image_.setCreateTime(time);
        image_.setDesc(weather.getStatus()); //说明
        WeatherImg image = WeatherImg.init(image_);
        try {
            return WeatherImg.draw(image);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public Page<List<QueryWeatherResp>> getPage(Page page, QueryWeatherReq req) {

        return page;
    }

    public QueryWeatherResp detail(Integer id) {
         return new QueryWeatherResp();
    }

    @Transactional(rollbackFor = Exception.class)
    public Weather save(SaveWeatherReq req) {
        Weather weather = new Weather();
        BeanUtil.copyProperties(req, weather);
        this.weatherMapper.insert(weather);
        return weather;
    }

    @Transactional(rollbackFor = Exception.class)
    public Weather update(SaveWeatherReq req) {
         return null;
    }

    @Transactional(rollbackFor = Exception.class)
    public Integer delete(Integer id) {
        return null;
    }
}
