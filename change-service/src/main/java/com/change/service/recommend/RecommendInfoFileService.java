package com.change.service.recommend;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.change.entity.recommend.RecommendInfoFile;
import com.change.mapper.recommend.RecommendInfoFileMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @author 坏孩子是你啊
 * @date 2022-09-05
 */
@Slf4j
@Service
public class RecommendInfoFileService extends ServiceImpl<RecommendInfoFileMapper, RecommendInfoFile> {
    private final RecommendInfoFileMapper recommendInfoFileMapper;

    public RecommendInfoFileService(RecommendInfoFileMapper recommendInfoFileMapper) {
        this.recommendInfoFileMapper = recommendInfoFileMapper;
    }

}
