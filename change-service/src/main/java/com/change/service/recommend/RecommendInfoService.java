package com.change.service.recommend;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.change.core.page.Page;
import com.change.core.util.TokenUtil;
import com.change.dto.req.QueryRecommendInfoReq;
import com.change.dto.req.SaveRecommendInfoReq;
import com.change.dto.resp.QueryRecommendInfoResp;
import com.change.entity.recommend.RecommendInfo;
import com.change.entity.recommend.RecommendInfoFile;
import com.change.mapper.recommend.RecommendInfoFileMapper;
import com.change.mapper.recommend.RecommendInfoMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
* @author 坏孩子是你啊
* @date  2022-09-04
*/
@Slf4j
@Service
public class RecommendInfoService extends ServiceImpl<RecommendInfoMapper, RecommendInfo>{
    private final RecommendInfoMapper recommendInfoMapper;
    private final RecommendInfoFileMapper recommendInfoFileMapper;
    private final RecommendInfoFileService recommendInfoFileService;

    public RecommendInfoService(RecommendInfoMapper recommendInfoMapper,
                                RecommendInfoFileMapper recommendInfoFileMapper,
                                RecommendInfoFileService recommendInfoFileService) {
    this.recommendInfoMapper = recommendInfoMapper;
        this.recommendInfoFileMapper = recommendInfoFileMapper;
        this.recommendInfoFileService = recommendInfoFileService;
    }

    public Page<List<QueryRecommendInfoResp>> getPage(Page page, QueryRecommendInfoReq req) {
        // 公司id todo
        // 用户id
        Integer userId = TokenUtil.getUserId();
        req.setUserId(userId);
        List<QueryRecommendInfoResp> items = this.recommendInfoMapper.pageRecommendInfo(page,req);
        long total = this.recommendInfoMapper.pageRecommendInfoTotal(req);
        page.setItems(items);
        page.setTotal(total);
        return page;
    }

    public QueryRecommendInfoResp detail(Integer id) {
         return new QueryRecommendInfoResp();
    }

    @Transactional(rollbackFor = Exception.class)
    public RecommendInfo saveOrUpdate(SaveRecommendInfoReq req) {
        // 推荐信息
        RecommendInfo recommendInfo = new RecommendInfo();
        BeanUtil.copyProperties(req,recommendInfo);
        this.saveOrUpdate(recommendInfo);
        // 推荐附件
        Integer recInfoId = recommendInfo.getId();
        String[] urls = req.getUrls();
        if(urls!=null && urls.length > 0){
            // 所有附件
//            List<Integer> allIdList = recommendInfoFileMapper.queryIdListByRecInfoId(recInfoId);
            // 参数附件
//            List<Integer> reqIdList = recommendInfoFileMapper.queryIdListByUrls(recInfoId,urls);
            // 删除附件
            recommendInfoFileMapper.updateDeleteByRecInfoId(recInfoId);
            // 新增附件
            List<RecommendInfoFile> fileList = new ArrayList<>();
            for (int i = 0; i < urls.length; i++) {
                String url = urls[i];
                RecommendInfoFile recommendInfoFile = new RecommendInfoFile();
                recommendInfoFile.setUrl(url);
                recommendInfoFile.setRecInfoId(recInfoId);
                if(url.lastIndexOf(".") != -1 && url.lastIndexOf(".") != 0) {
                    String fileType = url.substring(url.lastIndexOf(".")+1);
                    recommendInfoFile.setFileType(fileType);
                }
                fileList.add(recommendInfoFile);
            }
            // 批量保存附件
            this.recommendInfoFileService.saveOrUpdateBatch(fileList);
        }
        return recommendInfo;
    }

    @Transactional(rollbackFor = Exception.class)
    public RecommendInfo update(SaveRecommendInfoReq req) {
         return null;
    }

    @Transactional(rollbackFor = Exception.class)
    public Integer delete(Integer id) {
        return null;
    }
}
