package com.change.controller.article;

import com.change.core.page.Page;
import com.change.dto.req.QueryArticleInfoReq;
import com.change.dto.req.SaveArticleInfoReq;
import com.change.dto.resp.QueryArticleInfoResp;
import com.change.entity.article.ArticleInfo;
import com.change.service.article.ArticleInfoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;

/**
 * @author 坏孩子是你啊
 * @date 2022-07-24
 */
@Api(value = "ArticleInfoController", tags = "文章管理")
@Slf4j
@RestController
@RequestMapping(value = "/api/articleInfo")
@ApiIgnore
public class ArticleInfoController {
    private final ArticleInfoService articleInfoService;

    public ArticleInfoController(ArticleInfoService articleInfoService) {
        this.articleInfoService = articleInfoService;
    }

    @ApiImplicitParams({
            @ApiImplicitParam(name = "current", value = "当前页", defaultValue = "1"),
            @ApiImplicitParam(name = "size", value = "每页数量", defaultValue = "100")
    })
    @ApiOperation(value = "分页查询")
    @GetMapping("/page")
    public Page<List<QueryArticleInfoResp>> page(@ApiIgnore Page page, QueryArticleInfoReq req) {
        return articleInfoService.getPage(page, req);
    }

    @ApiOperation(value = "详情")
    @GetMapping("/detail/{id}")
    public QueryArticleInfoResp detail(@PathVariable Integer id) {
        return articleInfoService.detail(id);
    }

    @ApiOperation(value = "新增")
    @PostMapping("/save")
    public ArticleInfo save(@RequestBody SaveArticleInfoReq req) {
        return articleInfoService.save(req);
    }

    @ApiOperation(value = "修改")
    @PostMapping("/update")
    public ArticleInfo update(@RequestBody SaveArticleInfoReq req) {
        return articleInfoService.update(req);
    }

    @ApiOperation(value = "删除")
    @PostMapping("/delete/{id}")
    public boolean delete(@PathVariable Integer id) {
        return articleInfoService.removeById(id);
    }

}