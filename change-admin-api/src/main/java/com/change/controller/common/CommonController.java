package com.change.controller.common;


import cn.hutool.core.io.resource.ClassPathResource;
import cn.hutool.core.util.StrUtil;
import com.change.config.ApplicationProperties;
import com.change.config.COSClientUtil;
import com.change.core.util.JSON;
import com.change.core.web.response.NotWrapResponse;
import com.change.dto.params.UploadFileParams;
import com.change.dto.result.UploadApiResult;
import com.change.exception.BizException;
import com.change.exception.ResultStatus;
import com.change.service.common.CommonService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import springfox.documentation.annotations.ApiIgnore;


import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * 通用接口，前端sql执行调用
 */
@Api(value = "CommonController", tags = "通用接口")
@Slf4j
@RestController
@RequestMapping(value = "/api/common")
public class CommonController {

    private final CommonService commonService;
    private final COSClientUtil cosClientUtil;
    private final ApplicationProperties applicationProperties;

    public CommonController(CommonService commonService, COSClientUtil cosClientUtil, ApplicationProperties applicationProperties) {
        this.commonService = commonService;
        this.cosClientUtil = cosClientUtil;
        this.applicationProperties = applicationProperties;
    }


    /*@ApiOperation(value = "查询数据")
    @GetMapping("/querySql")
    public List<Map<Integer, String>> querySql(String sql) {
        if (StrUtil.isBlank(sql) || !sql.startsWith("select")) {
            throw new BizException(ResultStatus.Common.NOT_NULL_SQL);
        }
        List<Map<Integer, String>> list = commonService.queryList(sql);
        log.info("数据内容：{}", list);
        return list;
    }*/

    /*@ApiOperation(value = "更新数据：新增,更新,删除")
    @GetMapping("/updateSql")
    public Integer updateSql(String sql) {
        if (StrUtil.isBlank(sql)) {
            throw new BizException(ResultStatus.Common.NOT_NULL_SQL);
        }
        Integer result = commonService.updateSql(sql);
        return result;
    }*/


    /**
     * 腾讯云COS:上传文件
     * @param
     * @return
     * @throws Exception
     */
    @ApiOperation(value = "腾讯云COS:上传文件")
    @PostMapping(value = "/cosUpload")
    @NotWrapResponse
    public UploadApiResult cosUpload(@RequestParam MultipartFile file ) throws Exception {
        UploadApiResult result = new UploadApiResult();
//        log.info("上传文件参数:{}", JSON.toJSON(params));
//        MultipartFile file = params.getFile();
        String filePath = cosClientUtil.upload(file);
        result.setUrl(filePath);
        return result;
    }

    @ApiOperation(value = "执行deploy_pro.sh脚本", httpMethod = "Get")
    @GetMapping("/deploy")
    public Integer execShell() {
        int i = 0;
        try {
            String deployDir = applicationProperties.getWebsite().getDeployDir();
            Process ps = Runtime.getRuntime().exec(deployDir);
            i = ps.waitFor();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }catch (IOException e) {
            e.printStackTrace();
        }
        return i;
    }
}
