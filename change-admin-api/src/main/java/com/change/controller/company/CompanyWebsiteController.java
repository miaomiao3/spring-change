package com.change.controller.company;
import com.change.core.page.Page;
import com.change.dto.req.QueryCompanyWebsiteReq;
import com.change.dto.req.SaveCompanyWebsiteReq;
import com.change.dto.resp.QueryCompanyWebsiteResp;
import com.change.entity.company.CompanyWebsite;
import com.change.service.company.CompanyWebsiteService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;

/**
* @author 路人甲
* @date  2022-08-30
*/
@Api(value = "CompanyWebsiteController", tags = "公司网站信息管理")
@Slf4j
@RestController
@RequestMapping(value = "/api/company")
public class CompanyWebsiteController {
        private final CompanyWebsiteService companyWebsiteService;

        public CompanyWebsiteController(CompanyWebsiteService companyWebsiteService) {
                this.companyWebsiteService = companyWebsiteService;
        }

        @ApiImplicitParams({
        @ApiImplicitParam(name = "current", value = "当前页", defaultValue = "1"),
        @ApiImplicitParam(name = "size", value = "每页数量", defaultValue = "100")
        })
        @ApiOperation(value = "分页查询")
        @GetMapping("/pageCompanyWebsite")
        public Page<List<QueryCompanyWebsiteResp>> pageCompanyWebsite(@ApiIgnore Page page, QueryCompanyWebsiteReq req) {
                return companyWebsiteService.getPage(page, req);
        }

        @ApiOperation(value = "详情ById")
        @GetMapping("/detailWebsiteById/{id}")
        public QueryCompanyWebsiteResp detail(@PathVariable Integer id) {
                return companyWebsiteService.detail(id);
        }

        @ApiOperation(value = "详情ByCompanyId")
        @GetMapping("/detailWebsiteByCompanyId")
        public QueryCompanyWebsiteResp detailWebsiteByCompanyId() {
                return companyWebsiteService.detailWebsiteByCompanyId();
        }

        @ApiOperation(value = "新增或更新")
        @PostMapping("/saveOrUpdateCompanyWebsite")
        public CompanyWebsite saveOrUpdate(@RequestBody SaveCompanyWebsiteReq req) {
                return companyWebsiteService.saveOrUpdate(req);
        }

        /*@ApiOperation(value = "修改")
        @PostMapping("/update")
        public CompanyWebsite update(@RequestBody SaveCompanyWebsiteReq req) {
                return companyWebsiteService.update(req);
        }*/

        @ApiOperation(value = "删除")
        @PostMapping("/deleteCompanyWebsite/{id}")
        public Integer delete(@PathVariable Integer id) {
                return companyWebsiteService.delete(id);
        }

}