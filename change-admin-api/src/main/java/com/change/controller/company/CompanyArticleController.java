package com.change.controller.company;

import com.change.core.page.Page;
import com.change.dto.req.QueryCompanyArticleReq;
import com.change.dto.req.SaveCompanyArticleReq;
import com.change.dto.resp.QueryCompanyArticleResp;
import com.change.entity.company.CompanyArticle;
import com.change.service.company.CompanyArticleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;

/**
* @author 坏孩子是你啊
* @date  2022-08-30
*/
@Api(value = "CompanyArticleController", tags = "公司文章管理")
@Slf4j
@RestController
@RequestMapping(value = "/api/company")
public class CompanyArticleController {
        private final CompanyArticleService companyArticleService;

        public CompanyArticleController(CompanyArticleService companyArticleService) {
                this.companyArticleService = companyArticleService;
        }

        @ApiImplicitParams({
        @ApiImplicitParam(name = "current", value = "当前页", defaultValue = "1"),
        @ApiImplicitParam(name = "size", value = "每页数量", defaultValue = "100")
        })
        @ApiOperation(value = "分页查询")
        @GetMapping("/pageCompanyArticle")
        public Page<List<QueryCompanyArticleResp>> pageCompanyArticle(@ApiIgnore Page page, QueryCompanyArticleReq req) {
                return companyArticleService.getPage(page, req);
        }

        @ApiOperation(value = "详情")
        @GetMapping("/detailCompanyArticle/{id}")
        public QueryCompanyArticleResp detail(@PathVariable Integer id) {
                return companyArticleService.detail(id);
        }

        @ApiOperation(value = "新增或更新")
        @PostMapping("/saveOrUpdateCompanyArticle")
        public CompanyArticle saveOrUpdate(@RequestBody SaveCompanyArticleReq req) {
                return companyArticleService.saveOrUpdate(req);
        }

        /**@ApiOperation(value = "修改")
        @PostMapping("/update")
        public CompanyArticle update(@RequestBody SaveCompanyArticleReq req) {
                return companyArticleService.update(req);
        }*/

        @ApiOperation(value = "删除")
        @PostMapping("/deleteCompanyArticle/{id}")
        public Integer delete(@PathVariable Integer id) {
                return companyArticleService.delete(id);
        }

}