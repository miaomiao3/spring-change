package com.change.controller.company;

import com.change.core.page.Page;
import com.change.dto.req.QueryCompanyGoodsReq;
import com.change.dto.req.SaveCompanyGoodsReq;
import com.change.dto.resp.QueryCompanyGoodsResp;
import com.change.entity.company.CompanyGoods;
import com.change.service.company.CompanyGoodsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;

/**
 * @author jhlz
 * @date 2022-08-30
 */
@Api(value = "CompanyGoodsController", tags = "商品管理")
@Slf4j
@RestController
@RequestMapping(value = "/api/company")
public class CompanyGoodsController {
    private final CompanyGoodsService companyGoodsService;

    public CompanyGoodsController(CompanyGoodsService companyGoodsService) {
        this.companyGoodsService = companyGoodsService;
    }

    @ApiImplicitParams({
            @ApiImplicitParam(name = "current", value = "当前页", defaultValue = "1"),
            @ApiImplicitParam(name = "size", value = "每页数量", defaultValue = "100")
    })
    @ApiOperation(value = "分页查询")
    @GetMapping("/pageCompanyGoods")
    public Page<List<QueryCompanyGoodsResp>> pageCompanyGoods(@ApiIgnore Page page, QueryCompanyGoodsReq req) {
        return companyGoodsService.getPage(page, req);
    }

    @ApiOperation(value = "详情")
    @GetMapping("/detailCompanyGoods/{id}")
    public QueryCompanyGoodsResp detail(@PathVariable Integer id) {
        return companyGoodsService.detail(id);
    }

    @ApiOperation(value = "新增或更新")
    @PostMapping("/saveOrUpdateCompanyGoods")
    public CompanyGoods saveOrUpdate(@RequestBody SaveCompanyGoodsReq req) {
        return companyGoodsService.saveOrUpdate(req);
    }

    /**
     * @ApiOperation(value = "修改")
     * @PostMapping("/update") public CompanyGoods update(@RequestBody SaveCompanyGoodsReq req) {
     * return companyGoodsService.update(req);
     * }
     */

    @ApiOperation(value = "删除")
    @PostMapping("/deleteCompanyGoods/{id}")
    public Integer delete(@PathVariable Integer id) {
        return companyGoodsService.delete(id);
    }

}