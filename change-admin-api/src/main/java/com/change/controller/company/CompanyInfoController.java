package com.change.controller.company;

import com.change.core.page.Page;
import com.change.dto.req.QueryCompanyInfoReq;
import com.change.dto.req.SaveAccountInfoReq;
import com.change.dto.req.SaveCompanyInfoReq;
import com.change.dto.resp.QueryCompanyInfoResp;
import com.change.entity.company.CompanyInfo;
import com.change.service.company.CompanyInfoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;

/**
* @author 坏孩子是你啊
* @date  2022-08-15
*/
@Api(value = "CompanyInfoController", tags = "公司信息管理")
@Slf4j
@RestController
@RequestMapping(value = "/api/company")
public class CompanyInfoController {
        private final CompanyInfoService companyInfoService;

        public CompanyInfoController(CompanyInfoService companyInfoService) {
                this.companyInfoService = companyInfoService;
        }

        @ApiImplicitParams({
        @ApiImplicitParam(name = "current", value = "当前页", defaultValue = "1"),
        @ApiImplicitParam(name = "size", value = "每页数量", defaultValue = "100")
        })
        @ApiOperation(value = "分页查询")
        @GetMapping("/pageCompanyInfo")
        public Page<List<QueryCompanyInfoResp>> page(@ApiIgnore Page page, QueryCompanyInfoReq req) {
                return companyInfoService.getPage(page, req);
        }

        @ApiOperation(value = "详情")
        @GetMapping("/detailCompanyInfo/{id}")
        public QueryCompanyInfoResp detail(@PathVariable Integer id) {
                return companyInfoService.detail(id);
        }

        @ApiOperation(value = "新增或更新")
        @PostMapping("/saveOrUpdateCompanyInfo")
        public CompanyInfo save(@RequestBody SaveAccountInfoReq accountInfoReq, @RequestBody SaveCompanyInfoReq req) {
                return companyInfoService.save(accountInfoReq, req);
        }
/*
        @ApiOperation(value = "修改")
        @PostMapping("/update")
        public CompanyInfo update(@RequestBody SaveCompanyInfoReq req) {
                return companyInfoService.update(req);
        }*/

        @ApiOperation(value = "删除")
        @PostMapping("/deleteCompanyInfo/{id}")
        public Integer delete(@PathVariable Integer id) {
                return companyInfoService.delete(id);
        }

        /**
         * 启用/禁用 为审核是否通过的标志
         * @param id
         * @return
         */
        @ApiOperation(value = "审核")
        @PostMapping("/auditCompanyInfo/{id}")
        public Integer audit(@PathVariable Integer id) {
                return companyInfoService.audit(id);
        }

}