package com.change.controller.system;

import cn.hutool.core.lang.tree.Tree;
import com.change.core.page.Page;
import com.change.dto.req.QuerySystemDepartmentReq;
import com.change.dto.req.SaveSystemDepartmentReq;
import com.change.dto.resp.QuerySystemDepartmentResp;
import com.change.entity.system.SystemDepartment;
import com.change.service.system.SystemDepartmentService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;

/**
* @author 路人甲
* @date  2022-08-21
*/
@Api(value = "SystemDepartmentController", tags = "部门管理")
@Slf4j
@RestController
@RequestMapping(value = "/api/system")
public class SystemDepartmentController {
        private final SystemDepartmentService systemDepartmentService;

        public SystemDepartmentController(SystemDepartmentService systemDepartmentService) {
                this.systemDepartmentService = systemDepartmentService;
        }

        @ApiOperation(value = "部门树列表")
        @GetMapping("/getDeptList")
        public List<Tree<String>> getDeptList(QuerySystemDepartmentReq req) {
                return systemDepartmentService.getDeptList(req);
        }

        @ApiOperation(value = "详情")
        @GetMapping("/detailSystemDepartment/{id}")
        public QuerySystemDepartmentResp detail(@PathVariable Integer id) {
                return systemDepartmentService.detail(id);
        }

        @ApiOperation(value = "新增")
        @PostMapping("/saveOrUpdateDept")
        public SystemDepartment saveOrUpdate(@RequestBody SaveSystemDepartmentReq req) {
                return systemDepartmentService.saveOrUpdate(req);
        }

        @ApiOperation(value = "修改")
        @PostMapping("/update")
        public SystemDepartment update(@RequestBody SaveSystemDepartmentReq req) {
                return systemDepartmentService.update(req);
        }

        @ApiOperation(value = "删除")
        @GetMapping("/deleteDept/{id}")
        public Integer deleteDept(@PathVariable Integer id) {
                return systemDepartmentService.delete(id);
        }

}