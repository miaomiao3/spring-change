package com.change.controller.system;

import cn.hutool.core.lang.tree.Tree;
import com.change.core.page.Page;
import com.change.dto.req.QuerySystemDepartmentReq;
import com.change.dto.req.QuerySystemMenuReq;
import com.change.dto.req.SaveSystemMenuReq;
import com.change.dto.resp.QuerySystemMenuResp;
import com.change.entity.system.SystemMenu;
import com.change.service.system.SystemMenuService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;

/**
 * @author jhlz
 * @date 2022-08-27
 */
@Api(value = "SysMenuController", tags = "菜单管理")
@Slf4j
@RestController
@RequestMapping(value = "/api/system")
public class SystemMenuController {
    private final SystemMenuService sysMenuService;

    public SystemMenuController(SystemMenuService sysMenuService) {
        this.sysMenuService = sysMenuService;
    }

    @ApiImplicitParams({
            @ApiImplicitParam(name = "current", value = "当前页", defaultValue = "1"),
            @ApiImplicitParam(name = "size", value = "每页数量", defaultValue = "100")
    })
    @ApiOperation(value = "分页查询")
    @GetMapping("/pageMenu")
    public Page<List<QuerySystemMenuResp>> page(@ApiIgnore Page page, QuerySystemMenuReq req) {
        return sysMenuService.getPage(page, req);
    }

    @ApiOperation(value = "菜单树列表")
    @GetMapping("/getMenuList")
    public List<Tree<Integer>> listMenu(QuerySystemMenuReq req) {
        return sysMenuService.getMenuTreeList(req);
    }

    @ApiOperation(value = "详情")
    @GetMapping("/detailMenu/{id}")
    public QuerySystemMenuResp detail(@PathVariable Integer id) {
        return sysMenuService.detail(id);
    }

    @ApiOperation(value = "新增或更新菜单")
    @PostMapping("/saveOrUpdateMenu")
    public SystemMenu saveOrUpdateMenu(@RequestBody SaveSystemMenuReq req) {
        return sysMenuService.saveOrUpdateMenu(req);
    }

    /*@ApiOperation(value = "修改")
    @PostMapping("/update")
    public SystemMenu update(@RequestBody SaveSystemMenuReq req) {
        return sysMenuService.update(req);
    }*/

    @ApiOperation(value = "删除")
    @GetMapping("/deleteMenu/{id}")
    public Integer delete(@PathVariable Integer id) {
        return sysMenuService.delete(id);
    }

}