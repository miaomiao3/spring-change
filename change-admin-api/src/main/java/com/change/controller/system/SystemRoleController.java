package com.change.controller.system;

import com.change.core.page.Page;
import com.change.dto.req.QuerySystemRoleReq;
import com.change.dto.req.SaveSystemRoleReq;
import com.change.dto.resp.QuerySystemRoleResp;
import com.change.entity.system.SystemRole;
import com.change.service.system.SystemRoleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;

/**
 * @author jhlz
 * @date 2022-08-27
 */
@Api(value = "SysRoleController", tags = "角色管理")
@Slf4j
@RestController
@RequestMapping(value = "/api/system")
public class SystemRoleController {
    private final SystemRoleService sysRoleService;

    public SystemRoleController(SystemRoleService sysRoleService) {
        this.sysRoleService = sysRoleService;
    }

    @ApiImplicitParams({
            @ApiImplicitParam(name = "current", value = "当前页", defaultValue = "1"),
            @ApiImplicitParam(name = "size", value = "每页数量", defaultValue = "100")
    })
    @ApiOperation(value = "分页查询")
    @GetMapping("/getRoleListByPage")
    public Page<List<QuerySystemRoleResp>> page(@ApiIgnore Page page, QuerySystemRoleReq req) {
        return sysRoleService.getPage(page, req);
    }

    @ApiOperation(value = "角色列表")
    @GetMapping("/getAllRoleList")
    public List<SystemRole> listRole() {
        return sysRoleService.getAllRoleList();
    }

    @ApiOperation(value = "详情")
    @GetMapping("/detailRole/{id}")
    public QuerySystemRoleResp detailRole(@PathVariable Integer id) {
        return sysRoleService.detail(id);
    }

    @ApiOperation(value = "新增")
    @PostMapping("/saveOrUpdateRole")
    public SystemRole saveOrUpdateRole(@RequestBody SaveSystemRoleReq req) {
        return sysRoleService.save(req);
    }

    /*@ApiOperation(value = "修改")
    @PostMapping("/update")
    public SystemRole update(@RequestBody SaveSystemRoleReq req) {
        return sysRoleService.update(req);
    }*/

    @ApiOperation(value = "删除")
    @PostMapping("/deleteRole/{id}")
    public Integer deleteRole(@PathVariable Integer id) {
        return sysRoleService.delete(id);
    }

}