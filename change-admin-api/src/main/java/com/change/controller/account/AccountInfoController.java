package com.change.controller.account;

import com.change.core.page.Page;
import com.change.dto.params.AccountExistParams;
import com.change.dto.req.QueryAccountInfoReq;
import com.change.dto.req.RegisterReq;
import com.change.dto.req.SaveAccountInfoReq;
import com.change.dto.resp.LoginResultModel;
import com.change.dto.resp.QueryAccountInfoResp;
import com.change.dto.resp.UserInfoResultModel;
import com.change.entity.account.AccountInfo;
import com.change.exception.BizException;
import com.change.exception.ResultStatus;
import com.change.service.account.AccountInfoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;

/**
 * @author 坏孩子是你啊
 * @date 2022-07-15
 */
@Api(value = "AccountInfoController", tags = "账户中心管理")
@Slf4j
@RestController
@RequestMapping(value = "/api/system")
public class AccountInfoController {

    private final AccountInfoService accountInfoService;

    public AccountInfoController(AccountInfoService accountInfoService) {
        this.accountInfoService = accountInfoService;
    }

    @ApiImplicitParams({
            @ApiImplicitParam(name = "current", value = "当前页", defaultValue = "1"),
            @ApiImplicitParam(name = "size", value = "每页数量", defaultValue = "100")
    })
    @ApiOperation(value = "分页查询")
    @GetMapping("/getAccountList")
    public Page<List<QueryAccountInfoResp>> getAccountList(@ApiIgnore Page page, QueryAccountInfoReq req) {
        return accountInfoService.getPage(page, req);
    }

    @ApiOperation(value = "详情")
    @GetMapping("/detailAccount/{id}")
    public QueryAccountInfoResp accountDetail(@PathVariable Integer id) {
        return accountInfoService.detail(id);
    }

    @ApiOperation(value = "新增或更新账号")
    @PostMapping("/saveOrUpdateAccount")
    public AccountInfo saveOrUpdateAccount(@RequestBody SaveAccountInfoReq req) {
        return accountInfoService.saveOrUpdateAccount(req);
    }

    @ApiOperation(value = "账号校验")
    @PostMapping("/accountExist")
    public String accountExist(@RequestBody AccountExistParams params) {
        return accountInfoService.accountExist(params.getAccount());
    }

    @ApiOperation(value = "删除")
    @PostMapping("/deleteAccount/{id}")
    public Integer deleteAccount(@PathVariable Integer id) {
        return accountInfoService.delete(id);
    }

}