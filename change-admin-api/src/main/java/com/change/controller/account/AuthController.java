package com.change.controller.account;

import com.change.core.util.TokenUtil;
import com.change.dto.params.ChangePwdParams;
import com.change.dto.params.RegisterParams;
import com.change.dto.req.RegisterReq;
import com.change.dto.req.SaveAccountInfoReq;
import com.change.dto.resp.LoginResultModel;
import com.change.dto.resp.UserInfoResultModel;
import com.change.entity.account.AccountInfo;
import com.change.service.account.AccountInfoService;
import com.change.service.company.CompanyInfoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

/**
 * @author 坏孩子是你啊
 * @date 2022-07-15
 */
@Api(value = "AccountInfoController", tags = "用户鉴权管理")
@Slf4j
@RestController
@RequestMapping(value = "/api")
public class AuthController {

    private final AccountInfoService accountInfoService;
    private final CompanyInfoService companyInfoService;

    public AuthController(AccountInfoService accountInfoService,
                          CompanyInfoService companyInfoService) {
        this.accountInfoService = accountInfoService;
        this.companyInfoService = companyInfoService;
    }

   /* @ApiOperation(value = "注册")
    @PostMapping("/register")
    public Integer register(@RequestBody RegisterReq req) {
        return accountInfoService.register(req);
    }*/

    @ApiOperation(value = "注册")
    @PostMapping("/register")
    public Integer register(@RequestBody RegisterParams params) {
//        return accountInfoService.registerCompany(params);
        return accountInfoService.register(params);
    }

    @ApiOperation(value = "登录")
    @PostMapping("/login")
    public LoginResultModel login(@RequestBody RegisterReq req){
        if(req.getAnonymous() == 1){
            return accountInfoService.login(req);
        }else {
            return accountInfoService.anonymousLogin(req);
        }
    }

    @ApiOperation(value = "注销")
    @GetMapping("/logout")
    public boolean logout(String token){
        // TODO
        log.info("注销成功");
        return true;
    }

    @ApiOperation(value = "修改密码")
    @PostMapping("/changePwd")
    public boolean changePwd(@RequestBody ChangePwdParams params){
        return this.accountInfoService.changePwd(params);
    }

    @ApiOperation(value = "用户信息")
    @GetMapping("/getUserInfo")
    public UserInfoResultModel getUserInfo(){
        Integer userId = TokenUtil.getUserId();
        UserInfoResultModel userInfo = accountInfoService.getUserInfoFromToken(userId);
        return userInfo;
    }

    @ApiOperation(value = "更新用户信息")
    @PostMapping("/updateUserInfo")
    public AccountInfo updateUserInfo(@RequestBody SaveAccountInfoReq req) {
        Integer userId = TokenUtil.getUserId();
        log.info("当前用户:{}",userId);
        req.setUserId(userId);
        return accountInfoService.saveOrUpdateAccount(req);
    }
}
