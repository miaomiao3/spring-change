package com.change.controller.account;

import com.change.core.page.Page;
import com.change.dto.req.QueryAccountWechatReq;
import com.change.dto.req.SaveAccountWechatReq;
import com.change.dto.resp.QueryAccountWechatResp;
import com.change.entity.account.AccountWechat;
import com.change.service.account.AccountWechatService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;

/**
 * @author 坏孩子是你啊
 * @date 2022-07-27
 */
@Api(value = "AccountWechatController", tags = "微信好友管理")
@Slf4j
@RestController
@RequestMapping(value = "/api/accountWechat")
public class AccountWechatController {
    private final AccountWechatService accountWechatService;

    public AccountWechatController(AccountWechatService accountWechatService) {
        this.accountWechatService = accountWechatService;
    }

    @ApiImplicitParams({
            @ApiImplicitParam(name = "current", value = "当前页", defaultValue = "1"),
            @ApiImplicitParam(name = "size", value = "每页数量", defaultValue = "100")
    })
    @ApiOperation(value = "分页查询")
    @GetMapping("/page")
    public Page<List<QueryAccountWechatResp>> page(@ApiIgnore Page page, QueryAccountWechatReq req) {
        return accountWechatService.getPage(page, req);
    }

    @ApiOperation(value = "详情")
    @GetMapping("/detail/{id}")
    public QueryAccountWechatResp detail(@PathVariable Integer id) {
        return accountWechatService.detail(id);
    }

    @ApiOperation(value = "微信号查找")
    @GetMapping("/detailByWxNo/{wxNo}")
    public QueryAccountWechatResp detailByWxNo(@PathVariable String wxNo) {
        return accountWechatService.detailByWxNo(wxNo);
    }

    @ApiOperation(value = "新增")
    @PostMapping("/save")
    public AccountWechat save(@RequestBody SaveAccountWechatReq req) {
        return accountWechatService.save(req);
    }

    @ApiOperation(value = "批量新增")
    @PostMapping("/saveBatch")
    public boolean saveBatch(@RequestBody List<SaveAccountWechatReq> req) {
        return accountWechatService.saveBatchWechat(req);
    }

    @ApiOperation(value = "修改")
    @PostMapping("/update")
    public AccountWechat update(@RequestBody SaveAccountWechatReq req) {
        return accountWechatService.update(req);
    }

    @ApiOperation(value = "删除")
    @PostMapping("/delete/{id}")
    public Integer delete(@PathVariable Integer id) {
        return accountWechatService.delete(id);
    }

}