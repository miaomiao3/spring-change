package com.change.controller.moments;

import com.change.core.page.Page;
import com.change.dto.params.MomentsGatherParams;
import com.change.dto.result.MomentsGatherResult;
import com.change.entity.moments.MomentsGather;
import com.change.service.moments.MomentsGatherService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;

/**
* @author 坏孩子是你啊
* @date  2022-10-06
*/
@Api(value = "MomentsGatherController", tags = "集合管理")
@Slf4j
@RestController
@RequestMapping(value = "/api/moments")
public class MomentsGatherController {
        private final MomentsGatherService momentsGatherService;

        public MomentsGatherController(MomentsGatherService momentsGatherService) {
                this.momentsGatherService = momentsGatherService;
        }

        @ApiImplicitParams({
        @ApiImplicitParam(name = "current", value = "当前页", defaultValue = "1"),
        @ApiImplicitParam(name = "size", value = "每页数量", defaultValue = "100")
        })
        @ApiOperation(value = "分页查询")
        @GetMapping("/pageMomentsGather")
        public Page<List<MomentsGatherResult>> pageMomentsGather(@ApiIgnore Page page, MomentsGatherParams req) {
                return momentsGatherService.getPage(page, req);
        }

        @ApiOperation(value = "详情")
        @GetMapping("/detailMomentsGather/{id}")
        public MomentsGatherParams detail(@PathVariable Integer id) {
                return momentsGatherService.detail(id);
        }

        @ApiOperation(value = "新增或更新")
        @PostMapping("/saveOrUpdateMomentsGather")
        public MomentsGather saveOrUpdate(@RequestBody MomentsGatherParams req) {
                return momentsGatherService.saveOrUpdate(req);
        }

        /**@ApiOperation(value = "修改")
        @PostMapping("/update")
        public MomentsGather update(@RequestBody MomentsGatherParams req) {
                return momentsGatherService.update(req);
        }*/

        @ApiOperation(value = "删除")
        @PostMapping("/deleteMomentsGather/{id}")
        public Integer delete(@PathVariable Integer id) {
                return momentsGatherService.delete(id);
        }

}