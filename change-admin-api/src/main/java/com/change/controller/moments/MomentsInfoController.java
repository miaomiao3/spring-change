package com.change.controller.moments;

import com.change.core.page.Page;
import com.change.dto.params.MomentsInfoParams;
import com.change.dto.result.MomentsInfoResult;
import com.change.entity.moments.MomentsInfo;
import com.change.service.moments.MomentsInfoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;

/**
* @author 坏孩子是你啊
* @date  2022-10-04
*/
@Api(value = "MomentsInfoController", tags = "朋友圈基础信息")
@Slf4j
@RestController
@RequestMapping(value = "/api/moments")
public class MomentsInfoController {
        private final MomentsInfoService momentsInfoService;

        public MomentsInfoController(MomentsInfoService momentsInfoService) {
                this.momentsInfoService = momentsInfoService;
        }

        @ApiImplicitParams({
        @ApiImplicitParam(name = "current", value = "当前页", defaultValue = "1"),
        @ApiImplicitParam(name = "size", value = "每页数量", defaultValue = "100")
        })
        @ApiOperation(value = "分页查询")
        @GetMapping("/pageMomentsInfo")
        public Page<List<MomentsInfoResult>> pageMomentsInfo(@ApiIgnore Page page, MomentsInfoParams req) {
                return momentsInfoService.getPage(page, req);
        }

        @ApiOperation(value = "详情")
        @GetMapping("/detailMomentsInfo/{id}")
        public MomentsInfoParams detail(@PathVariable Integer id) {
                return momentsInfoService.detail(id);
        }

        @ApiOperation(value = "新增或更新")
        @PostMapping("/saveOrUpdateMomentsInfo")
        public MomentsInfo saveOrUpdate(@RequestBody MomentsInfoParams req) {
                return momentsInfoService.saveOrUpdate(req);
        }

        /**@ApiOperation(value = "修改")
        @PostMapping("/update")
        public MomentsInfo update(@RequestBody MomentsInfoParams req) {
                return momentsInfoService.update(req);
        }*/

        @ApiOperation(value = "删除")
        @PostMapping("/deleteMomentsInfo/{id}")
        public Integer delete(@PathVariable Integer id) {
                return momentsInfoService.delete(id);
        }

}