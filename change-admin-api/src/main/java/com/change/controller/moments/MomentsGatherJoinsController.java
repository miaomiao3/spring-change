package com.change.controller.moments;

import com.change.core.page.Page;
import com.change.dto.params.MomentsGatherJoinsParams;
import com.change.entity.moments.MomentsGatherJoins;
import com.change.service.moments.MomentsGatherJoinsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;

/**
* @author 坏孩子是你啊
* @date  2022-10-06
*/
@Api(value = "MomentsGatherJoinsController", tags = "集合人员管理")
@Slf4j
@RestController
@RequestMapping(value = "/api/moments")
public class MomentsGatherJoinsController {
        private final MomentsGatherJoinsService momentsGatherJoinsService;

        public MomentsGatherJoinsController(MomentsGatherJoinsService momentsGatherJoinsService) {
                this.momentsGatherJoinsService = momentsGatherJoinsService;
        }

        @ApiImplicitParams({
        @ApiImplicitParam(name = "current", value = "当前页", defaultValue = "1"),
        @ApiImplicitParam(name = "size", value = "每页数量", defaultValue = "100")
        })
        @ApiOperation(value = "分页查询")
        @GetMapping("/pageMomentsGatherJoins")
        public Page<List<MomentsGatherJoinsParams>> pageMomentsGatherJoins(@ApiIgnore Page page, MomentsGatherJoinsParams req) {
                return momentsGatherJoinsService.getPage(page, req);
        }

        @ApiOperation(value = "详情")
        @GetMapping("/detailMomentsGatherJoins/{id}")
        public MomentsGatherJoinsParams detail(@PathVariable Integer id) {
                return momentsGatherJoinsService.detail(id);
        }

        @ApiOperation(value = "新增或更新")
        @PostMapping("/saveOrUpdateMomentsGatherJoins")
        public MomentsGatherJoins saveOrUpdate(@RequestBody MomentsGatherJoinsParams req) {
                return momentsGatherJoinsService.saveOrUpdate(req);
        }

        /**@ApiOperation(value = "修改")
        @PostMapping("/update")
        public MomentsGatherJoins update(@RequestBody MomentsGatherJoinsParams req) {
                return momentsGatherJoinsService.update(req);
        }*/

        @ApiOperation(value = "删除")
        @PostMapping("/deleteMomentsGatherJoins/{id}")
        public Integer delete(@PathVariable Integer id) {
                return momentsGatherJoinsService.delete(id);
        }

}