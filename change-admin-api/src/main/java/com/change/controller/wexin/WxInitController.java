package com.change.controller.wexin;

import cn.hutool.core.lang.UUID;
import cn.hutool.core.util.StrUtil;
import com.change.config.WxUtil;
import com.change.dto.result.WxConfigResult;
import com.change.exception.BizException;
import com.change.exception.ResultStatus;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * 微信初始化接入Controller控制器
 */
@Api(value = "WxInitController", tags = "微信管理")
@Slf4j
@RestController
@RequestMapping("/api/weixin")
public class WxInitController {

    /**
     *  初始化微信JSSDK Config信息
     * @param url  分享url地址
     * @return
     * @throws Exception
     */
    @ApiOperation(value = "初始化JSSDKConfig")
    @GetMapping("/initWXJSSDKConfigInfo")
    public WxConfigResult initWXJSConfig (WxConfigResult param) throws Exception {
        String url = param.getUrl();
        if(StrUtil.isBlank(url)){
            throw new BizException(ResultStatus.Common.WX_URL_ISNULL);
        }
        //获取AccessToken
        String accessToken = WxUtil.getJSSDKAccessToken();
        //获取JssdkGetticket
        String jsapiTicket = WxUtil.getJssdkGetticket(accessToken);
        String timestamp = Long.toString(System.currentTimeMillis() / 1000);
        String nonceStr = UUID.randomUUID().toString();
        String signature = WxUtil.buildJSSDKSignature(jsapiTicket,timestamp,nonceStr,url);
        String appId = WxUtil.getAppId();
        // 返回结果
        WxConfigResult result = new WxConfigResult();
        result.setUrl(url);
        result.setJsapiTicket(jsapiTicket);
        result.setNonceStr(nonceStr);
        result.setTimestamp(timestamp);
        result.setSignature(signature);
        result.setAppId(appId);

        return result;
    }
}
