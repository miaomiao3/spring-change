package com.change.controller.recommend;

import com.change.core.page.Page;
import com.change.dto.req.QueryRecommendInfoReq;
import com.change.dto.req.SaveRecommendInfoReq;
import com.change.dto.resp.QueryRecommendInfoResp;
import com.change.entity.recommend.RecommendInfo;
import com.change.service.recommend.RecommendInfoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;

/**
* @author 坏孩子是你啊
* @date  2022-09-04
*/
@Api(value = "RecommendInfoController", tags = "推荐管理")
@Slf4j
@RestController
@RequestMapping(value = "/api/recommend")
public class RecommendInfoController {
        private final RecommendInfoService recommendInfoService;

        public RecommendInfoController(RecommendInfoService recommendInfoService) {
                this.recommendInfoService = recommendInfoService;
        }

        @ApiImplicitParams({
        @ApiImplicitParam(name = "current", value = "当前页", defaultValue = "1"),
        @ApiImplicitParam(name = "size", value = "每页数量", defaultValue = "100")
        })
        @ApiOperation(value = "分页查询")
        @GetMapping("/pageRecommendInfo")
        public Page<List<QueryRecommendInfoResp>> pageRecommendInfo(@ApiIgnore Page page, QueryRecommendInfoReq req) {
                return recommendInfoService.getPage(page, req);
        }

        @ApiOperation(value = "详情")
        @GetMapping("/detailRecommendInfo/{id}")
        public QueryRecommendInfoResp detail(@PathVariable Integer id) {
                return recommendInfoService.detail(id);
        }

        @ApiOperation(value = "新增或更新")
        @PostMapping("/saveOrUpdateRecommendInfo")
        public RecommendInfo saveOrUpdate(@RequestBody SaveRecommendInfoReq req) {
                log.info("接受参数:{}",req);
                log.info("文件地址:{}",req.getUrls());
                return recommendInfoService.saveOrUpdate(req);
        }

        /**@ApiOperation(value = "修改")
        @PostMapping("/update")
        public RecommendInfo update(@RequestBody SaveRecommendInfoReq req) {
                return recommendInfoService.update(req);
        }*/

        @ApiOperation(value = "删除")
        @PostMapping("/deleteRecommendInfo/{id}")
        public Integer delete(@PathVariable Integer id) {
                return recommendInfoService.delete(id);
        }

}