package com.change.config;

import cn.hutool.core.util.RandomUtil;
import com.google.common.collect.Lists;
import com.qcloud.cos.COSClient;
import com.qcloud.cos.ClientConfig;
import com.qcloud.cos.auth.BasicCOSCredentials;
import com.qcloud.cos.auth.COSCredentials;
import com.qcloud.cos.model.PutObjectRequest;
import com.qcloud.cos.model.UploadResult;
import com.qcloud.cos.region.Region;
import com.qcloud.cos.transfer.TransferManager;
import com.qcloud.cos.transfer.Upload;
import lombok.extern.slf4j.Slf4j;
import net.coobird.thumbnailator.Thumbnails;
import org.apache.kafka.common.protocol.types.Field;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDate;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 腾讯文件管理
 */
@Component
@Slf4j
public class COSClientUtil {

    private final ApplicationProperties applicationProperties;
    public COSClientUtil(ApplicationProperties applicationProperties) {
        log.info("1111111111");
        this.applicationProperties = applicationProperties;
        init();
    }

    {
        log.info("7777777777777777777");
    }
    static {
        log.info("6666666666666666666");
    }
    private ApplicationProperties.TencentOss tencentOss;
    private COSCredentials cred;
    private ClientConfig clientConfig;
    private COSClient cosClient;

    private void init() {
        tencentOss = this.applicationProperties.getTencentOss();
        cred = new BasicCOSCredentials(tencentOss.getSecretId(),tencentOss.getSecretKey()); // 初始化用户身份信息
        clientConfig = new ClientConfig(new Region(tencentOss.getRegionName())); //   设置bucket的区域
        cosClient = new COSClient(cred, clientConfig); //  生成COS客户端
    }

    /**
     * 上传文件
     *
     * @param file
     * @return
     * @throws Exception
     */
  public String upload(MultipartFile file) throws Exception {
        String date = LocalDate.now().toString();
        String originalFilename = file.getOriginalFilename();
        // 文件格式
        String extension = originalFilename.substring(originalFilename.lastIndexOf(".")+1);

        String nextId = RandomUtil.randomString(10) +"-"; // 可以优化为自动增长
        String name = nextId + originalFilename;
        String folderName = tencentOss.getFolderPrefix() + "/" + date + "/";
        String key = folderName + name;
        File localFile = null;
        try {
            Boolean isImage = Lists.newArrayList("BMP","JPG","JPEG","PNG","GIF").contains(extension.toUpperCase());
            if(isImage){
                // 图片
                log.info("图片压缩前大小：{}",file.getSize());
                localFile = transferToFileForImage(file);
                log.info("图片压缩后大小：{}",localFile.length());
            }else {
                // 非图片
                localFile = transferToFile(file);
            }
            String filePath = uploadFileToCOS(localFile, key);
            log.info("upload COS successful: {}", filePath);
            return filePath;
        } catch (Exception e) {
            throw new Exception("文件上传失败");
        } finally {
            localFile.delete();
        }
    }

    /**
     * 上传文件到COS
     *
     * @param localFile
     * @param key
     * @return
     */
    private String uploadFileToCOS(File localFile, String key) throws InterruptedException {
        PutObjectRequest putObjectRequest = new PutObjectRequest(tencentOss.getBucketName(), key, localFile);
        ExecutorService threadPool = Executors.newFixedThreadPool(8);
        // 传入一个threadPool, 若不传入线程池, 默认TransferManager中会生成一个单线程的线程池
        TransferManager transferManager = new TransferManager(cosClient, threadPool);
        // 返回一个异步结果Upload, 可同步的调用waitForUploadResult等待upload结束, 成功返回UploadResult, 失败抛出异常
        Upload upload = transferManager.upload(putObjectRequest);
        UploadResult uploadResult = upload.waitForUploadResult();
//        transferManager.shutdownNow();  // java.lang.IllegalStateException: Connection pool shut down
//        cosClient.shutdown();
        String filePath = tencentOss.getBaseUrl() + uploadResult.getKey();
        return filePath;
    }

    /**
     * 用缓冲区来实现这个转换, 即创建临时文件
     * 使用 MultipartFile.transferTo()
     *
     * @param multipartFile
     * @return
     */
    private File transferToFileForImage(MultipartFile multipartFile) throws IOException {

        String originalFilename = multipartFile.getOriginalFilename();
        String prefix = originalFilename.split("\\.")[0];
        String suffix = originalFilename.substring(originalFilename.lastIndexOf("."));
        File file = File.createTempFile(prefix, suffix);
        // 对图片进行压缩
        InputStream inputStream = multipartFile.getInputStream();
        Thumbnails.of(inputStream)
                .scale(1f)
                .outputQuality(0.5f).toFile(file);
        log.info("绝对位置：{}",file.getAbsolutePath());
//        multipartFile.transferTo(file);
        return file;
    }

    /**
     * 非图片格式，文件临时存储
     * @param multipartFile
     * @return
     * @throws IOException
     */
    private File transferToFile(MultipartFile multipartFile) throws IOException {
        String originalFilename = multipartFile.getOriginalFilename();
        String prefix = originalFilename.split("\\.")[0];
        String suffix = originalFilename.substring(originalFilename.lastIndexOf("."));
        File file = File.createTempFile(prefix, suffix);
        multipartFile.transferTo(file);
        return file;
    }
}
