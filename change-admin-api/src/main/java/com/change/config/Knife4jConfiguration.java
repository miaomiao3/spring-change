package com.change.config;

import io.swagger.annotations.ApiOperation;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2WebMvc;

@Configuration
@EnableSwagger2WebMvc
public class Knife4jConfiguration {

    private static final String splitor = ";";

    @Bean(value = "defaultApi")
    public Docket defaultApi() {

        return new Docket(DocumentationType.SWAGGER_2)
            .apiInfo(new ApiInfoBuilder()
                .title("ChangeWorld RESTful APIs")
                .description("# 改变从Hello World开始")
                .termsOfServiceUrl("https://www.xuexuejava.cn/")
                .contact(new Contact("xuexuejava", "https://www.xuexuejava.cn/", "1003632308@qq.com"))
                .version("1.0")
                .build())
            //分组名称
            .groupName("1.x版本")
            .select()
            //这里指定Controller扫描包路径
            .apis(RequestHandlerSelectors.withMethodAnnotation(ApiOperation.class))
            .paths(PathSelectors.any())
            .build();

    }

}
