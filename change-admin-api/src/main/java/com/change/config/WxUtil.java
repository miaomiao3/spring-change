package com.change.config;

import cn.hutool.core.util.StrUtil;
import com.change.core.constant.Constants;
import com.change.core.redis.Redis;
import com.change.exception.BizException;
import com.change.exception.ResultStatus;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.qcloud.cos.COSClient;
import com.qcloud.cos.ClientConfig;
import com.qcloud.cos.auth.BasicCOSCredentials;
import com.qcloud.cos.region.Region;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.security.MessageDigest;
import java.util.Arrays;
import java.util.Map;

/**
 * 微信公众号工具类
 */
@Component
@Slf4j
public class WxUtil {

    // token过期时间为1小时
    private static final long tokenTimeout = 3600 * 1;

    private final ApplicationProperties applicationProperties;

    public WxUtil(ApplicationProperties applicationProperties) {
        this.applicationProperties = applicationProperties;
        init();
    }
    private static ApplicationProperties.Weixin weixin;
    private void init() {
        weixin = this.applicationProperties.getWeixin();
    }

    /**
     * 获取appid
     * @return
     */
    public static String getAppId(){
        return weixin.getAppId();
    }
    /**
     * 调取微信接口
     * @param getAccessTokenUrl
     * @return
     */
    private static String postRequestForWeiXinService(String getAccessTokenUrl) {
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<String> postForEntity = restTemplate.postForEntity(getAccessTokenUrl, null, String.class);
        String json = postForEntity.getBody();
        return json;
    }

    /**
     * 转换格式
     * @param json
     * @return
     */
    private static Map jsonToMap(String json) {
        Gson gons = new Gson();
        Map map = gons.fromJson(json, new TypeToken<Map>(){}.getType());
        return map;
    }

    /**
     * 根据appid和secret获取AccessToken
     * @return
     */
    public static String getJSSDKAccessToken() {
        String token = null;
        // 查询redis是否存在
        token = Redis.get(Constants.WX_ACCESS_TOKEN_KEY);
        if(StrUtil.isNotBlank(token)){
            log.info("access_token未过期:{}",token);
            return token;
        }
        String url = weixin.getJssdkAccesstokenUrl().replaceAll("APPID",
                weixin.getAppId()).replaceAll("APPSECRET",
                weixin.getAppSecret());
        String json = postRequestForWeiXinService(url);
        Map map = jsonToMap(json);
        if (map != null) {
            token = (String) map.get("access_token");
            // 60_F0NO6SSU8zr_5ncCc-UVajz9xVu8EGKWn-yqzcQrFCRJsQFFRdh20KP8u3OfPv2zWXl4iYn3u-X6gGVM9-rnVQDrYMNkKgrfwMxBKSPmxieCXrio7EzyHLb0X7CeoKFdq-tMvcEbOjcSOAOrJNHiACASWQ
            // 60__AjiEZvv3AcdEaRgsrpXL_WKg_nBzW8SXsAF8c3VQUkeZOSjGg3eHZMyalOUOrWC4GEMHuga7SD0mwgQUZrbM3oo9togb_587oik3jTL3OpfG3LlugY_MJ7OXNiekr6_tsUHeDIFgEWyfddVERIdAAAREV
            // 缓存到redis中
            Redis.set(Constants.WX_ACCESS_TOKEN_KEY, token, tokenTimeout);
        }
        if(StrUtil.isBlank(token)){
            log.error("accessToken错误响应:{}",map);
            throw new BizException(ResultStatus.Common.WX_ACCESS_TOKEN_ISNULL);
        }
        log.info("首次获取access_token成功:{}",token);
        return token;
    }

    /**
     * 根据accessToken获取JssdkGetticket
     * @param accessToken
     * @return
     */
    public static String getJssdkGetticket(String accessToken) {
        String url = weixin.getJssdkGetticketUrl().replaceAll("ACCESS_TOKEN", accessToken);
        String json = postRequestForWeiXinService(url);
        Map map = jsonToMap(json);
        String jsapi_ticket = null;
        if (map != null) {
            jsapi_ticket = (String) map.get("ticket");
        }
        return jsapi_ticket;
    }

    /**
     *@param:ticket 根据accessToken生成的JssdkGetticket
     *@param:timestamp 支付签名时间戳，注意微信jssdk中的所有使用timestamp字段均为小写。但最新版的支付后台生成签名使用的timeStamp字段名需大写其中的S字符
     *@param:nonceStr 随机字符串
     *@param:url 当前网页的URL
     *@Description: 构建分享链接的签名
     */
    public static String buildJSSDKSignature(String ticket,String timestamp,String nonceStr ,String url) throws Exception {
//        String orderedString = "jsapi_ticket=" + ticket
//                + "&noncestr=" + nonceStr + "&timestamp=" + timestamp
//                + "&url=" + url;
        String[] signArr =new String[]{"url=" + url,"jsapi_ticket=" + ticket,"noncestr=" + nonceStr,"timestamp=" + timestamp};
        Arrays.sort(signArr);
//        String signStr = StringUtils.join(signArr,"&");
        String orderedString = StrUtil.join("&",signArr);
        log.info("排序后请求:{}",orderedString);
        return sha1(orderedString);
    }

    /**
     * sha1 加密JSSDK微信配置参数获取签名。
     *
     * @return
     */
    public static String sha1(String orderedString) throws Exception {
        String ciphertext = null;
        MessageDigest md = MessageDigest.getInstance("SHA-1");
        byte[] digest = md.digest(orderedString.getBytes());
        ciphertext = byteToStr(digest);
        return ciphertext.toLowerCase();
    }
    /**
     * 将字节数组转换为十六进制字符串
     *
     * @param byteArray
     * @return
     */
    private static String byteToStr(byte[] byteArray) {
        String strDigest = "";
        for (int i = 0; i < byteArray.length; i++) {
            strDigest += byteToHexStr(byteArray[i]);
        }
        return strDigest;
    }
    /**
     * 将字节转换为十六进制字符串
     *
     * @param mByte
     * @return
     */
    private static String byteToHexStr(byte mByte) {
        char[] Digit = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };
        char[] tempArr = new char[2];
        tempArr[0] = Digit[(mByte >>> 4) & 0X0F];
        tempArr[1] = Digit[mByte & 0X0F];

        String s = new String(tempArr);
        return s;
    }
    /**
     *@param:  map
     *@Description:  mapToJson
     */
    public static String mapToJson(Map map){
        Gson gson = new Gson();
        String json = gson.toJson(map);
        return json;
    }
}
