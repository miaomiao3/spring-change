package com.change;

import cn.hutool.core.convert.Convert;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONUtil;
import com.change.config.ApplicationProperties;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

@ActiveProfiles("dev")
@SpringBootTest(classes={ApplicationProperties.class})
@Slf4j
public class HutoolTest {

    @Test
    public void testConvertStrArray() {
        String str = "[\"https://change-1308848436.cos.ap-guangzhou.myqcloud.com/image/2022-10-04/w963hjib5v-分类选中.png\", \"https://change-1308848436.cos.ap-guangzhou.myqcloud.com/image/2022-10-04/m27iw18del-tang001.jpg\"]";
//        String[] strings = Convert.toStrArray(str);
        JSONArray objects = JSONUtil.parseArray(str);
        System.out.println(JSONUtil.toJsonPrettyStr(objects));
    }
}
