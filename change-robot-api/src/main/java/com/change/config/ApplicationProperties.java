package com.change.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 项目的自定义配置
 */
@Component
@ConfigurationProperties(prefix = "application", ignoreUnknownFields = false)
public class ApplicationProperties {

    private final WxMaApp wxMaApp = new WxMaApp();
    private final Website website = new Website();
    private final Minio minio = new Minio();
    private final BaiduOCR baiduOCR = new BaiduOCR();
    private final Pulsar pulsar = new Pulsar();
    private final TencentOss tencentOss = new TencentOss();

    public WxMaApp getWxMaApp() {
        return wxMaApp;
    }
    public Website getWebsite() {
        return website;
    }
    public Minio getMinio() {
        return minio;
    }
    public BaiduOCR getBaiduOCR(){
        return baiduOCR;
    }
    public Pulsar getPulsar(){
        return pulsar;
    }
    public TencentOss getTencentOss(){
        return tencentOss;
    }

    /**
     * 微信小程序配置
     */
    @Data
    public static class WxMaApp {
        private String appid;
        private String secret;
        private String token;
        private String aesKey;
        private String msgDataFormat;
    }

    @Data
    public static class Website {
        private String apiHost;
        private String appSecret;
        private String deployDir;
    }

    @Data
    public static class Minio {
        private String url;
        private String accessKey;
        private String secretKey;

    }

    /**
     * 百度OCR参数
     */
    @Data
    public static class BaiduOCR {
        private String clientId;
        private String apiKey;
        private String secretKey;
        private String accessTokenUrl;
    }

    /**
     * 腾讯消息队列
     */
    @Data
    public static class Pulsar {
        private String url;
        private String token;
        private String topicStockMsg;
        private String subPrice;
    }

    /**
     * 腾讯OSS文件管理
     */
    @Data
    @Component
    public static class TencentOss {
        private String secretId;
        private String secretKey;
        private String baseUrl;
        private String regionName;
        private String bucketName;
        private String folderPrefix;
    }
}
