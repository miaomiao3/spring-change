package com.change.config;

import com.change.core.convert.GenericTimeConverter;
import com.change.core.interceptor.JwtTokenAuthenticationInterceptor;
import com.change.core.jackson.CustomTimeModule;
import com.fasterxml.jackson.databind.Module;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.format.FormatterRegistry;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.i18n.FixedLocaleResolver;

import java.util.Locale;

/**
 * WebMvcConfiguration
 *
 * @author linqunxun
 * @date 2021/9/10 10:24 上午
 */
@Configuration
@ConditionalOnClass(WebMvcConfigurer.class)
public class WebMvcConfiguration implements WebMvcConfigurer {

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        //JwtToken 拦截器
        registry.addInterceptor(new JwtTokenAuthenticationInterceptor())
        .addPathPatterns("/boot/**")
        .excludePathPatterns("/boot/robot/token");
    }

    @Override
    public void addFormatters(FormatterRegistry registry) {
        // 针对GET请求的路径参数和POST请求的Form表单参数中的日期时间进行格式转换
        registry.addConverter(new GenericTimeConverter());
    }

    /**
     * 创建时间相关的序列器和反序列器
     * 该Module会被加载进SpringBoot创建的ObjectMapper中，会在全局生效
     *
     * @return
     */
    @Bean
    public Module customTimeModule() {
        return new CustomTimeModule();
    }

    /**
     * 修改默认validate信息为中文
     *
     * @return
     */
    @Bean
    public LocaleResolver localeResolver() {
        return new FixedLocaleResolver(Locale.CHINA);
    }
}
