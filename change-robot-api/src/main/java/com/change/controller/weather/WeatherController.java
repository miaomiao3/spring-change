package com.change.controller.weather;

import com.change.config.COSClientUtil;
import com.change.core.page.Page;
import com.change.dto.req.QueryWeatherReq;
import com.change.dto.req.SaveWeatherReq;
import com.change.dto.resp.QueryWeatherResp;
import com.change.entity.weather.Weather;
import com.change.exception.BizException;
import com.change.exception.ResultStatus;
import com.change.service.weather.WeatherService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.io.File;
import java.util.List;

/**
* @author 坏孩子是你啊
* @date  2022-08-18
*/
@Api(value = "WeatherController", tags = "天气信息管理")
@Slf4j
@RestController
@RequestMapping(value = "/boot/weather")
public class WeatherController {
        private final WeatherService weatherService;
        private final COSClientUtil cosClientUtil;

        public WeatherController(WeatherService weatherService, COSClientUtil cosClientUtil) {
                this.weatherService = weatherService;
                this.cosClientUtil = cosClientUtil;
        }

        @ApiImplicitParams({
        @ApiImplicitParam(name = "current", value = "当前页", defaultValue = "1"),
        @ApiImplicitParam(name = "size", value = "每页数量", defaultValue = "100")
        })
        @ApiOperation(value = "分页查询")
        @GetMapping("/page")
        public Page<List<QueryWeatherResp>> page(@ApiIgnore Page page, QueryWeatherReq req) {
                return weatherService.getPage(page, req);
        }

        @ApiOperation(value = "详情")
        @GetMapping("/detail/{id}")
        public QueryWeatherResp detail(@PathVariable Integer id) {
                return weatherService.detail(id);
        }

        @ApiOperation(value = "新增")
        @PostMapping("/save")
        public Weather save(@RequestBody SaveWeatherReq req) {
                return weatherService.save(req);
        }

        @ApiOperation(value = "修改")
        @PostMapping("/update")
        public Weather update(@RequestBody SaveWeatherReq req) {
                return weatherService.update(req);
        }

        @ApiOperation(value = "删除")
        @PostMapping("/delete/{id}")
        public Integer delete(@PathVariable Integer id) {
                return weatherService.delete(id);
        }

        @ApiOperation(value = "生成天气图片")
        @GetMapping("/image")
        public String generateImage() {
                File file = weatherService.generateImage();
                if(file == null){
                        throw new BizException(ResultStatus.Common.NOT_FOUND);
                }
                String url = null;
                try {
                        url = cosClientUtil.uploadFile(file);
                } catch (Exception e) {
                        e.printStackTrace();
                }
                return url;
        }
}