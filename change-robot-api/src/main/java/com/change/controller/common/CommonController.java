package com.change.controller.common;


import cn.hutool.core.util.StrUtil;
import com.change.config.ApplicationProperties;
import com.change.config.COSClientUtil;
import com.change.exception.BizException;
import com.change.exception.ResultStatus;
import com.change.service.common.CommonService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import springfox.documentation.annotations.ApiIgnore;

import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * 通用接口，前端sql执行调用
 */
@Api(value = "CommonController", tags = "通用接口")
@Slf4j
@RestController
@RequestMapping(value = "/api/common")
public class CommonController {

    private final COSClientUtil cosClientUtil;
    private final ApplicationProperties applicationProperties;

    public CommonController(COSClientUtil cosClientUtil, ApplicationProperties applicationProperties) {
        this.cosClientUtil = cosClientUtil;
        this.applicationProperties = applicationProperties;
    }

    /**
     * 腾讯云COS:上传文件
     * @param file
     * @return
     * @throws Exception
     */
    @ApiOperation(value = "腾讯云COS:上传文件")
    @PostMapping(value = "/cosUpload")
    public String cosUpload(@RequestParam("file") MultipartFile file) throws Exception {
        String filePath = cosClientUtil.upload(file);
        return filePath;
    }

    @ApiOperation(value = "执行deploy_pro.sh脚本", httpMethod = "Get")
    @GetMapping("/deploy")
    public Integer execShell() {
        int i = 0;
        try {
            String deployDir = applicationProperties.getWebsite().getDeployDir();
            Process ps = Runtime.getRuntime().exec(deployDir);
            i = ps.waitFor();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }catch (IOException e) {
            e.printStackTrace();
        }
        return i;
    }

}
