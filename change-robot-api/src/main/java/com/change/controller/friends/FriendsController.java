package com.change.controller.friends;

import com.change.core.page.Page;
import com.change.dto.req.QueryFriendsReq;
import com.change.dto.req.SaveFriendsReq;
import com.change.dto.resp.QueryFriendsResp;
import com.change.entity.friends.Friends;
import com.change.service.friends.FriendsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;

/**
* @author 坏孩子是你啊
* @date  2022-08-18
*/
@Api(value = "FriendsController", tags = "好友管理")
@Slf4j
@RestController
@RequestMapping(value = "/boot/friends")
public class FriendsController {
        private final FriendsService friendsService;

        public FriendsController(FriendsService friendsService) {
                this.friendsService = friendsService;
        }

        @ApiImplicitParams({
        @ApiImplicitParam(name = "current", value = "当前页", defaultValue = "1"),
        @ApiImplicitParam(name = "size", value = "每页数量", defaultValue = "100")
        })
        @ApiOperation(value = "分页查询")
        @GetMapping("/page")
        public Page<List<QueryFriendsResp>> page(@ApiIgnore Page page, QueryFriendsReq req) {
                return friendsService.getPage(page, req);
        }

        @ApiOperation(value = "详情")
        @GetMapping("/detail/{id}")
        public QueryFriendsResp detail(@PathVariable Integer id) {
                return friendsService.detail(id);
        }

        @ApiOperation(value = "新增")
        @PostMapping("/save")
        public Friends save(@RequestBody SaveFriendsReq req) {
                return friendsService.save(req);
        }

        @ApiOperation(value = "修改")
        @PostMapping("/update")
        public Friends update(@RequestBody SaveFriendsReq req) {
                return friendsService.update(req);
        }

        @ApiOperation(value = "删除")
        @PostMapping("/delete/{id}")
        public Integer delete(@PathVariable Integer id) {
                return friendsService.delete(id);
        }

}