package com.change.controller.robot;

import com.change.core.page.Page;
import com.change.dto.req.QueryRobotReq;
import com.change.dto.req.SaveRobotReq;
import com.change.dto.resp.QueryRobotResp;
import com.change.entity.robot.Robot;
import com.change.service.robot.RobotService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;

/**
* @author 坏孩子是你啊
* @date  2022-08-18
*/
@Api(value = "RobotController", tags = "机器人管理")
@Slf4j
@RestController
@RequestMapping(value = "/boot/robot")
public class RobotController {
        private final RobotService robotService;

        public RobotController(RobotService robotService) {
                this.robotService = robotService;
        }

        @ApiImplicitParams({
        @ApiImplicitParam(name = "current", value = "当前页", defaultValue = "1"),
        @ApiImplicitParam(name = "size", value = "每页数量", defaultValue = "100")
        })
        @ApiOperation(value = "分页查询")
        @GetMapping("/page")
        public Page<List<QueryRobotResp>> page(@ApiIgnore Page page, QueryRobotReq req) {
                return robotService.getPage(page, req);
        }

        @ApiOperation(value = "详情")
        @GetMapping("/detail/{id}")
        public Robot detail(@PathVariable Integer id) {
                return robotService.detail(id);
        }

        @ApiOperation(value = "新增")
        @PostMapping("/save")
        public Robot save(@RequestBody SaveRobotReq req) {
                return robotService.save(req);
        }

        @ApiOperation(value = "修改")
        @PostMapping("/update")
        public Robot update(@RequestBody SaveRobotReq req) {
                return robotService.update(req);
        }

        @ApiOperation(value = "删除")
        @PostMapping("/delete/{id}")
        public Integer delete(@PathVariable Integer id) {
                return robotService.delete(id);
        }

        @ApiOperation(value = "获取token")
        @PostMapping("/token")
        public String getToken(@RequestBody QueryRobotResp robotResp) {
                return robotService.getToken(robotResp);
        }

}