package com.change;

import com.change.config.ApplicationProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * @author tongzhaomei
 * @version 1.0
 * @time 2022-07-15 12-03
 */
@EnableScheduling
@EnableConfigurationProperties({ ApplicationProperties.class })
@SpringBootApplication
@Slf4j
public class RobotApplication {

    private static ApplicationProperties applicationProperties;

    public RobotApplication(ApplicationProperties applicationProperties) {
        this.applicationProperties = applicationProperties;
    }

    public static void main(String[] args) {
        SpringApplication.run(RobotApplication.class, args);
        log.info("===>机器人项目启动完成==>");
    }
}
