package com.change;

import com.change.core.dto.Poster;
import com.change.core.util.PosterUtil;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

@Slf4j
public class ImageTest {

    @Test
    public void postImage() throws Exception {
        String qrCodeUrl = "http://m.xinjuenet.com/images/banner/i-wx.jpg"; //二维码
        String goodsUrl = "http://m.xinjuenet.com/images/banner/b-xldh.jpg"; //顶部图片
        String avatarUrl = "http://donghaipeng-web.heifeng.xin/static/img/normalFace.06a38ba0.png"; //头像
        String name = "智能机器人";
        String desc = "服务项目：突发天气预警；考试预约提醒；股票分析；大数据分析；精准推送；";
        String price = "99.99/月";

        Poster poster_ = new Poster();
        poster_.setAvatarUrl(avatarUrl);
        poster_.setName(name);
        poster_.setWidth(375);
        poster_.setHeight(670);
        poster_.setQrCodeUrl(qrCodeUrl); //二维码
        // poster_.setGoodsUrl(goodsUrl); //顶部banner
        poster_.setDesc(desc); //测评结果说明
        // poster_.setPrice(price); //测评结果标题

        Poster poster = PosterUtil.initPoster(poster_);
        PosterUtil.drawPoster(poster);

    }
}
