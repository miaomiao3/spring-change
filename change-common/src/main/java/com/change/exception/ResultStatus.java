package com.change.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 返回状态的定义
 * 有新的返回状态类型时，应当明确其错误码的范围
 */
public interface ResultStatus {


    /**
     * 通用的错误码
     */
    @Getter
    @AllArgsConstructor
    enum Common implements ResultStatus {
        SUCCESS(0, "成功"),
        ERROR_UNKNOWN(-1, "未知错误"),
        ERROR_SESSION_ERROR(-2, "因页面停留超时或账号发生变动，请重新登录。"),
        ERROR_SQL_RESULT_ERROR(-3, "数据操作错误"),
        ERROR_PARAM_NOT_VALID(-4, "请求参数不合法错误"),
        ERROR_SYSTEM(-5, "服务繁忙，请稍后重试"),
        NOT_FOUND(-6, "未找到资源"),
        ERROR_PASSWORD(-7, "原密码不正确"),
        ERROR_UPLOAD_FILE(-8, "上传文件报错"),
        ERROR_FORMAT_FILE(-9, "文件格式不正确"),
        ERROR_PARAM_NOT_NULL(-10, "请求参数不能为空"),
        NOT_NULL_SQL(-12, "SQL语句为空或格式不正确"),
        INVALIDATE_SQL(-13, "SQL语句不合法,存在恶意行为"),
        UPDATE_VALIDATE_SQL(-14, "更新语句存在恶意操作,被拒绝执行"),
        INSERT_VALIDATE_SQL(-15, "新增语句存在恶意操作,被拒绝执行"),
        ERROR_PASSWORD_ACCOUNT(-16,"账号或密码错误"),
        NOT_FOUNT_BROWSER_UUID(-17,"浏览器唯一标识不能为空"),
        NOT_FOUNT_WECHAT_LIST(-18,"微信好友集合为空"),

        NOT_FOUNT_ROBOT(-19,"机器人账户不存在"),

        ROBOT_TOKEN_EXPIRED(-20, "token已过期"),
        ROBOT_TOKEN_ISNULL(-21, "token为空"),

        LOGIN_TOKEN_EXPIRED(-22, "token已过期"),
        LOGIN_TOKEN_ISNULL(-23, "token为空"),
        WX_ACCESS_TOKEN_ISNULL(-24, "获取access_token为空"),
        WX_URL_ISNULL(-24, "url为空"),
        ;
        private int retcode;
        private String msg;
    }

    /**
     * 鉴权的错误码，预留10001～10020
     */
    @Getter
    @AllArgsConstructor
    enum Auth implements ResultStatus {
        ERROR_USER_NAME(10001, "用户名错误"),
        ERROR_PASSWORD(10001, "密码错误"),
        PARAMS_ERROR(10001, "用户名密码错误"),
        ERROR_DECRYPT(10004,"解密失败"),
        ERROR_ACCOUNT_EXIST(10005,"账号已存在"),
        ERROR_COMPANY_EXIST(10006,"公司已存在"),

        ;
        private int retcode;
        private String msg;
    }



    /**
     * 返回码
     */
    int getRetcode();

    /**
     * 错误信息，返回码为0时，该值为空
     */
    String getMsg();
}
