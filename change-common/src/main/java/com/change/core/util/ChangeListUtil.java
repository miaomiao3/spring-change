package com.change.core.util;

import java.util.List;

import static java.util.stream.Collectors.toList;

/**
 * 集合工具类
 */
public class ChangeListUtil {


    /**
     * 两个集合的交集，返回数据为：待更新的列表
     */
    public static List<Integer> intersectionList(List<Integer> list1, List<Integer> list2){
        List<Integer> result = list1.stream().filter(item -> list2.contains(item)).collect(toList());
        return result;
    }

    /**
     * 两个集合的差集，返回数据为：待新增或待删除的列表
     */
    public static List<Integer> reduceList(List<Integer> list1,List<Integer> list2){
        List<Integer> result = list1.stream().filter(item -> !list2.contains(item)).collect(toList());
        return result;
    }
}
