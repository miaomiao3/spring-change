package com.change.core.util;

import cn.hutool.jwt.JWTException;
import cn.hutool.jwt.JWTPayload;
import cn.hutool.jwt.JWTUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

@Slf4j
public class TokenUtil {



    /**
     * 获取request
     *
     * @return
     */
    public static HttpServletRequest getRequest() {
        ServletRequestAttributes requestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        return requestAttributes == null ? null : requestAttributes.getRequest();
    }



    /**
     * 获取请求token
     *
     * @param request
     * @return
     */
    public static String getToken(HttpServletRequest request) {
        String token = request.getHeader(HttpHeaders.AUTHORIZATION);
        if (token == null) {
            token = request.getHeader("token");
        }
        if ("".equals(token)) {
            token = null;
        }
        return token;
    }

    /**
     * 通过token获取用户id
     * @return
     */
    public static Integer getUserId() {
        try {
            HttpServletRequest request = getRequest();
            String token = getToken(request);
            // 解析用户id
            Integer userId = (Integer) JWTUtil.parseToken(token).getPayload().getClaim(JWTPayload.AUDIENCE);
            log.info("当前用户id:{}",userId);
            return userId;
        }catch (JWTException e){
            log.error(e.getMessage());
            return 0;
        }
    }


}
