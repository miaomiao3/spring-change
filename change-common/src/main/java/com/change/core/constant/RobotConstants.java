package com.change.core.constant;

/**
 * @author jhlz
 * @time 2022/8/19 22:23
 * @desc: RobotConstants 机器人常量
 */
public class RobotConstants {

    // jwt key
    public static final String JWT_KEY = "robot";
}
