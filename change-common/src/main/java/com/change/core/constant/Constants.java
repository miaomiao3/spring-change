package com.change.core.constant;

/**
 * @author jhlz
 * @time 2022/8/22 16:00
 * @desc: Constants 后端接口常量
 */
public class Constants {

    // login jwt key
    public static final String LOGIN_USER_KEY = "login_user_key";

    // anonymous login jwt key
    public static final String ANONYMOUS_LOGIN_USER_KEY = "anonymous_login_user_key";

    // 微信公众号 access_token
    public static final String WX_ACCESS_TOKEN_KEY = "wx_access_token_key";
}
