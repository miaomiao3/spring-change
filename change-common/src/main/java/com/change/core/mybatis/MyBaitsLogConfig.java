package com.change.core.mybatis;

import org.apache.ibatis.logging.Log;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class MyBaitsLogConfig implements Log{

    private Logger log;

    public MyBaitsLogConfig(String clazz) {
        log = LoggerFactory.getLogger(clazz);
    }

    @Override
    public boolean isDebugEnabled() {
//        return log.isDebugEnabled();
        return true; // 默认开启 Debug
    }

    @Override
    public boolean isTraceEnabled() {
//        return log.isTraceEnabled();
        return true; // 默认开启 Trace
    }

    @Override
    public void error(String s, Throwable e) {
        log.error(s,e);
    }

    @Override
    public void error(String s) {
        log.error(s);
    }

    @Override
    public void debug(String s) {
        if(s.indexOf("SELECT")!=-1){
            log.info("======>>>查询语句===>>>");
        }else if(s.indexOf("UPDATE")!=-1){
            log.info("======>>>更新语句===>>>");
        }else if(s.indexOf("INSERT")!=-1){
            log.info("======>>>新增语句===>>>");
        }else if(s.indexOf("DELETE")!=-1){
            log.info("======>>>删除语句===>>>");
        }
        log.info(s);  //SQL 获取参数输出至 info
    }

    @Override
    public void trace(String s) {
        log.trace(s); //SQL 信息输出至 info
    }

    @Override
    public void warn(String s) {
        log.warn(s);
    }
}
