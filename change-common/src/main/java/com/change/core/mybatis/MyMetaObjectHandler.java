package com.change.core.mybatis;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import com.change.core.util.TokenUtil;
import com.change.core.web.request.AccountContext;
import org.apache.ibatis.reflection.MetaObject;

import java.time.LocalDateTime;

public class MyMetaObjectHandler implements MetaObjectHandler {

    @Override
    public void insertFill(MetaObject metaObject) {
        // jwt 中token 解析 公司id 账号id TODO
        Integer companyId = 0;
        Integer userId = TokenUtil.getUserId();
        this.strictInsertFill(metaObject, "createTime", () -> LocalDateTime.now(), LocalDateTime.class);
        this.strictInsertFill(metaObject, "updateTime", () -> LocalDateTime.now(), LocalDateTime.class);
        this.strictInsertFill(metaObject, "creatorId", () -> userId, Integer.class);
        this.strictInsertFill(metaObject, "updateId", () -> userId, Integer.class);
        this.strictInsertFill(metaObject, "companyId", () -> companyId, Integer.class);
    }

    @Override
    public void updateFill(MetaObject metaObject) {
        // jwt 中token 解析 公司id 账号id TODO
        Integer userId = TokenUtil.getUserId();
        this.strictUpdateFill(metaObject, "updateTime", () -> LocalDateTime.now(), LocalDateTime.class);
        this.strictInsertFill(metaObject, "updateId", () -> userId, Integer.class);
    }
}
