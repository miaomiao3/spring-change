package com.change.core.mybatis;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.plugins.handler.TenantLineHandler;
import com.change.core.web.request.AccountContext;
import net.sf.jsqlparser.expression.Expression;
import net.sf.jsqlparser.expression.LongValue;
import org.reflections.Reflections;

import java.util.HashSet;
import java.util.Set;

/**
 * MyTenantLineHandler
 *
 * @author linqunxun
 * @date 2021/9/17 12:09 下午
 */
public class MyTenantLineHandler implements TenantLineHandler {

    private final static Set<String> tenantBeanNames = new HashSet<>();

    static {
        Reflections reflections = new Reflections("com.wisight");
        reflections.getTypesAnnotatedWith(TableName.class).forEach(clazz -> {
           /* if (OwnerEntity.class.isAssignableFrom(clazz)) {
                tenantBeanNames.add(clazz.getAnnotation(TableName.class).value());
            }*/
        });
    }

    @Override
    public Expression getTenantId() {
        return new LongValue(AccountContext.getAccountId());
    }

    @Override
    public String getTenantIdColumn() {
        return "company_id";
    }

    @Override
    public boolean ignoreTable(String tableName) {
        return !tenantBeanNames.contains(tableName);
    }

}
