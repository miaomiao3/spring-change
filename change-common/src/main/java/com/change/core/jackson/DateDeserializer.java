package com.change.core.jackson;

import cn.hutool.core.util.StrUtil;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

import java.io.IOException;
import java.util.Date;

/**
 * DateDeserializer
 *
 * @author linqunxun
 * @date 2021/9/10 10:24 上午
 */
public class DateDeserializer extends StdDeserializer<Date> {

    public DateDeserializer() {
        super(Date.class);
    }

    @Override
    public Date deserialize(JsonParser parser, DeserializationContext ctxt) throws IOException {
        String text = parser.getText();
        if (StrUtil.isEmpty(text)) {
            return null;
        }
        try {
            return new Date(Long.parseLong(text));
        } catch (Exception e) {
            throw new RuntimeException("日期格式有误！");
        }
    }

}
