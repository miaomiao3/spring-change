package com.change.core.jackson;

import com.fasterxml.jackson.databind.module.SimpleModule;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * CustomTimeModule
 *
 * @author linqunxun
 * @date 2021/9/10 10:24 上午
 */
public final class CustomTimeModule extends SimpleModule {

    public CustomTimeModule() {
        super("customTimeModule");
        addSerializer(LocalDateTime.class, new  LocalDateTimeSerializer());
        addSerializer(LocalDate.class, new  LocalDateSerializer());
        addSerializer(Date.class, new  DateSerializer());

        addDeserializer(LocalDateTime.class, new  LocalDateTimeDeserializer());
        addDeserializer(LocalDate.class, new  LocalDateDeserializer());
        addDeserializer(Date.class, new DateDeserializer());
    }
}
