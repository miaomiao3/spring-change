package com.change.core.convert;

import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.util.Locale;

public class DateFormatter {
    /**
     * 自定义的日期时间格式器，"yyyy-MM-dd HH:mm:ss"
     */
    public final static DateTimeFormatter LOCAL_DATE_TIME =
            new DateTimeFormatterBuilder()
                    .parseCaseInsensitive()
                    .append(DateTimeFormatter.ISO_LOCAL_DATE)
                    .appendLiteral(' ')
                    .append(DateTimeFormatter.ISO_LOCAL_TIME)
                    .toFormatter(Locale.CHINA);
}
