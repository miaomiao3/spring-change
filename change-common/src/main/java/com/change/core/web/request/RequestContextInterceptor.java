package com.change.core.web.request;

import cn.hutool.core.util.StrUtil;
import com.change.exception.BizException;
import com.change.exception.ResultStatus;
import org.springframework.http.HttpHeaders;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Objects;

/**
 * 请求上下文拦截器
 * <p>
 * 用于在请求上下文中设置一些信息
 */
public class RequestContextInterceptor implements HandlerInterceptor {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
       /* String authorization = request.getHeader(HttpHeaders.AUTHORIZATION);
        String token = null;
        if (StrUtil.isNotEmpty(authorization)) {
            token = authorization.replace("Bearer ", "").trim();
        }
        if (Objects.isNull(token)) {
            throw new BizException(ResultStatus.Common.ERROR_SESSION_ERROR);
        }
        if(!AccountContext.initThreadSession(token)){
            throw new BizException(ResultStatus.Common.ERROR_SESSION_ERROR);
        }*/
        return true;
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        AccountContext.clearThreadSession();
    }
}
