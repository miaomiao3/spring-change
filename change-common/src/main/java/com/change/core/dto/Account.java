package com.change.core.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class Account {

    @ApiModelProperty("主键")
    private Integer id;

    @ApiModelProperty(value = "手机号")
    private String mobile;

    @ApiModelProperty(value = "昵称")
    private String nickname;

    @ApiModelProperty(value = "账号")
    private String account;

    @ApiModelProperty(value = "备注")
    private String uuid;

    @ApiModelProperty(value = "密码")
    private String password;

    @ApiModelProperty(value = "头像")
    private String avatar;

    @ApiModelProperty(value = "盐值")
    private String salt;


}
