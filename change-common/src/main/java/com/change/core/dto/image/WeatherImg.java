package com.change.core.dto.image;

import com.change.core.dto.Poster;
import lombok.Data;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.geom.Ellipse2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.Serializable;
import java.net.URL;
import java.time.Instant;

/**
 * 天气预报
 * @author tongzhaomei
 * @version 1.0
 * @time 2022-08-19 10-51
 */
@Data
public class WeatherImg implements Serializable {

    // 画布
    private int width = 160;//宽（像素为单位）
    private int height = 260;//高

    //天气状态图标地址
    private String statusUrl;
    private int statusX;
    private int statusY;
    private int statusWidth;
    private int statusHeight;

    //时间
    private String createTime;
    private Font timeFont;
    private Color timeColor;
    private int timeX;
    private int timeY;

    //温度
    private String temperature;
    private Font temperatureFont;
    private Color temperatureColor;
    private int temperatureX;
    private int temperatureY;

    //描述
    private String desc;
    private Font descFont;
    private Color descColor;
    private int descX;
    private int descY;


    public static WeatherImg init(WeatherImg image_) {
        WeatherImg image = new WeatherImg();
        //天气状态图片
        image.setStatusUrl(image_.getStatusUrl());
        image.setStatusWidth(75);
        image.setStatusHeight(80);
        image.setStatusX(50); // x轴
        image.setStatusY(80); // y轴
        //时间
        image.setCreateTime(image_.getCreateTime());
        image.setTimeColor(Color.black);
        image.setTimeFont(new Font("微软雅黑", Font.PLAIN, 14));
        image.setTimeX(10);
        image.setTimeY(30);
        //温度
        image.setTemperature(image_.getTemperature());
        image.setTemperatureColor(Color.BLACK);
        image.setTemperatureFont(new Font("微软雅黑", Font.PLAIN, 20));
        image.setTemperatureX(image_.getWidth() / 4 + 10 );
        image.setTemperatureY(image.getStatusHeight() + 80 + 20);
        //描述
        image.setDesc(image_.getDesc());
        image.setDescColor(Color.BLACK);
        image.setDescFont(new Font("微软雅黑", Font.BOLD, 18));
        image.setDescX(image_.getWidth() / 4 + 30 );
        image.setDescY(image.getStatusHeight() + 80 + 60);
        return image;
    }

    public static File draw(WeatherImg image) throws Exception {
        long startTime = System.currentTimeMillis();
        String statusUrl = image.getStatusUrl();
        BufferedImage statusImage = ImageIO.read(new URL(statusUrl));
        int width = image.getWidth();
        int height = image.getHeight();
        //画布
        BufferedImage canvas = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        Graphics2D g = (Graphics2D) canvas.getGraphics();
        g.setBackground(Color.lightGray);
//        g.setBackground(new Color(233, 253, 255));//设置背景色
        g.clearRect(0, 0, width, height);

        // 设置文字抗锯齿
        g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_LCD_HRGB);
        // 写入温度
        g.setColor(image.getTemperatureColor());
        g.setFont(image.getTemperatureFont());
        g.drawString(image.getTemperature(), image.getTemperatureX(), image.getTemperatureY());
        // 写入时间
        g.setFont(image.getTimeFont());
        g.setColor(image.getTimeColor());
        g.drawString(image.getCreateTime(), image.getTimeX(), image.getTimeY());

        //画商品
        g.drawImage(statusImage.getScaledInstance(image.getStatusWidth(), image.getStatusHeight(), Image.SCALE_SMOOTH), image.getStatusX(), image.getStatusY(), null);
        //描述
        g.setColor(image.getDescColor());
        g.setFont(image.getDescFont());
        g.drawString(image.getDesc(), image.getDescX(), image.getDescY());

        g.dispose();
        File resultImg = new File("data/image/weather/" + Instant.now().getEpochSecond() + ".png");
        if (!resultImg.exists()) {
            resultImg.mkdirs();
        }
        ImageIO.write(canvas, "png", resultImg);
        //上传服务器代码
        //ByteArrayOutputStream bs = new ByteArrayOutputStream();
        //ImageOutputStream imgOut = ImageIO.createImageOutputStream(bs);
        //ImageIO.write(canvas, "png", imgOut);
        //InputStream inSteam = new ByteArrayInputStream(bs.toByteArray());
        //String url = OSSFactory.build().upload(inSteam, UUID.randomUUID().toString()+".png");
        System.out.println("生成成功！");
        System.out.println("耗时: " + (System.currentTimeMillis() - startTime) / 1000.0 + "s");
        System.out.println("生成文件路径: " + resultImg.getAbsolutePath());
        return resultImg;
    }


    /**
     * rgb转int类型
     * @param rgb
     * @return
     */
    public static int rgbToInt(int[] rgb) {
        int r = (rgb[0]<<16)&0x00ff0000;
        int g = (rgb[1]<<8)&0x0000FF00;
        int b = rgb[2]&0x000000FF;
        int i =0xFF000000 | r | g | b;
        return i;
    }

}
