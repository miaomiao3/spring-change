package com.change.core.dto;

import lombok.Data;

import java.awt.*;
import java.io.Serializable;

/**
 * @author tongzhaomei
 * @version 1.0
 * @time 2022-06-08 10-51
 */
@Data
public class Poster implements Serializable {

    private int width;//海报的宽（像素为单位）
    private int height;//海报的高
    //头像
    private String avatarUrl;//头像url
    private int avatarX;//头像左上角横坐标
    private int avatarY;//头像左上角纵坐标
    private int avatarWidth;//头像宽
    private int avatarHeight;//头像高
    private boolean isCircle;//是否圆形头像
    //名字
    private String name;
    private Font nameFont;
    private Color nameColor;
    private int nameX;
    private int nameY;
    //天气状态图标地址
    private String statusUrl;
    private int statusX;
    private int statusY;
    private int statusWidth;
    private int statusHeight;
    //温度
    private String temperature;
    private Font temperatureFont;
    private Color temperatureColor;
    private int temperatureX;
    private int temperatureY;
    private int temperatureWidth;
    private int temperatureHeight;
    //描述
    private String desc;
    private Font descFont;
    private Color descColor;
    private int descX;
    private int descY;
    //时间
    private String createTime;
    private Font timeFont;
    private Color timeColor;
    private int timeX;
    private int timeY;
    //小程序码
    private String qrCodeUrl;
    private int qrCodeX;
    private int qrCodeY;
    private int qrCodeWidth;
    private int qrCodeHeight;
    //提示1
    private String tip1;
    private Font tip1Font;
    private Color tip1Color;
    private int tip1X;
    private int tip1Y;
    //底部栏
    private Color footerColor;
    private int footerWidth;
    private int footerHeight;
    private int footerX;
    private int footerY;
    //底部栏提示字
    private String footerTip;
    private Font footerTipFont;
    private Color footerTipColor;
    private int footerTipX;
    private int footerTipY;


}
