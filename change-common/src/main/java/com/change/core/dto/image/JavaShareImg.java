package com.change.core.dto.image;

import lombok.Data;

import java.io.Serializable;

/**
 * java技术分享
 * @author tongzhaomei
 * @version 1.0
 * @time 2022-08-19 10-51
 */
@Data
public class JavaShareImg implements Serializable {

    // 画布高和宽
    private int width;//宽（像素为单位）
    private int height;//高


}
