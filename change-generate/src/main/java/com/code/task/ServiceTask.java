package com.code.task;

import com.code.configs.CommonConfig;
import com.code.configs.CommonUnit;
import com.code.db.TableInformation;
import com.code.task.base.AbstractTask;
import com.code.utils.FileUtil;
import com.code.utils.FreemarketConfigUtils;
import freemarker.template.TemplateException;

import java.io.IOException;
import java.util.Map;

public class ServiceTask extends AbstractTask {


    public ServiceTask(TableInformation tableInformation) {
        super(tableInformation);
    }

    @Override
    public void run() throws IOException, TemplateException {
        //填充数据
        CommonConfig commonConfig = new CommonConfig(tableInformation.getBaseClassName());
        CommonUnit thisUnit = commonConfig.ServiceUnit;
        Map<String, String> ftlData = commonConfig.ftlDataMap;

        String filePath = thisUnit.completePackageName;
        String fileName = thisUnit.classFileName + ".java";

        //文件
        FileUtil.generateToJava(FreemarketConfigUtils.TYPE_SERVICE, ftlData, filePath, fileName);
    }
}
