package com.code.task.base;

import com.code.db.TableInformation;
import freemarker.template.TemplateException;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;

public abstract class SingleAbstractTask implements Serializable {
    protected TableInformation tableInformation;

    public SingleAbstractTask(TableInformation tableInformation) {
        this.tableInformation = tableInformation;
    }

    public abstract void run() throws IOException, TemplateException;

    @Deprecated
    protected void createFilePathIfNotExists(String filePath) {
        File file = new File(filePath);
        if (!file.exists()) { // 检测文件路径是否存在，不存在则创建
            file.mkdir();
        }
    }

}
