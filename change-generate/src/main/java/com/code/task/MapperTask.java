package com.code.task;

import com.code.configs.CommonConfig;
import com.code.configs.CommonUnit;
import com.code.db.TableInformation;
import com.code.task.base.AbstractTask;
import com.code.utils.FileUtil;
import com.code.utils.FreemarketConfigUtils;
import freemarker.template.TemplateException;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class MapperTask extends AbstractTask {
    public MapperTask(TableInformation tableInformation) {
        super(tableInformation);
    }

    @Override
    public void run() throws IOException, TemplateException {
        //填充数据
        Map<String, String> ftlData = new HashMap<>();
        CommonConfig commonConfig = new CommonConfig(tableInformation.getBaseClassName());
        CommonUnit thisUnit = commonConfig.MapperUnit;
        ftlData = commonConfig.ftlDataMap;

        String filePath = thisUnit.completePackageName;
        String fileName = thisUnit.classFileName + ".xml";

        // 生成Mapper文件
        FileUtil.generateToJava(FreemarketConfigUtils.TYPE_MAPPER, ftlData, filePath, fileName);
    }


}
