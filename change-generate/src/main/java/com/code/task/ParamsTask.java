package com.code.task;

import com.code.configs.CommonConfig;
import com.code.configs.CommonUnit;
import com.code.db.TableInformation;
import com.code.entity.ColumnInfo;
import com.code.task.base.AbstractTask;
import com.code.utils.FileUtil;
import com.code.utils.FreemarketConfigUtils;
import com.code.utils.GeneratorUtil;
import freemarker.template.TemplateException;

import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * @author 坏孩子是你啊
 * @date 2022/4/29
 */

public class ParamsTask extends AbstractTask {

    public ParamsTask(TableInformation tableInformation) {
        super(tableInformation);
    }

    @Override
    public void run() throws IOException, TemplateException {
        //填充数据
        List<ColumnInfo> tableInfos = tableInformation.getTableInfos();
        CommonConfig commonConfig = new CommonConfig(tableInformation.getBaseClassName());
        CommonUnit thisUnit = commonConfig.Params;
        Map<String, String> ftlData = commonConfig.ftlDataMap;
        ftlData.put("EntityClassFileName", tableInformation.getBaseClassName());
        ftlData.put("Properties", GeneratorUtil.generateDhpEntityProperties(tableInfos));

        String filePath = thisUnit.completePackageName;
        String fileName = tableInformation.getBaseClassName() + "Params" + ".java";
        // 生成文件
        FileUtil.generateToJava(FreemarketConfigUtils.TYPE_PARAMS, ftlData, filePath, fileName);
    }
}

