package com.code.task;

import com.code.configs.CommonConfig;
import com.code.configs.CommonUnit;
import com.code.db.TableInformation;
import com.code.task.base.AbstractTask;
import com.code.utils.FileUtil;
import com.code.utils.FreemarketConfigUtils;
import freemarker.template.TemplateException;

import java.io.IOException;
import java.util.Map;


public class DaoTask extends AbstractTask {


    public DaoTask(TableInformation tableInformation) {
        super(tableInformation);
    }

    @Override
    public void run() throws IOException, TemplateException {
        //生成填充数据
        CommonConfig commonConfig = new CommonConfig(tableInformation.getBaseClassName());
        CommonUnit thisUnit = commonConfig.DaoUnit;
        Map<String, String> ftlData = commonConfig.ftlDataMap;
        ftlData.put("TableName", tableInformation.getTableName());


        String filePath = thisUnit.completePackageName;
        String fileName = thisUnit.classFileName + ".java";
        //生成文件
        FileUtil.generateToJava(FreemarketConfigUtils.TYPE_DAO, ftlData, filePath, fileName);
    }
}
