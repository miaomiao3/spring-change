package com.code.task;

import com.code.configs.CommonConfig;
import com.code.configs.CommonUnit;
import com.code.db.TableInformation;
import com.code.task.base.AbstractTask;
import com.code.utils.FileUtil;
import com.code.utils.FreemarketConfigUtils;
import com.code.utils.StringUtil;
import freemarker.template.TemplateException;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.util.Map;

@Slf4j
public class ControllerTask extends AbstractTask {

    public ControllerTask(TableInformation tableInformation) {
        super(tableInformation);
    }

    @Override
    public void run() throws IOException, TemplateException {
        //填充数据
        CommonConfig commonConfig = new CommonConfig(tableInformation.getBaseClassName());
        CommonUnit thisUnit = commonConfig.ControllerUnit;
        Map<String, String> ftlData = commonConfig.ftlDataMap;
        ftlData.put("EntityLowerName", StringUtil.firstToLowerCase(commonConfig.BaseClassName));

        String filePath = thisUnit.completePackageName;
        String fileName = thisUnit.classFileName + ".java";
        //生成文件
        FileUtil.generateToJava(FreemarketConfigUtils.TYPE_CONTROLLER, ftlData, filePath, fileName);
    }
}
