package com.code.task;

import com.code.configs.CommonConfig;
import com.code.configs.CommonUnit;
import com.code.db.TableInformation;
import com.code.entity.ColumnInfo;
import com.code.task.base.AbstractTask;
import com.code.utils.FileUtil;
import com.code.utils.FreemarketConfigUtils;
import com.code.utils.GeneratorUtil;
import freemarker.template.TemplateException;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.util.List;
import java.util.Map;
@Slf4j
public class EntityTask extends AbstractTask {

    public EntityTask(TableInformation tableInformation) {
        super(tableInformation);
    }

    @Override
    public void run() throws IOException, TemplateException {
        //填充数据
        CommonConfig commonConfig = new CommonConfig(tableInformation.getBaseClassName());
        CommonUnit thisUnit = commonConfig.EntityUnit;
        List<ColumnInfo> tableInfos = tableInformation.getTableInfos();
        Map<String, String> ftlData = commonConfig.ftlDataMap;
        ftlData.put("TableName", tableInformation.getTableName());

        ftlData.put("EntityLowerName", commonConfig.BaseClassName.toLowerCase());
        ftlData.put("EntityClassFileName",tableInformation.getBaseClassName());
        ftlData.put("Properties", GeneratorUtil.generateDhpEntityProperties(tableInfos));
        ftlData.put("Methods", GeneratorUtil.generateEntityMethods(tableInfos));


        String filePath = thisUnit.completePackageName;
        String fileName = tableInformation.getBaseClassName() + ".java";
        // 生成文件
        FileUtil.generateToJava(FreemarketConfigUtils.TYPE_ENTITY, ftlData, filePath, fileName);
    }
}
