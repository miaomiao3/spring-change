package com.code.application;

import com.code.configs.TableListEnum;
import com.code.invoker.SingleInvoker;
import com.code.invoker.base.Invoker;


public class Main {

    public static void main(String[] args) {
         single();
    }

    /**
     * 根据DB生成对应数据
     */
    public static void single() {
        for (TableListEnum table : TableListEnum.values()) {
            Invoker invoker = new SingleInvoker.Builder()
                    .setTableName(table.name())
                    .setClassName(table.getEntityName())
                    .setFlag(table.getFlag())
                    .build();
            invoker.execute();
        }
    }
}
