package com.code.entity;

import com.code.configs.FieldTypeEnum;
import com.code.utils.StringUtil;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
public class ColumnInfo implements Serializable {
    private String columnName; // 列名
    private int type; // 类型代码
    private String propertyName; // 属性名
    private boolean isPrimaryKey; // 是否主键
    private String jdbcType;
    private String javaType;
    private String remarks;
    private String typeName;


    public ColumnInfo(String columnName, int type, boolean isPrimaryKey) {
        this.columnName = columnName;
        this.type = type;
        this.propertyName = StringUtil.columnName2PropertyName(columnName);
        this.isPrimaryKey = isPrimaryKey;

        FieldTypeEnum fte = FieldTypeEnum.getByValue(type);
        setJdbcType(fte.getJdbcType());
        setJavaType(fte.getJavaType());
    }

    public ColumnInfo(String columnName, int type, boolean isPrimaryKey, String remarks) {
        this.columnName = columnName;
        this.type = type;
        this.propertyName = StringUtil.columnName2PropertyName(columnName);
        this.isPrimaryKey = isPrimaryKey;
        this.remarks = remarks;

        FieldTypeEnum fte = FieldTypeEnum.getByValue(type);
        setJdbcType(fte.getJdbcType());
        setJavaType(fte.getJavaType());
    }
}
