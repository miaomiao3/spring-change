package com.code.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
public class Configuration implements Serializable {
    private Db db;
    private String packageName;
    private PackagePath packagePath;
    private String modelName;
    private String author;

    public String getPackageName() {
        return packageName == null ? "" : packageName + ".";
    }

    @Data
    @NoArgsConstructor
    public static class Db {
        private String url;
        private String username;
        private String password;
        private String catalog;
    }

    @Data
    @NoArgsConstructor
    public static class PackagePath {
        String entity;
        String dao;
        String controller;
        String service;
        String mapper;
        String queryReq;
        String queryResp;
        String params;
        String saveReq;
    }
}
