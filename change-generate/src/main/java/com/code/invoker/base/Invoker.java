package com.code.invoker.base;

public interface Invoker {

    public void execute();

}
