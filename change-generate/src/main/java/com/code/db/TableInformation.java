package com.code.db;

import com.code.entity.ColumnInfo;
import lombok.Data;

import java.util.List;

@Data
public class TableInformation {
    String tableName;
    String baseClassName;
    String className;
    String parentTableName;
    String parentClassName;
    String foreignKey;
    String relationalTableName;
    String parentForeignKey;
    List<ColumnInfo> tableInfos;
    List<ColumnInfo> parentTableInfos;
    List<String> units;
}
