package com.code.db;


import com.code.entity.ColumnInfo;
import com.code.utils.ConfigUtil;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ConnectionUtil {
    private final static String DRIVER_MYSQL = "com.mysql.cj.jdbc.Driver";
    private final static String DRIVER_ORACLE = "oracle.jdbc.driver.OracleDriver";
    private final static String DRIVER_SQLSERVER = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
    private Connection connection;
    private Statement statement;
    private ResultSet resultSet;

    /**
     * 初始化数据库连接
     *
     * @return
     */
    public boolean initConnection() {
        try {
            Class.forName(this.getDriver(ConfigUtil.getConfiguration().getDb().getUrl()));
            String url = ConfigUtil.getConfiguration().getDb().getUrl();
            String username = ConfigUtil.getConfiguration().getDb().getUsername();
            String password = ConfigUtil.getConfiguration().getDb().getPassword();
            connection = DriverManager.getConnection(url, username, password);
            return true;
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public String getDriver(String url) {
        if (url.contains("mysql")) {
            return DRIVER_MYSQL;
        }
        if (url.contains("oracle")) {
            return DRIVER_ORACLE;
        }
        if (url.contains("sqlserver")) {
            return DRIVER_SQLSERVER;
        }
        return null;
    }

    /**
     * 获取表结构数据
     *
     * @param tableName 表名
     * @return 包含表结构数据的列表
     */
    public List<ColumnInfo> getMetaData(String tableName) throws SQLException {
        ResultSet tempResultSet = connection.getMetaData().getPrimaryKeys("", "", tableName);
        String primaryKey = null;
        if (tempResultSet.next()) {
            primaryKey = tempResultSet.getObject(4).toString();
        }
        List<ColumnInfo> columnInfos = new ArrayList<>();
        statement = connection.createStatement();
        String sql = "SELECT * FROM " + tableName + " WHERE 1 != 1";
        resultSet = statement.executeQuery(sql);
        ResultSetMetaData metaData = resultSet.getMetaData();
        for (int i = 1; i <= metaData.getColumnCount(); i++) {
            ColumnInfo info;
            if (metaData.getColumnName(i).equals(primaryKey)) {
                info = new ColumnInfo(metaData.getColumnName(i), metaData.getColumnType(i), true);
            } else {
                info = new ColumnInfo(metaData.getColumnName(i), metaData.getColumnType(i), false);
            }
            columnInfos.add(info);
        }
        statement.close();
        resultSet.close();
        return columnInfos;
    }

    public void close() {
        try {
            if (!connection.isClosed()) {
                connection.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    public List<ColumnInfo> getMetaData1(String tableName) throws SQLException {
        DatabaseMetaData dbmd = connection.getMetaData();
        ResultSet tempResultSet = dbmd.getPrimaryKeys(ConfigUtil.getConfiguration().getDb().getCatalog(), "", tableName);
        String primaryKey = null;
        if (tempResultSet.next()) {
            primaryKey = tempResultSet.getObject(4).toString();
        }
        ResultSet rs2 = dbmd.getColumns(ConfigUtil.getConfiguration().getDb().getCatalog(), null, tableName, "%");
        List<ColumnInfo> columnInfos = new ArrayList<>();
        while (rs2.next()) {
            String columnName = rs2.getString("COLUMN_NAME");
            int type = rs2.getInt("DATA_TYPE");
            String remarks = rs2.getString("REMARKS");
            ColumnInfo info;
            if (columnName.equals(primaryKey)) {
                info = new ColumnInfo(columnName, type, true, remarks);
            } else {
                info = new ColumnInfo(columnName, type, false, remarks);
            }
            columnInfos.add(info);
        }
        return columnInfos;
    }


}
