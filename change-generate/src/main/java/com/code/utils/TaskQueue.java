package com.code.utils;

import com.code.db.TableInformation;
import com.code.task.*;
import com.code.task.base.AbstractTask;

import java.util.LinkedList;

public class TaskQueue {

    private LinkedList<AbstractTask> taskQueue = new LinkedList<>();


    public void initSingleTasks(TableInformation tableInformation) {
        for (String unit : tableInformation.getUnits()) {
            switch (unit) {
                case "Entity":
                    taskQueue.add(new EntityTask(tableInformation));
                    continue;
                case "Dao":
                    taskQueue.add(new DaoTask(tableInformation));
                    continue;
                case "Controller":
                    taskQueue.add(new ControllerTask(tableInformation));
                    continue;
                case "Service":
                    taskQueue.add(new ServiceTask(tableInformation));
                    continue;
                case "Mapper":
                    taskQueue.add(new MapperTask(tableInformation));
                    continue;
                /*case "QueryReq":
                    taskQueue.add(new QueryReqTask(tableInformation));
                    continue;
                case "QueryResp":
                    taskQueue.add(new QueryRespTask(tableInformation));
                    continue;
                case "SaveReq":
                    taskQueue.add(new SaveReqTask(tableInformation));
                    continue;*/
                case "Params":
                    taskQueue.add(new ParamsTask(tableInformation));
                    continue;
                default:
                    continue;
            }
        }
    }


    public boolean isEmpty() {
        return taskQueue.isEmpty();
    }

    public AbstractTask poll() {
        return taskQueue.poll();
    }

}
