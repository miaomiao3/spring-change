package com.code.utils;

import freemarker.template.Template;
import freemarker.template.TemplateException;

import java.io.*;

public class FileUtil {

    public static boolean mkdir(String path) {
        boolean rnt = false;
        path = path.replace("\\", "##");
        String[] paths = path.split("##");
        String filePath = "";
        for (String p : paths) {
            filePath = filePath + "\\" + p;
            File file = new File(filePath);
            if (!file.exists()) {
                rnt = file.mkdir();
                System.out.println(rnt + ":" + path);
            }
        }
        return rnt;
    }

    public static void generateToJava(int type, Object data, String filePath, String fileName) throws IOException, TemplateException {

        String completeFilePath = getSourcePath();
        for (String fp : filePath.split("\\.")) {
            completeFilePath = completeFilePath + "\\" + fp;
        }
        mkdir(completeFilePath);
        generateToJava(type, data, completeFilePath + "\\" + fileName);

    }

    /**
     * @param type     使用模板类型
     * @param data     填充数据
     * @param filePath 输出文件
     * @throws IOException
     * @throws TemplateException
     */
    public static void generateToJava(int type, Object data, String filePath) throws IOException, TemplateException {
        File file = new File(filePath);
        if (file.exists()) {
            System.err.println("ERROR: " + file.getPath().substring(file.getPath().lastIndexOf("\\") + 1, file.getPath().length()) + " 已存在，请手动修改");
            file.delete();
            //return;
        }
        Template tpl = getTemplate(type); // 获取模板文件
        // 填充数据
        StringWriter writer = new StringWriter();
        tpl.process(data, writer);
        writer.flush();
        // 写入文件
        FileOutputStream fos = new FileOutputStream(filePath);
        OutputStreamWriter osw = new OutputStreamWriter(fos, "UTF-8");
        BufferedWriter bw = new BufferedWriter(osw, 1024);
        tpl.process(data, bw);
        fos.close();
    }

    /**
     * 获取模板
     *
     * @param type 模板类型
     * @return
     * @throws IOException
     */
    private static Template getTemplate(int type) throws IOException {
        switch (type) {
            case FreemarketConfigUtils.TYPE_ENTITY:
                return FreemarketConfigUtils.getInstance().getTemplate("entity.ftl");
            case FreemarketConfigUtils.TYPE_DAO:
                return FreemarketConfigUtils.getInstance().getTemplate("dao.ftl");
            case FreemarketConfigUtils.TYPE_SERVICE:
                return FreemarketConfigUtils.getInstance().getTemplate("service.ftl");
            case FreemarketConfigUtils.TYPE_CONTROLLER:
                return FreemarketConfigUtils.getInstance().getTemplate("controller.ftl");
            case FreemarketConfigUtils.TYPE_MAPPER:
                return FreemarketConfigUtils.getInstance().getTemplate("mapper.ftl");
            case FreemarketConfigUtils.QUERY_REQ:
                return FreemarketConfigUtils.getInstance().getTemplate("queryReq.ftl");
            case FreemarketConfigUtils.QUERY_RESP:
                return FreemarketConfigUtils.getInstance().getTemplate("queryResp.ftl");
            case FreemarketConfigUtils.SAVE_REQ:
                return FreemarketConfigUtils.getInstance().getTemplate("saveReq.ftl");
            case FreemarketConfigUtils.TYPE_PARAMS:
                return FreemarketConfigUtils.getInstance().getTemplate("params.ftl");
            default:
                return null;
        }
    }

    private static String getBasicProjectPath() {
        String path = new File(FileUtil.class.getClassLoader().getResource("").getFile()).getPath() + File.separator;
        StringBuilder sb = new StringBuilder();
        sb.append(path.substring(0, path.indexOf("target"))).append("src").append(File.separator).append("main").append(File.separator);
        return sb.toString();
    }

    /**
     * 获取源码路径
     *
     * @return
     */
    public static String getSourcePath() {
        StringBuilder sb = new StringBuilder();
        sb.append(getBasicProjectPath()).append("java").append(File.separator);
        return sb.toString();
    }

}
