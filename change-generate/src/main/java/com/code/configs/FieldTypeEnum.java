package com.code.configs;

import lombok.Getter;

@Getter
public enum FieldTypeEnum {
    ARRAY(2003, "ARRAY", "ARRAY"),
    BIGINT(-5, "BIGINT", "Long"),
    BINARY(-2, "BINARY", "BINARY"),
    BIT(-7, "BIT", "BIT"),
    BLOB(2004, "BLOB", "BLOB"),
    BOOLEAN(16, "BOOLEAN", "boolean"),
    CHAR(1, "CHAR", "CHAR"),
    CLOB(2005, "CLOB", "CLOB"),
    DATALINK(70, "DATALINK", "DATALINK"),
    DATE(91, "DATE", "Timestamp"),
    DECIMAL(3, "DECIMAL", "BigDecimal"),
    DISTINCT(2001, "DISTINCT", "DISTINCT"),
    DOUBLE(8, "DOUBLE", "BigDecimal"),
    FLOAT(6, "FLOAT", "BigDecimal"),
    INTEGER(4, "INTEGER", "INTEGER"),
    JAVA_OBJECT(2000, "JAVA_OBJECT", "Object"),
    LONGNVARCHAR(-16, "LONGNVARCHAR", "String"),
    LONGVARBINARY(-4, "LONGVARBINARY", "byte[]"),
    LONGVARCHAR(-1, "LONGVARCHAR", "LONGVARCHAR"),
    NCHAR(-15, "NCHAR", "NCHAR"),
    NCLOB(2011, "NCLOB", "NCLOB"),
    NULL(0, "NULL", "NULL"),
    NUMERIC(2, "NUMERIC", "BigDecimal"),
    NVARCHAR(-9, "NVARCHAR", "NVARCHAR"),
    OTHER(1111, "OTHER", "OTHER"),
    REAL(7, "REAL", "float"),
    REF(2006, "REF", "REF"),
    REF_CURSOR(2012, "REF_CURSOR", "REF_CURSOR"),
    ROWID(-8, "ROWID", "ROWID"),
    SMALLINT(5, "SMALLINT", "int"),
    SQLXML(2009, "SQLXML", "SQLXML"),
    STRUCT(2002, "STRUCT", "STRUCT"),
    TIME(92, "TIME", "Timestamp"),
    TIME_WITH_TIMEZONE(2013, "TIME_WITH_TIMEZONE", "TIME_WITH_TIMEZONE"),
    TIMESTAMP(93, "TIMESTAMP", "Timestamp"),
    TIMESTAMP_WITH_TIMEZONE(2014, "TIMESTAMP_WITH_TIMEZONE", "TIMESTAMP_WITH_TIMEZONE"),
    TINYINT(-6, "TINYINT", "int"),
    VARBINARY(-3, "VARBINARY", "VARBINARY"),
    VARCHAR(12, "VARCHAR", "VARCHAR"),
    DEFAULT(-999, "JAVA_OBJECT", "Object");

    private Integer typeValue;
    private String jdbcType;
    private String javaType;

    FieldTypeEnum(Integer typeValue, String jdbcType, String javaType) {
        this.typeValue = typeValue;
        this.jdbcType = jdbcType;
        this.javaType = javaType;
    }

    public static FieldTypeEnum getByValue(Integer typeValue) {
        for (FieldTypeEnum fte : FieldTypeEnum.values()) {
            if (typeValue == fte.typeValue) {
                return fte;
            }
        }
        return FieldTypeEnum.DEFAULT;
    }

    public static FieldTypeEnum getByValue(String typeValue) {
        for (FieldTypeEnum fte : FieldTypeEnum.values()) {
            if (typeValue.equals(fte.jdbcType)) {
                return fte;
            }
        }
        return FieldTypeEnum.DEFAULT;
    }
}
