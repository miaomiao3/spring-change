package com.code.configs;

import com.code.utils.ConfigUtil;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class CommonConfig {
    public String Author;
    public String ModelName;
    public String CreatedDate;
    public String FilePackage;
    public String BaseClassName;
    public CommonUnit EntityUnit;
    public CommonUnit DaoUnit;
    public CommonUnit ControllerUnit;
    public CommonUnit ServiceUnit;
    public CommonUnit MapperUnit;
    public CommonUnit QueryReq;
    public CommonUnit QueryResp;
    public CommonUnit Params;
    public CommonUnit SaveReq;
    public Map<String, String> ftlDataMap;


    public CommonConfig(String baseClassName) {
        this.Author = ConfigUtil.getConfiguration().getAuthor();
        this.ModelName = ConfigUtil.getConfiguration().getModelName();
        this.CreatedDate = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
        this.BaseClassName = baseClassName;
        this.EntityUnit = new CommonUnit(CommonUnitEnum.Entity, baseClassName);
        this.DaoUnit = new CommonUnit(CommonUnitEnum.Dao, baseClassName);
        this.ControllerUnit = new CommonUnit(CommonUnitEnum.Controller, baseClassName);
        this.ServiceUnit = new CommonUnit(CommonUnitEnum.Service, baseClassName);
        this.MapperUnit = new CommonUnit(CommonUnitEnum.Mapper, baseClassName);
        this.QueryReq = new CommonUnit(CommonUnitEnum.QueryReq, baseClassName);
        this.QueryResp = new CommonUnit(CommonUnitEnum.QueryResp, baseClassName);
        this.Params = new CommonUnit(CommonUnitEnum.Params, baseClassName);
        this.SaveReq = new CommonUnit(CommonUnitEnum.SaveReq, baseClassName);
        this.ftlDataMap = new HashMap<>();
        ftlDataMap.put("Author", this.Author);
        ftlDataMap.put("ModelName", this.ModelName);
        ftlDataMap.put("CreatedDate", this.CreatedDate);
        ftlDataMap.put("Date", this.CreatedDate);
        ftlDataMap.put("FilePackage", this.FilePackage);
        ftlDataMap.put("BaseClassName", this.BaseClassName);
        ftlDataMap.putAll(this.EntityUnit.maps);
        ftlDataMap.putAll(this.ControllerUnit.maps);
        ftlDataMap.putAll(this.MapperUnit.maps);
        ftlDataMap.putAll(this.ServiceUnit.maps);
        ftlDataMap.putAll(this.DaoUnit.maps);

    }
}


