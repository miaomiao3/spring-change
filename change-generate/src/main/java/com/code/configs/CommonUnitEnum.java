package com.code.configs;

import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

@Getter
public enum CommonUnitEnum {
    Controller("Controller", "controller", "all,controller"),
    Service("Service", "service", "all,service"),
    Dao("Mapper", "mapper", "all,dao"),
    Mapper("Mapper", "mapper", "all,mapper"),
    Entity("Entity", "entity", "all,entity"),
    QueryResp("QueryResp", "queryResp", "all,queryResp"),
    QueryReq("QueryReq", "queryReq", "all,queryReq"),
    Params("Params", "params", "all,params"),
    SaveReq("SaveReq", "saveReq", "all,saveReq")
    ;


    String suffix;
    String dir;
    String flag;

    CommonUnitEnum(String suffix, String dir, String flag) {
        this.suffix = suffix;
        this.dir = dir;
        this.flag = flag;
    }

    public static List<String> getUnits(String commonFlag) {
        List<String> units = new ArrayList<>();
        for (CommonUnitEnum cue : CommonUnitEnum.values()) {
            if (cue.getFlag().indexOf(commonFlag) >= 0) {
                units.add(cue.name());
            }
        }
        return units;
    }


}
