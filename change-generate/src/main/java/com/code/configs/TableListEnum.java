package com.code.configs;

import lombok.Getter;

@Getter
public enum TableListEnum {


    //dyes_stuff_material_requisition_insufficient_log("DyesStuffMaterialRequisitionInsufficientLog", "all"),
    //config_project("ConfigProject", "controller"),
    //config_project("ConfigProject", "service")
    //config_project("ConfigProject", "dao")
    //config_account_name("ConfigAccountName", "entity"),
    // config_project("ConfigProject", "queryReq"),
    // config_project("ConfigProject", "queryResp"),
    //  account_wechat("AccountWechat", "all")
    // company_info("CompanyInfo", "all"),
    // weather("Weather", "all"),
//    robot("Robot", "all"),
    recommend_info_file("RecommendInfoFile", "all"),
    ;


    String entityName;
    String flag;

    TableListEnum(String entityName, String flag) {
        this.entityName = entityName;
        this.flag = flag;
    }


}
