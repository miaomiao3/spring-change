package com.code.configs;

import com.code.utils.ConfigUtil;
import com.code.utils.StringUtil;

import java.util.HashMap;
import java.util.Map;

public class CommonUnit {
    public CommonUnitEnum unit;
    public String classFileName;
    public String packageName;
    public String instName;
    public String completePackageName;
    public String completeClassFileName;
    public Map<String, String> maps;

    public CommonUnit(CommonUnitEnum unit, String baseClassName) {
        String mainPaakage = ConfigUtil.getConfiguration().getPackageName();
        this.unit = unit;
        this.classFileName = baseClassName + unit.suffix;
        if (unit == CommonUnitEnum.Entity) {
            this.packageName = ConfigUtil.getConfiguration().getPackagePath().getEntity()+ "." + ConfigUtil.getConfiguration().getModelName();
            this.classFileName = baseClassName;
        } else if (unit == CommonUnitEnum.Dao) {
            this.packageName = ConfigUtil.getConfiguration().getPackagePath().getDao()+ "." + ConfigUtil.getConfiguration().getModelName();
        } else if (unit == CommonUnitEnum.Controller) {
            this.packageName = ConfigUtil.getConfiguration().getPackagePath().getController()+ "." + ConfigUtil.getConfiguration().getModelName();
        } else if (unit == CommonUnitEnum.Service) {
            this.packageName = ConfigUtil.getConfiguration().getPackagePath().getService()+ "." + ConfigUtil.getConfiguration().getModelName();
        } else if (unit == CommonUnitEnum.Mapper) {
            this.packageName = ConfigUtil.getConfiguration().getPackagePath().getMapper()+ "." + ConfigUtil.getConfiguration().getModelName();
        } else if (unit == CommonUnitEnum.QueryReq) {
            this.packageName = ConfigUtil.getConfiguration().getPackagePath().getQueryReq();
        } else if (unit == CommonUnitEnum.QueryResp) {
            this.packageName = ConfigUtil.getConfiguration().getPackagePath().getQueryResp();
        } else if (unit == CommonUnitEnum.Params) {
            this.packageName = ConfigUtil.getConfiguration().getPackagePath().getParams();
        } else if (unit == CommonUnitEnum.SaveReq) {
            this.packageName = ConfigUtil.getConfiguration().getPackagePath().getSaveReq();
        } else {
            this.packageName = "default";
        }


        this.completePackageName = mainPaakage + this.packageName;
        this.instName = StringUtil.firstToLowerCase(this.classFileName);
        this.completeClassFileName = this.completePackageName + "." + this.classFileName;

        this.maps = new HashMap<>();
        maps.put(unit.suffix + "ClassFileName", this.classFileName);
        maps.put(unit.suffix + "PackageName", this.packageName);
        maps.put(unit.suffix + "InstName", this.instName);
        maps.put(unit.suffix + "CompletePackageName", this.completePackageName);
        maps.put(unit.suffix + "CompleteClassFileName", this.completeClassFileName);
    }
}
