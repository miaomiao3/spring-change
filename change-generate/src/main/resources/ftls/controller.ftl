package ${ControllerCompletePackageName};

import ${ServiceCompleteClassFileName};
import ${EntityCompleteClassFileName};
import com.change.core.page.Page;
import com.change.dto.params.${EntityClassFileName}Params;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;
import java.util.List;

/**
* @author ${Author}
* @date  ${Date}
*/
@Api(value = "${ControllerClassFileName}", tags = "")
@Slf4j
@RestController
@RequestMapping(value = "/api/${ModelName}")
public class ${ControllerClassFileName} {
        private final ${ServiceClassFileName} ${ServiceInstName};

        public ${ControllerClassFileName}(${ServiceClassFileName} ${ServiceInstName}) {
                this.${ServiceInstName} = ${ServiceInstName};
        }

        @ApiImplicitParams({
        @ApiImplicitParam(name = "current", value = "当前页", defaultValue = "1"),
        @ApiImplicitParam(name = "size", value = "每页数量", defaultValue = "100")
        })
        @ApiOperation(value = "分页查询")
        @GetMapping("/page${EntityClassFileName}")
        public Page<List<${EntityClassFileName}Params>> page${EntityClassFileName}(@ApiIgnore Page page, ${EntityClassFileName}Params req) {
                return ${ServiceInstName}.getPage(page, req);
        }

        @ApiOperation(value = "详情")
        @GetMapping("/detail${EntityClassFileName}/{id}")
        public ${EntityClassFileName}Params detail(@PathVariable Integer id) {
                return ${ServiceInstName}.detail(id);
        }

        @ApiOperation(value = "新增或更新")
        @PostMapping("/saveOrUpdate${EntityClassFileName}")
        public ${EntityClassFileName} saveOrUpdate(@RequestBody ${EntityClassFileName}Params req) {
                return ${ServiceInstName}.saveOrUpdate(req);
        }

        /**@ApiOperation(value = "修改")
        @PostMapping("/update")
        public ${EntityClassFileName} update(@RequestBody ${EntityClassFileName}Params req) {
                return ${ServiceInstName}.update(req);
        }*/

        @ApiOperation(value = "删除")
        @PostMapping("/delete${EntityClassFileName}/{id}")
        public Integer delete(@PathVariable Integer id) {
                return ${ServiceInstName}.delete(id);
        }

}