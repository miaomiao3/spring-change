package ${EntityCompletePackageName};

import com.baomidou.mybatisplus.annotation.TableName;
import com.change.dto.params.BaseParams;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.sql.Timestamp;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
* @author ${Author}
* @date  ${Date}
*/
@Data
public class ${EntityClassFileName}Params extends BaseParams {
private static final long serialVersionUID = 1L;
${Properties}

}