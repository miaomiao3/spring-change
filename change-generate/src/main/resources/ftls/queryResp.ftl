package com.change.dto.resp;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
* @author ${Author}
* @date  ${Date}
*/
@Data
public class Query${EntityClassFileName}Resp extends BaseResp{
${Properties}

}