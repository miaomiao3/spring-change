package ${MapperCompletePackageName};

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import ${EntityCompleteClassFileName};
import org.springframework.stereotype.Repository;

/**
* @author ${Author}
* @date  ${Date}
*/
@Repository
public interface ${MapperClassFileName} extends BaseMapper<${EntityClassFileName}> {

}