package com.change.dto.req;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
* @author ${Author}
* @date  ${Date}
*/
@Data
public class Query${EntityClassFileName}Req extends BaseReq {
${Properties}

}