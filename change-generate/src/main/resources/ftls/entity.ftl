package ${EntityCompletePackageName};

import com.baomidou.mybatisplus.annotation.TableName;
import com.change.entity.BaseEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.sql.Timestamp;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
* @author ${Author}
* @date  ${Date}
*/
@Data
@TableName("${TableName}")
public class ${EntityClassFileName} extends BaseEntity {
private static final long serialVersionUID = 1L;
${Properties}

}