package ${ServiceCompletePackageName};

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import lombok.extern.slf4j.Slf4j;
import org.springframework.transaction.annotation.Transactional;
import ${EntityCompleteClassFileName};
import ${MapperCompleteClassFileName};
import com.change.core.page.Page;
import com.change.dto.params.${EntityClassFileName}Params;
import java.util.List;

/**
* @author ${Author}
* @date  ${Date}
*/
@Slf4j
@Service
public class ${ServiceClassFileName} extends ServiceImpl<${MapperClassFileName}, ${EntityClassFileName}>{
    private final ${MapperClassFileName} ${MapperInstName};

    public ${ServiceClassFileName}(${MapperClassFileName} ${MapperInstName}) {
    this.${MapperInstName} = ${MapperInstName};
    }

    public Page<List<${EntityClassFileName}Params>> getPage(Page page, ${EntityClassFileName}Params req) {

        return page;
    }

    public ${EntityClassFileName}Params detail(Integer id) {
         return new ${EntityClassFileName}Params();
    }

    @Transactional(rollbackFor = Exception.class)
    public ${EntityClassFileName} saveOrUpdate(${EntityClassFileName}Params req) {

        return null;
    }

    @Transactional(rollbackFor = Exception.class)
    public ${EntityClassFileName} update(${EntityClassFileName}Params req) {
         return null;
    }

    @Transactional(rollbackFor = Exception.class)
    public Integer delete(Integer id) {
        return null;
    }
}
